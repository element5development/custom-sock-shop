<?php get_header(); ?>

	<section id="fullwidth" class="searchresults">

	<?php if (have_posts()) : ?>
		<?php while (have_posts()) : the_post(); ?>
			<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<div class="wrap">
					<h3><?php the_title(); ?></h3>
					<div class="entry">
						<p><?php echo excerpt(46); ?></p>
					</div>
					<a href="<?php the_permalink(); ?>" class="btn">Learn More</a>
				</div>
			</article>
		<?php endwhile; ?>
		<?php post_navigation(); ?>
	<?php else : ?>
		<article>
			<h3><?php _e('Nothing Found','html5reset'); ?></h3>
		</article>
	<?php endif; ?>

	</section>




<?php get_footer(); ?>
