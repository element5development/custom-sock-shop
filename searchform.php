<form role="search" method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
	<label for="search">Search</label>
	<input type="search" id="s" name="s" value="" placeholder="<?php the_search_query(); ?>..." />
	<input type="submit" value="search" id="searchsubmit" />
</form>