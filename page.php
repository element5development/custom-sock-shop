<?php get_header(); ?>

<!-- SUBHEADING -->
<?php if( get_field('page_subheading') ): ?>
	<?php get_template_part('template-parts/subheading'); ?>
<?php endif; ?>

<!-- DEFAULT CONTENT -->
<div class="wrap">
	<section id="fullwidth">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="post" id="post-<?php the_ID(); ?>">
				<div class="entry">
					<?php the_content(); ?>
				</div>
			</article>
		<?php endwhile; endif; ?>
	</section>
</div>

<?php get_footer(); ?>