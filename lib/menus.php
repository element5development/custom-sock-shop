<?php

/*----------------------------------------------------------------*\

	CUSTOM MENU AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/
function nav_creation() {
	register_nav_menu( 'primary', __( 'Navigation Menu', 'html5reset' ) );
	register_nav_menu( 'landerfooter', __( 'Dress Footer Navigation', 'html5reset' ) );
}
add_action( 'init', 'nav_creation' );
/*----------------------------------------------------------------*\
	PAGNATION MENUS
\*----------------------------------------------------------------*/
function post_navigation() {
	echo '<div class="navigation">';
	echo '<div class="next-posts">'.get_next_posts_link('&laquo; Older Entries').'</div>';
	echo '<div class="prev-posts">'.get_previous_posts_link('Newer Entries &raquo;').'</div>';
	echo '</div>';
}

?>