<?php

/*----------------------------------------------------------------*\

	CUSTOM POST TYPES
	www.wp-hasty.com

\*----------------------------------------------------------------*/
	//testimonials
	function testimonial_post_type() {
		$labels = array(
			'name' => _x("Testimonials", "post type general name"),
			'singular_name' => _x("Testimonial", "post type singular name"),
			'menu_name' => 'Testimonials',
			'add_new' => _x("Add New", "testimonial item"),
			'add_new_item' => __("Add New Testimonial"),
			'edit_item' => __("Edit Testimonial"),
			'new_item' => __("New Testimonial"),
			'view_item' => __("View Testimonial"),
			'search_items' => __("Search Testimonials"),
			'not_found' =>  __("No Testimonials Found"),
			'not_found_in_trash' => __("No Testimonials Found in Trash"),
			'parent_item_colon' => ''
		);
		register_post_type('testimonial' , array(
			'labels' => $labels,
			'public' => true,
			'has_archive' => false,
			'rewrite' => false,
			'menu_position' => 5,
			'supports' => array('title', 'editor', 'thumbnail'),
				'taxonomies' => array('category'),
				'menu_icon' => '' //Add Menu Icon Class to CPT
		) );
	}
	add_action( 'init', 'testimonial_post_type', 0 );

	// Register Custom Post Type Sock of the Month
	function create_sockofthemonth_cpt() {

		$labels = array(
			'name' => _x( 'Sock of the Month', 'Post Type General Name', 'textdomain' ),
			'singular_name' => _x( 'Sock of the Month', 'Post Type Singular Name', 'textdomain' ),
			'menu_name' => _x( 'Sock of the Month', 'Admin Menu text', 'textdomain' ),
			'name_admin_bar' => _x( 'Sock of the Month', 'Add New on Toolbar', 'textdomain' ),
			'archives' => __( 'Sock of the Month Archives', 'textdomain' ),
			'attributes' => __( 'Sock of the Month Attributes', 'textdomain' ),
			'parent_item_colon' => __( 'Parent Sock of the Month:', 'textdomain' ),
			'all_items' => __( 'All Sock of the Month', 'textdomain' ),
			'add_new_item' => __( 'Add New Sock of the Month', 'textdomain' ),
			'add_new' => __( 'Add New', 'textdomain' ),
			'new_item' => __( 'New Sock of the Month', 'textdomain' ),
			'edit_item' => __( 'Edit Sock of the Month', 'textdomain' ),
			'update_item' => __( 'Update Sock of the Month', 'textdomain' ),
			'view_item' => __( 'View Sock of the Month', 'textdomain' ),
			'view_items' => __( 'View Sock of the Month', 'textdomain' ),
			'search_items' => __( 'Search Sock of the Month', 'textdomain' ),
			'not_found' => __( 'Not found', 'textdomain' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
			'featured_image' => __( 'Featured Image', 'textdomain' ),
			'set_featured_image' => __( 'Set featured image', 'textdomain' ),
			'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
			'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
			'insert_into_item' => __( 'Insert into Sock of the Month', 'textdomain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Sock of the Month', 'textdomain' ),
			'items_list' => __( 'Sock of the Month list', 'textdomain' ),
			'items_list_navigation' => __( 'Sock of the Month list navigation', 'textdomain' ),
			'filter_items_list' => __( 'Filter Sock of the Month list', 'textdomain' ),
		);
		$args = array(
			'label' => __( 'Sock of the Month', 'textdomain' ),
			'description' => __( '', 'textdomain' ),
			'labels' => $labels,
			'menu_icon' => 'dashicons-calendar',
			'supports' => array('title', 'editor', 'page-attributes', 'custom-fields'),
			'taxonomies' => array(),
			'public' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 5,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'can_export' => true,
			'has_archive' => true,
			'hierarchical' => false,
			'exclude_from_search' => false,
			'show_in_rest' => true,
			'publicly_queryable' => true,
			'capability_type' => 'page',
		);
		register_post_type( 'sock-of-the-month', $args );

	}
	add_action( 'init', 'create_sockofthemonth_cpt', 0 );
?>