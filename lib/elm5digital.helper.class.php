<?php
class ElmHelper {
    public static function log($obj, $file = "", $prefixLogWithDate = true) {
        if (empty($file)) {
            $file = "logs-".(date("Ymd")).".log";
        }
        $upload_dir = wp_upload_dir();
        $dir = $upload_dir['basedir'] . '/logs/element5digital';
        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }

        $path = $dir . '/'. $file;
        $date = "";
        if ($prefixLogWithDate) {
            $date = "[" . date("Y-m-d H:i:s"). "] ";
        }
        if (is_array($obj) || is_object($obj)) {
            $log = json_encode($obj, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_PARTIAL_OUTPUT_ON_ERROR);
        }
        else {
            $log = $obj;
        }
        file_put_contents($path, $date.$log."\r\n", FILE_APPEND);
    }

    public static function logNoDate($obj, $file = "") {
        self::log($obj, $file, false);
    }

    public static function ceilTime($time, $seconds = 300) {
        return ceil($time/$seconds)*$seconds;
    }

    public static function sanitizeTitleToId($title) {
        $sTitle = preg_replace('/\W/', '', strtolower($title));
        return $sTitle;
    }

    public static function secondsToHumanTime($seconds) {
        $time = array();
        if (!empty($seconds)) {
            //$seconds = intval($seconds);
            $dtF = new DateTime('@0');
            $dtT = new DateTime("@$seconds");
            $diff = $dtF->diff($dtT);
            $days = $diff->format('%a');
            $hours = $diff->format('%h');
            $mins = $diff->format('%i');
            $snds = $diff->format('%s');

            if (!empty($days)) {
                $time[] = "$days days";
            }
            if (!empty($hours)) {
                $time[] = "$hours hours";
            }
            if (!empty($mins)) {
                $time[] = "$mins minutes";
            }
            if (!empty($snds)) {
                $time[] = "$snds seconds";
            }
        }

        $timeString = implode(" ", $time);
        return $timeString;
    }

    public static function getOption( $option_name , $default = '') {
        global $wpdb;
        $row = $wpdb->get_row( $wpdb->prepare( "SELECT option_value
            FROM $wpdb->options WHERE option_name = %s LIMIT 1", $option_name ) );
        if ( is_object( $row ) ) {
            $value = $row->option_value;
        } else {
            $value = $default;
        }
        return $value;
    }


    public static function copyFileFromUrl($sourceUrl, $saveFile, $savePath, $options = array()) {

        error_reporting( E_ALL );
        ini_set( 'display_errors', 1 );

        $saveDetails = self::prepareUpload($savePath, $saveFile);
        extract($saveDetails);
        /** @var  $path */
        /** @var  $url */

        ElmHelper::log("Copying Image From Url:$sourceUrl TO $path");

        try {
            copy($sourceUrl, $path);
        }
        catch (Exception $ex) {
            ElmHelper::log("ERROR SAVING IMAGE FROM $sourceUrl TO $path");
            $error = $ex->getMessage();
            ElmHelper::log($error);
            $saveDetails['error'] = $error;
        }
        ini_set( 'display_errors', 0 );

        return $saveDetails;

    }

    public static function prepareUpload($path ='', $file = '') {

        $upload = wp_get_upload_dir();
        $basePath = array(str_replace('\\','/', $upload['basedir']));
        $baseUrl = array($upload['baseurl']);
        if (!empty($path)) {
            $basePath[] = $path;
            $baseUrl[] = $path;
        }
        $basePathFlat = implode('/', $basePath);
        if (!file_exists($basePathFlat)) {
            mkdir($basePathFlat, 0777, true);
        }
        if (!empty($file)) {
            $file = sanitize_file_name($file);
            $basePath[] = $file;
            $baseUrl[] = $file;
        }

        $basePathFlat = implode('/', $basePath);
        $baseUrlFlat = implode('/', $baseUrl);

        return array(
            'path' => $basePathFlat,
            'url' => $baseUrlFlat,
        );
    }

    public static function archiveFiles($files, $zipFileName, $zipFilePath, $options = array()) {
        $defaults = array(
            'customFileNames' => false,
        );
        $options = array_merge($defaults, $options);
        extract($options);

        $saveDetails = self::prepareUpload($zipFilePath, $zipFileName);
        extract($saveDetails);
        /** @var  $path */
        /** @var  $url */

        try {
            $zip = new ZipArchive;
            $res = $zip->open($path, ZipArchive::CREATE | ZipArchive::OVERWRITE);

            foreach ($files as $index => $value) {
                if (!empty($customFileNames)) {
                    $zip->addFile($value, $index);
                }
                else {
                    $zip->addFile($value, basename($value));
                }
            }
            $zip->close();

        }
        catch (Exception $ex) {
            ElmHelper::log("ERROR ARCHIVING FILE TO ZIP $zipFileName");
            ElmHelper::log($files);
            $error = $ex->getMessage();
            ElmHelper::log($error);
            $saveDetails['error'] = $error;
            //echo 'Message: ' . $ex->getMessage();
        }

        return $saveDetails;
    }
}
