<?php

	/*----------------------------------------------------------------*\
		ADDITIONAL IMAGE SIZES
	\*----------------------------------------------------------------*/
	if ( function_exists( 'add_image_size' ) ) {
		add_image_size( 'socklife', 400, 300, array( 'center', 'top' ) );
		add_image_size( 'slide', 2000, 800, array( 'center', 'top' ) );
	}
	/*----------------------------------------------------------------*\
		ADDITIONAL MEDIA FILE FORMATS
	\*----------------------------------------------------------------*/
	function cc_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		$mimes['zip'] = 'application/zip';
		return $mimes;
	}
	add_filter('upload_mimes', 'cc_mime_types');

?>