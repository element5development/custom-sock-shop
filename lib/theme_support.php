<?php

/*----------------------------------------------------------------*\
	JS AND CSS FILES
\*----------------------------------------------------------------*/
add_action( 'admin_enqueue_scripts', 'load_admin_styles' );
function load_admin_styles() {
    wp_enqueue_style( 'admin_css_lp', get_template_directory_uri() . '/assets/styles/custom/admin.css', false, '1.0.0');
}

function wp_main_assets() {	
  wp_enqueue_style( 'default', get_stylesheet_uri() );
  wp_enqueue_style( 'dashicons' );
  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), 'lp-5.0.0.0', 'all');
  wp_enqueue_script('vendors', get_template_directory_uri() . '/dist/scripts/vendors/vendors.js', array( 'jquery' ), 1.1, true);
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), 'lp-5.0.0.0',
      true);

}
add_action('wp_enqueue_scripts', 'wp_main_assets');

/*----------------------------------------------------------------*\
	ENABLE FEATURED IMAGES
\*----------------------------------------------------------------*/
add_theme_support( 'post-thumbnails' );
/*----------------------------------------------------------------*\
	DECLARE WOOCOMMERCE SUPPORT
\*----------------------------------------------------------------*/
add_theme_support('woocommerce');
/*----------------------------------------------------------------*\
	SHORTCODE FOR SUPPORT IN WIDGETS
\*----------------------------------------------------------------*/
add_filter('widget_text', 'do_shortcode');
/*----------------------------------------------------------------*\
	ADD CLASSES TO BODY ON MOBILE DEVICES
\*----------------------------------------------------------------*/
function add_mobile_body_class( $classes ) {
	if (wp_is_mobile()) {
		$classes[] = 'mobile-device';
	}
	return $classes;
}
add_filter( 'body_class', 'add_mobile_body_class' );
/*----------------------------------------------------------------*\
	CUSTOM EXCERPT
\*----------------------------------------------------------------*/
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
		array_pop($excerpt);
		$excerpt = implode(" ",$excerpt).'...';
	} else {
		$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
	return $excerpt;
}
/*----------------------------------------------------------------*\
 Like get_template_part() put lets you pass args to the template file
 Args are available in the tempalte as $template_args array
 @param string filepart
 @param mixed wp_args style argument list
\*----------------------------------------------------------------*/
function hm_get_template_part( $file, $template_args = array(), $cache_args = array() ) {
	$template_args = wp_parse_args( $template_args );
	$cache_args = wp_parse_args( $cache_args );
	if ( $cache_args ) {
			foreach ( $template_args as $key => $value ) {
					if ( is_scalar( $value ) || is_array( $value ) ) {
							$cache_args[$key] = $value;
					} else if ( is_object( $value ) && method_exists( $value, 'get_id' ) ) {
							$cache_args[$key] = call_user_method( 'get_id', $value );
					}
			}
			if ( ( $cache = wp_cache_get( $file, serialize( $cache_args ) ) ) !== false ) {
					if ( ! empty( $template_args['return'] ) )
							return $cache;
					echo $cache;
					return;
			}
	}
	$file_handle = $file;
	do_action( 'start_operation', 'hm_template_part::' . $file_handle );
	if ( file_exists( get_stylesheet_directory() . '/' . $file . '.php' ) )
			$file = get_stylesheet_directory() . '/' . $file . '.php';
	elseif ( file_exists( get_template_directory() . '/' . $file . '.php' ) )
			$file = get_template_directory() . '/' . $file . '.php';
	ob_start();
	$return = require( $file );
	$data = ob_get_clean();
	do_action( 'end_operation', 'hm_template_part::' . $file_handle );
	if ( $cache_args ) {
			wp_cache_set( $file, $data, serialize( $cache_args ), 3600 );
	}
	if ( ! empty( $template_args['return'] ) )
			if ( $return === false )
					return false;
			else
					return $data;
	echo $data;
}

?>