<?php

/*----------------------------------------------------------------*\
	EDITOR ROLE CAPABILITIES
\*----------------------------------------------------------------*/
function add_editor_power(){
	$role = get_role('editor');
	$role->add_cap('gform_full_access');
	$role->add_cap('edit_theme_options');
	$role->add_cap('edit_users');
	$role->add_cap('list_users');
	$role->add_cap('promote_users');
	$role->add_cap('create_users');
	$role->add_cap('add_users');
	$role->add_cap('delete_users');
	$role->add_cap('manage_options');
}
add_action('admin_init','add_editor_power');

?>