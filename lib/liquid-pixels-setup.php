<?php

require_once 'elm5digital.helper.class.php';

/*----------------------------------------------------------------*\
    CUSTOM LOGO MANAGEMENT
\*----------------------------------------------------------------*/
if (!defined('DS')) {
    define('DS', '/'); //DIRECTORY_SEPARATOR
}

function get_custom_logo_upload_dir($suffix = '')
{
    $folder_name = ''; //"custom_logo" . ($suffix ? '_' . $suffix : '');
    $upload_dir = wp_upload_dir();
    $dir = $upload_dir['basedir'] . DS . ($suffix ? $suffix . DS : '');
    $url = $upload_dir['baseurl'] . DS . ($suffix ? $suffix . DS : '');
    if (!file_exists($dir)) {
        mkdir($dir, 0777, true);
    }
    return ["dir" => $dir, "url" => $url];
}

function getUploadedPathFromUrl($uri) {
    $uploadPaths = get_custom_logo_upload_dir();
    /** @var $dir  */
    /** @var $url  */
    extract($uploadPaths);
    $uri = preg_replace('/http.*\/uploads\//', '', $uri);
    return $dir . $uri;
}

function getFileNameFromUrl($uri) {
    return basename($uri);
}


function delete_custom_logo()
{
    $upload_info = get_custom_logo_upload_dir();
    $response = ["success" => false, "url" => "", "file" => ""];
    $directory = $upload_info['dir'];
    $fileToDelete = @$_POST['filename'];
    $path = $directory . $fileToDelete;
    unlink($path);
    $response['success'] = true;
    $response['file'] = $fileToDelete;
    echo json_encode($response);
    wp_die();
}

function upload_logo_image()
{
    $response = ["success" => false, "url" => "", "file" => ""];
    $uploadedFileName = $_FILES["images"]["name"];
    $tmpFileName = $_FILES["images"]["tmp_name"];
    $imagename = preg_replace('/[^\w\.]/', '', $uploadedFileName);
    $upload_info = get_custom_logo_upload_dir();
    $directory = $upload_info['dir'];
    $url = $upload_info['url'];
    do {
        $imagename_unique = "CustomLogo_" . uniqid() . '_' . $imagename;
        $imageFullPath = $directory . $imagename_unique;
        $imageFullUrl = $url . $imagename_unique;
    } while (file_exists($imageFullPath));

    if (move_uploaded_file($tmpFileName, $imageFullPath)) {
        $response['success'] = true;
        $response['file'] = $imagename_unique;
        $response['url'] = $imageFullUrl;
    }
    echo json_encode($response);
    wp_die();
}

function add_order_item_custom_logo_to_order($order_id)
{
    /*
     * 1. Get Order Object and all order items
     * 2. Check if any order item has customer Logo as meta (need to know which meta)
     * 3. If so, combine all files and zip and put in another folder
     * 4. Add images to to order Meta box as well as zip download link
     * 5. Send email to admin as attachment
     */
    ElmHelper::log("Start add_order_item_custom_logo_to_order $order_id >>");

    error_log("$order_id process started", 0);
    // get all custom logos from all order items of the order
    $order = new WC_Order($order_id);
    $items = $order->get_items();
    $logos = [];
    $attachments = [];
    foreach ($items as $item_id => $item) {
        $product_name = $item['name'];
        $product = $item->get_product();
        $productId = $product->get_id();

        $logo = $item->get_meta('Logo on Sock - Image');
        if (empty($logo)) {
            $logo = $item->get_meta('Logo on Sock');
        }
        $size = $item->get_meta('Sock Size');
        $color = $item->get_meta('Sock Base Color');


        ElmHelper::log(compact('product_name', 'productId', 'size', 'color', 'logo'));

        if (!empty($logo)) {

//            if (stripos($logo, 'CustomLogo_') !== false) {
                $logos[$item_id] = [
                    'product_name' => $product_name,
                    'product_id' => $productId,
                    'size' => $size,
                    'color' => $color,
                    'logo' => $logo,
                    'path' => getUploadedPathFromUrl($logo),
                    'file' => getFileNameFromUrl($logo),
                ];

                ElmHelper::log("LOGO DETAILS - $item_id");
                ElmHelper::log($logos[$item_id]);
//            }
        }

    }

    ElmHelper::log(!empty($logos) ? "LOGOS FOUND!" : "NO LOGO FOUND");
    if (!empty($logos)) {

        $finalUploadZipDirData = get_custom_logo_upload_dir('ordered_compressed');
        $finalFiles = [];
        $readme = ["Product Name,Size,Color,Logo File"];

        foreach ($logos as $order_item_id => $logoItem) {

            $readme[] = implode(",", [
                '"' . $logoItem['product_name'] . '"',
                '"' . $logoItem['size'] . '"',
                '"' . $logoItem['color'] . '"',
                '"' . $logoItem['logo'] . '"',
            ]);

            $finalFiles[$logoItem['file']] = $logoItem['path'];
            //add post metas
            add_post_meta($order_id, 'custom_logo_data', json_encode($logoItem));
            add_post_meta($order_id, 'custom_logo_url', $logoItem['logo']);
            add_post_meta($order_id, 'custom_logo', $logoItem['file']);
            add_post_meta($order_id, 'custom_logo_path', $logoItem['path']);
        }

        $readmeTxt = implode("\r\n", $readme);

        ElmHelper::log("CSV: ". $readmeTxt);
        ElmHelper::log("Final zip location calculated: ");
        ElmHelper::log($finalUploadZipDirData);

        // create zip of logo files
        $zipFileName = "customlogos_" . $order_id . ".zip";
        $zipFilePath = $finalUploadZipDirData['dir'] . $zipFileName;
        $zipFileUrl = $finalUploadZipDirData['url'] . $zipFileName;

        try {
            ElmHelper::log("Zippping...");
            $zip = new ZipArchive;
            $res = $zip->open($zipFilePath, ZipArchive::CREATE | ZipArchive::OVERWRITE);
            $zip->addFromString('OrderItems.csv', $readmeTxt);
            foreach ($finalFiles as $fileName => $filePath) {
                $zip->addFile($filePath, $fileName);
            }
            $zip->close();
            ElmHelper::log("Zippping done!");
        } catch (Exception $e) {
            echo 'Message: ' . $e->getMessage();
            ElmHelper::log("Zippping ERROR -" . $e->getMessage());
        }

        // update order post meta
        update_post_meta($order_id, 'custom_logo_zip_url', $zipFileUrl);
        update_post_meta($order_id, 'custom_logo_zip_path', $zipFilePath);


    }

    ElmHelper::log("goto : add_specform_images_to_order");
    $specFormArchiveDetails = add_specform_images_to_order($order_id);
    ElmHelper::log("done : add_specform_images_to_order");

}


function add_specform_images_to_order($order_id)
{
    ElmHelper::log("START : add_specform_images_to_order");
    $archiveDetails = null;
    $order = new WC_Order($order_id);
    $items = $order->get_items();
    $imageSavedPaths = []; // file path to saved images
    $imageSavedPathsFlat = [];
    $imageSavedUrls = []; // urls of saved images
    $imageSavedUrlsFlat = [];
    $imageSourceUrls = []; // urls of source (liquid pixels) images
    $imageSourceUrlsFlat = [];

    foreach ($items as $item_id => $item) {
        $product_name = $item['name'];
        $imageSavedPaths[$product_name . '-' . $item_id] = []; // file path to saved images
        $imageSavedUrls[$product_name . '-' . $item_id] = []; // urls of saved images
        $imageSourceUrls[$product_name . '-' . $item_id] = []; // urls of source (liquid pixels) images

        $mainUrl = $item->get_meta('Preview Details - Preview Details');
        if (empty($mainUrl)) {
            $mainUrl = $item->get_meta('Preview Details');
        }
        ElmHelper::log("LiquidPixels Source Image URL: ");
        ElmHelper::log($mainUrl);
        $hasSpecFormImages = !empty($mainUrl);

        ElmHelper::log(!empty($mainUrl) ? "GOT SPEC IMAGE URL - $mainUrl": "DONOT GET SPEC IMAGE URL");
        if (!empty($mainUrl)) {
            update_post_meta($order_id, 'specform_preview_main_url', $mainUrl);
            $faces = ['left', 'right', 'front', 'back'];
            $shownImages = [];
            foreach ($faces as $face) {
                $saveUrl = strpos($mainUrl, 'https://') === 0 ? $mainUrl : urldecode($mainUrl);
                $saveUrl = preg_replace('/face\%5B.+?\%5D/', "face%5B{$face}%5D", $saveUrl);
                $key = $product_name . '-' . $item_id;
                $flatKey = $key . '-' . $face;
                $file = $flatKey . '.jpg';
                $path = 'orders' . DS . 'order-' . $order_id . DS . 'spec-images';
                $imageDetails = ElmHelper::copyFileFromUrl($saveUrl, $file, $path);
                $imageSavedPathsFlat[$flatKey] = $imageSavedPaths[$key][$face] = $imageSavedPath = $imageDetails['path'];
                $imageSavedUrlsFlat[$flatKey] = $imageSavedUrls[$key][$face] = $imageSavedUrl = $imageDetails['url'];
                $imageSourceUrlsFlat[$flatKey] = $imageSourceUrls[$key][$face] = $mainUrl;
                $shownImages[] = "<img width='50' src='{$imageSavedUrl}' />";
            }

            ElmHelper::log("END : add_specform_images_to_order - IMAGES:");
            ElmHelper::log($shownImages);

            ElmHelper::log("UPDATE ITEM META ($item_id) -  Preview Details - Preview Details to blank");

            wc_update_order_item_meta($item_id, 'Preview Details - Preview Details', '');
            wc_update_order_item_meta($item_id, 'Preview Details', '');

            ElmHelper::log("DONE UPDATE ITEM META");

        }
    }
    if (!empty($imageSavedPathsFlat)) {
        ElmHelper::log("Generate Zip " );
        //ElmHelper::log($imageSavedPathsFlat);
        $zipPath = 'orders' . DS . 'order-' . $order_id;
        $zipFileName = 'spec-form-images-' . $order_id . '.zip';
        $archiveDetails = ElmHelper::archiveFiles($imageSavedPathsFlat, $zipFileName, $zipPath);
        //ElmHelper::log($archiveDetails);
        update_post_meta($order_id, 'specform_images_zip_path', $archiveDetails['path']);
        update_post_meta($order_id, 'specform_images_zip_url', $archiveDetails['url']);
        update_post_meta($order_id, 'specform_images_source', $imageSourceUrls);
        update_post_meta($order_id, 'specform_images_paths', $imageSavedPaths);
        update_post_meta($order_id, 'specform_images_urls', $imageSavedUrls);

        ElmHelper::log("SAVED META" );
    }
    return $archiveDetails;
}

function add_order_logos_to_mail($attachments, $status, $order)
{
    ElmHelper::log("START : add_order_logos_to_mail **** THIS FUCNTION MUST NOT START");
    $orderId = $order->id;
    $zip = get_post_meta($orderId, 'custom_logo_zip_path', true);
    error_log("EMAIL ATTACHMENT: " . $zip, 0);
    $allowed_statuses = ['new_order', 'customer_invoice', 'customer_processing_order', 'customer_completed_order'];
    if (!empty($zip) && isset($status) && in_array($status, $allowed_statuses)) {
        $attachments[] = $zip;
    }

    ElmHelper::log("END : add_order_logos_to_mail - attachments:");
    ElmHelper::log($attachments);

    return $attachments;
}

function order_post_logo_callback()
{
    ElmHelper::log("ADD  METABOX" );
    global $post;
    $orderID = $post->ID;
    error_log("ORDER POST METABOX: " . $orderID, 0);
    $logos = get_post_meta($orderID, 'custom_logo_data');
    $zip = get_post_meta($orderID, 'custom_logo_zip_url', true);
    $html = "";
    if (!empty($logos) || !empty($zip)) {
        $html .= "<div class='metaBoxCustomContainer'>";
        $html .= "<h4 class='metaxBoxCustomTitle'>Custom Logo</h4>";
        if ($logos) {
            foreach ($logos as $logo) {
                if (!empty($logo)) {
                    $logoItem = json_decode(str_replace('\\', '\\\\', $logo), true);
                    if (!empty($logoItem['logo'])) {
                        $html .= "<div class='metaBoxCustomLogoContainer'>
                                    <div class='metaxBoxCustomTitle metaxBoxCustomLogoTitle'>
                                        <strong>
                                          {$logoItem['product_name']}
                                        </strong>
                                        <small>(Size:{$logoItem['size']}, Color: {$logoItem['color']})</small>
                                     </div>
                                      <div class='metaxBoxCustomLogoImageContainer'>
                                         <a href='{$logoItem['logo']}' target='_blank'>
                                            <img src='{$logoItem['logo']}'/>
                                        </a>
                                       </div>
									</div>";
                    }
                }
            }
        }
        if ($zip) {
            $html .= "<p class='metaBoxCustomLogoZipContainer'>
                    <strong><a class='button button-primary metaBoxCustomLogoZip' 
                    href='{$zip}' target='_blank'>Download Custom Logo</a></strong></p>";
        }
        $html .= '</div>';
    }
    echo $html;

    ElmHelper::log("START SPEC DOWNKLOAD LINK IN METABOX" );
    add_specform_images_download_link($orderID);
    ElmHelper::log("END SPEC DOWNKLOAD LINK IN METABOX" );

    ElmHelper::log("END ADD  METABOX" );

}

function add_specform_images_download_link($orderID = null)
{
    ElmHelper::log("START EMAIL SPEC FORM DOWNLOAD LINK" );
    if (empty($orderID)) {
        global $post;
        $orderID = $post->ID;
    }
    $zip = get_post_meta($orderID, 'specform_images_zip_url', true);
    if (!empty($zip)) {
        echo "<div class='metaBoxCustomContainer'>";
        echo "<h4 class='metaxBoxCustomTitle'>Spec Form Images</h4>";
        echo "<p class=''><strong><a class='button button-primary' href='{$zip}' target='_blank'>Download Spec Form Design</a></strong></p>";
        echo "</div>";
        ElmHelper::log("DOWNLOAD LINK: $zip" );
    }
    ElmHelper::log("START EMAIL SPEC FORM DOWNLOAD LINK" );
}


/*----------------------------------------------------------------*\
		ADD ORDER NOTE WITH DESIGNER/SPEC FORM USE
\*----------------------------------------------------------------*/
function add_order_note_designer_specform($order_id)
{
    ElmHelper::log("START add_order_note_designer_specform" );
    $order = new WC_Order($order_id);
    $items = $order->get_items();
    $isSpecForm = false;
    foreach ($items as $item_id => $item) {
        $isLP = get_field('enable_liquid_pixels', $item_id);
        if ($isLP) {
            $isSpecForm = $isLP;
            break;
        }
    }
    $source = [];

    if ($isSpecForm) {
        $source[] = "Spec Form";
    }
    $note = "Order submitted via " . (implode(" and ", $source));
    $order->add_order_note($note);

    ElmHelper::log("END  add_order_note_designer_specform" );
}

/*----------------------------------------------------------------*\
    ADD ORDER EMAIL WITH DESIGNER/SPEC FORM USE
\*----------------------------------------------------------------*/
/**
 * @param $order \WC_Order
 * @param $isSentToAdmin
 */
function add_order_email_designer_specform($order, $isSentToAdmin)
{
    ElmHelper::log("START add_order_email_designer_specform" );

    if ($isSentToAdmin) {
        $items = $order->get_items();

        $isSpecForm = false;
        $isSpecFormLogo = false;
        foreach ($items as $item_id => $item) {
            $isLP = get_field('enable_liquid_pixels', $item_id);
            if ($isLP) {
                // $isSpecForm = $isLP;
                $logo = $item->get_meta('Logo on Sock - Image');
                if (empty($logo)) {
                    $logo = $item->get_meta('Logo on Sock');
                }
                if (!empty($logo)) {
                    $isSpecFormLogo = true;
                    break;
                }

            }
        }
        $source = [];

        if ($isSpecForm) {
            $source[] = "Spec Form";
        }
        if (!empty($source)) {
            echo "<p><strong>Order submitted via " . implode(" and ", $source) . ".</strong></p>";
        }
        if ($isSpecFormLogo) {
            echo "<p><strong>Spec Form Logo Uploaded.</strong></p>";
        }

        ElmHelper::log("END add_order_email_designer_specform" );
    }
}

//add_action( 'woocommerce_order_again_cart_item_data', 'add_order_email_designer_specform', 10, 2);

/*----------------------------------------------------------------*\
    ADD LOGO TO ORDER EMAIL
\*----------------------------------------------------------------*/
function wdm_add_logo_url_to_order_email($order, $is_admin_email)
{

     ElmHelper::log("START wdm_add_logo_url_to_order_email" );


    $order_id = $order->id;
    $speclogourl = get_post_meta($order_id, 'custom_logo_zip_url', true);
    if (!empty($speclogourl)) {

        echo '<table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top; margin: 10px 0 20px 0; padding: 0; border-collapse: collapse; border:1px solid #0c99ae;" border="0">
	        <tr>
	            <td  style="border: 0; padding: 10px; border-collapse: collapse; border-bottom:1px solid #0c99ae;" valign="middle">
		            <h2 style="color: #0c99ae; margin: 20px 0 20px 18px; ">Download Logo Files</h2>
	            </td>
	        </tr>
	        <tr>
	            <td  style="border: 0; padding: 10px; border-collapse: collapse;" valign="middle">
		            <a href="' . $speclogourl . '" >Click here to download Logo zip file</a>		
	            </td>
	        </tr>	
        </table>';

        ElmHelper::log("EMAIL LINK ADDD" );
    }

    ElmHelper::log("END  wdm_add_logo_url_to_order_email" );

}

function wdm_add_specform_images_url_order_email($order, $is_admin_email)
{

    ElmHelper::log("START wdm_add_specform_images_url_order_email" );

    $order_id = $order->id;
    $specImagesUrl = get_post_meta($order_id, 'specform_images_zip_url', true);
    if (!empty($specImagesUrl)) {

        echo '<table cellspacing="0" cellpadding="0" style="width: 100%; vertical-align: top; margin: 10px 0 20px 0; padding: 0; border-collapse: collapse; border:1px solid #0c99ae;" border="0">
	        <tr>
	            <td  style="border: 0; padding: 10px; border-collapse: collapse; border-bottom:1px solid #0c99ae;" valign="middle">
		            <h2 style="color: #0c99ae; margin: 20px 0 20px 18px; ">Spec Form Design</h2>
	            </td>
	        </tr>
	        <tr>
	            <td  style="border: 0; padding: 10px; border-collapse: collapse;" valign="middle">
		            <a href="' . $specImagesUrl . '" >Click here to download Spec Form Design zip</a>		
	            </td>
	        </tr>	
        </table>';

        ElmHelper::log("EMAIL LINK ADDED for SPEC FORM" );
    }

    ElmHelper::log("END wdm_add_specform_images_url_order_email" );
}

/**
 * @param $order \WC_Order
 * @param $sent_to_admin
 * @param $plain_text
 * @param $email
 *
 * @return mixed
 */
function woocommerce_email_order_meta_fields_skip_preview_value($order, $sent_to_admin, $plain_text)
{
    ElmHelper::log("woocommerce_email_order_meta_fields_skip_preview_value" );
    $items = $order->get_items();
    foreach ($items as $item_id => $item) {
        $item->delete_meta_data('Preview Details - Preview Details');
        $item->delete_meta_data('Preview Details');
    }
}

add_filter( 'woocommerce_email_before_order_table', 'woocommerce_email_order_meta_fields_skip_preview_value', 20, 3 );

/*
 * Add Product Meta to frontend product page for JS access to help LiquidPixels implementation
 */
function wclp_add_product_meta()
{
    global $product;
    echo "\r\n<input name='liquidpixels_product_categories' id='liquidpixels_product_categories' type='hidden' value='";
    $termList = [];
    $terms = get_the_terms($product->id, 'product_cat');
    foreach ($terms as $term) {
        $slug = str_replace('knee-high', 'kneehigh', $term->slug);
        $slug = str_replace('leg-warmer', 'legwarmer', $term->slug);
        $termList[] = str_replace('-', ',', $slug);
    }
    echo implode(',', $termList);
    echo "'/>\r\n";
    $face = 'left';
    $frontFacers = ['beanie', 'scarf'];
    foreach($frontFacers as $facer) {
        if (in_array($facer, $termList)) {
            $face = 'front';
        }
    }

    echo "\r\n<input name='liquidpixels_product_face' id='liquidpixels_product_face' type='hidden' value='" . $face . "' />\r\n";
    echo "\r\n<input name='enable_liquid_pixels' id='enable_liquid_pixels' type='hidden' value='" .
        get_field('enable_liquid_pixels') . "' />\r\n";

    // add LiquidPixel revision for deleting cache on releases/fixes
    // The field is kept hidden from edit pages to ensure a single global default value
    // TO change the value during release, change the field' default value
    $revisionName = 'liquid_pixels_revision';
    $revisionField = acf_get_field($revisionName);
    $revisionValue = $revisionField['default_value'];
    echo "\r\n<input name='".$revisionName."' id='".$revisionName."' type='hidden' value='".$revisionValue."' />\r\n";
}

function cs_logo_meta_box()
{

    $screens = ['shop_order'];

    foreach ($screens as $screen) {
        add_meta_box('order_post_logo', __('Order Attachments', ''),
            'order_post_logo_callback', $screen, 'side', 'default');
    }
}

add_action('add_meta_boxes', 'cs_logo_meta_box');


add_action('woocommerce_order_status_processing', 'add_order_note_designer_specform');


add_action('wp_ajax_delete_custom_logo', 'delete_custom_logo');
add_action('wp_ajax_upload_logo_image', 'upload_logo_image');
add_action('wp_ajax_nopriv_delete_custom_logo', 'delete_custom_logo');
add_action('wp_ajax_nopriv_upload_logo_image', 'upload_logo_image');
add_action('woocommerce_order_status_processing', 'add_order_item_custom_logo_to_order');
//add_filter('woocommerce_email_attachments', 'add_order_logos_to_mail',10,3);

add_action('woocommerce_before_add_to_cart_button', 'wclp_add_product_meta');

add_action('woocommerce_email_after_order_table', 'wdm_add_logo_url_to_order_email', 15, 2);
add_action('woocommerce_email_after_order_table', 'wdm_add_specform_images_url_order_email', 20, 2);

add_filter( 'get_product_addons', 'update_product_addon_field_names', 5);

function update_product_addon_field_names($addons)
{
    foreach($addons as $key => $item) {
        $fieldName = $item['field_name'];
        $fieldName = preg_replace('/-\d+?$/', '', $fieldName);
        $addons[$key]['field_name'] = $fieldName;
    }
    return $addons;
}
