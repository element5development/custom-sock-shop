<?php

/*----------------------------------------------------------------*\
	GAVITY FORM TAB INDEX FIX
\*----------------------------------------------------------------*/
add_filter("gform_tabindex", function () {
	return false;
});

/*----------------------------------------------------------------*\
	ENABLE LABEL VISABILITY
\*----------------------------------------------------------------*/
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/*----------------------------------------------------------------*\
	DEFAULT QUANTITY
\*----------------------------------------------------------------*/
function default_quantity() {
    return 0;
}
add_filter( 'gform_field_value_default_quantity', 'default_quantity' );

?>