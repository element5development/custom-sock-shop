<?php

	/*----------------------------------------------------------------*\
		BUTTON
	\*----------------------------------------------------------------*/
	function custom_button( $atts, $content = null ) {
    extract(shortcode_atts(array(
    'url'	=> '',
    'target'	=> '',
    'color'	=> '',
    'size'	=> '',
    'align'	=> '',
    'type' 	=> '',
    'download' => '',
    'fancybox' => '',
    ), $atts));
		$color = ($color) ? ' '.$color. '' : '';
		$align = ($align) ? ' '.$align. '' : '';
		$size = ($size) ? ' '.$size. '' : '';
		$fancybox = ($fancybox == 'true') ? ' '.$fancybox. '' : '';
		$target = ($target == '_blank') ? ' target="_blank"' : '';
		$type = ($type) ? ' '.$type. '' : '';
		$download = ($download == 'true') ? ' download' : '';
		$out = '<a' .$download.$target. ' class="btn' .$color.$size.$align.$type.$fancybox. '" href="' .$url. '">' .do_shortcode($content). '</a>';
    return $out;
	}
	add_shortcode('button', 'custom_button');
	/*----------------------------------------------------------------*\
		ACCORDION
	\*----------------------------------------------------------------*/
	function custom_accordion($atts, $content = null) {
		extract( shortcode_atts( array(
			'title' => '',
		), $atts ) );
		$accordion = '<span class="collapseomatic" tabindex="0" title="'.$title.'">'.$title.'</span><div class="collapseomatic_content">' . do_shortcode($content) . '</div>';
		return $accordion;
	}
	add_shortcode('expand', 'custom_accordion');
	/*----------------------------------------------------------------*\
		SOCIAL PROFILES
	\*----------------------------------------------------------------*/
	function facebook_social( $atts ) {
		$html = '<a class="social fb" href="https://www.facebook.com/CustomSockShop" target="_blank"><span>Facebook</span></a>';
		return $html;
	}
	function linkedin_social( $atts ) {
		$html = '<a class="social linkedin" href="https://www.linkedin.com/company/customsockshop" target="_blank">Linkedin</a>';
		return $html;
	}
	function instagram_social( $atts ) {
		$html = '<a class="social instagram" href="https://instagram.com/customsockshop/" target="_blank">Instagram</a>';
		return $html;
	}
	function pinterest_social( $atts ) {
		$html = '<a class="social pinterest" href="https://www.pinterest.com/customsocksshop/" target="_blank">Pinterest</a>';
		return $html;
	}
	add_shortcode('facebook', 'facebook_social');
	add_shortcode('linkedin', 'linkedin_social');
	add_shortcode('instagram', 'instagram_social');
	add_shortcode('pinterest', 'pinterest_social');
?>