<?php

	/*----------------------------------------------------------------*\
		REDIRECT TO CHECKOUT
	\*----------------------------------------------------------------*/
	function woo_redirect_to_checkout() {
		global $woocommerce;
		$checkout_url = $woocommerce->cart->get_checkout_url();
		return $checkout_url;
	}
	add_filter ('add_to_cart_redirect', 'woo_redirect_to_checkout');
	/*----------------------------------------------------------------*\
		ENABLE PRODUCT GALLERY OPTIONS
	\*----------------------------------------------------------------*/
	add_theme_support( 'wc-product-gallery-lightbox' );
	/*----------------------------------------------------------------*\
		REMOVE DEFAULT SORTING
	\*----------------------------------------------------------------*/
	remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
	/*----------------------------------------------------------------*\
		DISABLE WOOCOMMERCE RELATED PRODUCTS
	\*----------------------------------------------------------------*/
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
	/*----------------------------------------------------------------*\
		RELOCATE SINGLE PRODUCT TABS
	\*----------------------------------------------------------------*/
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
	/*----------------------------------------------------------------*\
		REMOVE ADDITIONAL INFORMATION TAB
	\*----------------------------------------------------------------*/
	add_filter( 'woocommerce_product_tabs', 'remove_product_tabs', 98 );
	function remove_product_tabs( $tabs ) {
		unset( $tabs['additional_information'] ); 
		return $tabs;
	}
	/*----------------------------------------------------------------*\
		REMOVE REVIEWS TAB
	\*----------------------------------------------------------------*/
	function remove_reviews_tab($tabs) {
		unset($tabs['reviews']);
		return $tabs;
	}
	add_filter( 'woocommerce_product_tabs', 'remove_reviews_tab', 98);
	/*----------------------------------------------------------------*\
		BACK TO PRODUCTS LINK
	\*----------------------------------------------------------------*/
	// function back_to_products() {
	// 	echo "<a href='/custom-made-socks-onboarding/' class='back'>« Back to Products</a>";
	// }
	// add_action( 'woocommerce_single_product_summary', 'back_to_products', 35 );
	/*----------------------------------------------------------------*\
		DISABLE ADMIN BAR FOR CUSTOMER ACCOUNTS
	\*----------------------------------------------------------------*/
	function remove_admin_bar() {
		if (!current_user_can('administrator') && !is_admin()) {
			show_admin_bar(false);
		}
	}
	add_action('after_setup_theme', 'remove_admin_bar');
	/*----------------------------------------------------------------*\
		REMOVE COMPANY NAME FROM CHECKOUT
	\*----------------------------------------------------------------*/
	function custom_override_checkout_fields( $fields ) {
		unset($fields['billing']['billing_company']);
		unset($fields['shipping']['shipping_company']);
		return $fields;
	}
	add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
	/*----------------------------------------------------------------*\
		NO SHIPPING MESSAGE
	\*----------------------------------------------------------------*/
	function change_msg_no_available_shipping_methods( $default_msg ) {
		$custom_msg = "Please continue to the checkout and enter your full address to see if there are any available shipping methods.";
		if( empty( $custom_msg ) ) {
			return $default_msg;
		}
		return $custom_msg;
	}
	add_filter( 'woocommerce_cart_no_shipping_available_html', 'change_msg_no_available_shipping_methods', 10, 1  );
	add_filter( 'woocommerce_no_shipping_available_html', 'change_msg_no_available_shipping_methods', 10, 1 );
	/*----------------------------------------------------------------*\
		ADD BCC TO EMAIL NOTIFICATIONS
	\*----------------------------------------------------------------*/
	function mycustom_headers_filter_function( $headers, $object ) {
		if ($object == 'customer_completed_order') {
				$headers .= 'BCC: Trust Pilot <1801749662@trustpilotservice.com>' . "\r\n";
		}
		return $headers;
	}
	add_filter( 'woocommerce_email_headers', 'mycustom_headers_filter_function', 10, 2);
	/*----------------------------------------------------------------*\
		HIDE SIDEBAR ON SHOP
	\*----------------------------------------------------------------*/
	function disable_woo_commerce_sidebar() {
		remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10); 
	}
	add_action('init', 'disable_woo_commerce_sidebar');
	/*----------------------------------------------------------------*\
		RETURN TO SHOP
	\*----------------------------------------------------------------*/
	function wc_empty_cart_redirect_url() {
		return '/custom-made-socks/';
	}
	add_filter( 'woocommerce_return_to_shop_redirect', 'wc_empty_cart_redirect_url' );
	