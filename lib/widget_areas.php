<?php

/*----------------------------------------------------------------*\

	CUSTOM WIDGET AREAS
	www.wp-hasty.com

\*----------------------------------------------------------------*/
if ( function_exists('register_sidebar' )) {
	function html5reset_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Footer Contact' ),
			'id'            => 'footer-contact',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer Newsletter' ),
			'id'            => 'footer-newsletter',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer Menu One' ),
			'id'            => 'footer-menu-one',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer Menu Two' ),
			'id'            => 'footer-menu-two',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
		register_sidebar( array(
			'name'          => __( 'Footer Menu Three' ),
			'id'            => 'footer-menu-three',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
	add_action( 'widgets_init', 'html5reset_widgets_init' );
}

?>