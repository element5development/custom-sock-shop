<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

global $post;	
$terms = wp_get_post_terms( $post->ID, 'product_cat' );
foreach ( $terms as $term ) $categories[] = $term->slug;

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php if ( in_array( 'dress', $categories ) ):  ?>
			<?php if ( get_field('special_offer', 10776) ) : //dress socks landing  ?>
				<section class="special">
					<h3><?php the_field('special_offer', 10776); ?></h3>
				</section>
			<?php endif; ?>
		<?php endif; ?>

		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
		) );

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>

		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

		<?php if ( in_array( 'dress', $categories ) ):  ?>
			<span id="large-order-call">
				<b>Production will not begin without your written approval.</b>
				<br>Do you want to order over 800 pairs?
				<br>Then please call us at <a href="tel:+18885579185">(888)-557-9185</a> to place your custom order and get even larger discounts!
			</span>
			<!-- 
			REMOVED AS PLUGIN CAN DISPLAY PRICING TABLE NOW
			<div class="pricing-table">
				<div class="pricing-card">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dress-socks-1.svg">
					<h3>15-35 pairs</h3>
					<p>$15.99/pair</p>
				</div>
				<div class="pricing-card"> 
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dress-socks-1.svg">
					<h3>36-50 pairs</h3>
					<p>$12.99/pair</p>
				</div>
				<div class="pricing-card"> 
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dress-socks-2.svg">
					<h3>51-100 pairs</h3>
					<p>$11.99/pair</p>
				</div>
				<div class="pricing-card"> 
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dress-socks-2.svg">
					<h3>101-150 pairs</h3>
					<p>$9.99/pair</p>
				</div>
				<div class="pricing-card"> 
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dress-socks-3.svg">
					<h3>151-200 pairs</h3>
					<p>$8.99/pair</p>
				</div>
				<div class="pricing-card"> 
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dress-socks-3.svg">
					<h3>201-300 pairs</h3>
					<p>$7.99/pair</p>
				</div>
				<div class="pricing-card"> 
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dress-socks-4.svg">
					<h3>301-800 pairs</h3>
					<p>$6.99/pair</p>
				</div>
				<div class="pricing-card"> 
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/dress-socks-4.svg">
					<h3>800+ pairs</h3>
					<p>Call for pricing</p>
				</div>
			</div> 
			-->
		<?php else : ?>
			<span id="large-order-call">
				<b>Production will not begin without your written approval.</b>
			</span>
		<?php endif; ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
