<?php 
/*-------------------------------------------------------------------
404 Page
------------------------------------------------------------------*/
?>

<?php get_header(); ?>
<div class="wrap">
	<h2>
		<?php _e('Error 404 - Page Not Found','html5reset'); ?>
	</h2>
	<p>We're sorry, but the page you're requesting cannot be found. Please
		<a href="<?php echo home_url(); ?>">click here</a> to return to the home page and try again.</p>
</div>

<?php get_footer(); ?>