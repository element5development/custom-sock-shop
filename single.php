<?php get_header(); ?>
<main>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<div class="entry-content">
				<?php the_content(); ?>
				<?php wp_link_pages(array('before' => __('Pages: '), 'next_or_number' => 'number')); ?>
			</div>
		</article>
		<?php comments_template(); ?>
	<?php endwhile; endif; ?>
</main>

<?php get_footer(); ?>

