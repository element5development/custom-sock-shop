<?php get_header(); ?>

 <div class="wrap">
	<section id="socklife">
	 	<div class="slcontainer">	
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="post">
				<div class="entry">
					<?php if ( has_post_thumbnail() ) { ?>
					<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('socklife'); ?></a>
					<?php } else { ?>
					<a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_directory'); ?>/dist/images/default.jpg" alt="<?php the_title(); ?>" /></a>
					<?php } ?>
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<span class="date"><?php the_time('F j, Y'); ?></span>
				</div>
			</article>
			<?php endwhile; ?>
			<?php else : ?>
				<h2><?php _e('Nothing Found','html5reset'); ?></h2>
			<?php endif; ?>
			</div>
	</section>
</div>

<div class="postlinks">
	<div class="wrap">
		<?php post_navigation(); ?>
	</div>
</div>

<?php get_footer(); ?>

