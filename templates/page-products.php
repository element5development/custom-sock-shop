<?php
/**
 * Template Name: Products
 */
 get_header(); ?>

<!-- SUBHEADING -->
<?php if( get_field('page_subheading') ): ?>
	<?php get_template_part('template-parts/subheading'); ?>
<?php endif; ?>

<!-- Athletic + Sub Categories -->
 <?php if ( is_page() && ($post->post_parent==987||is_page(987) ) ) : ?>
		<section id="athleticcat">
			<div class="wrap">
				<ul id="subcategories">
					<?php wp_list_pages("title_li=&child_of=987" ); ?>
					<li class="purple"><a href="<?php echo get_permalink(989); ?>">Custom Marketing Socks</a></li>
					<li class="request"><a href="<?php echo get_permalink(4631); ?>">Request a Sample</a></li>
				</ul>
			</div>
		</section>
<!-- Custom Made Sub Categories -->
<?php elseif ( is_page() && ($post->post_parent==989||is_page(989) ) ) : ?>
	<section id="customcat">
		<div class="wrap">
			<ul id="subcategories">
				<?php wp_list_pages("title_li=&child_of=989" ); ?>
				<li class="purple"><a href="<?php echo get_permalink(987); ?>">Custom Athletic Socks</a></li>
				<li class="request"><a href="<?php echo get_permalink(4631); ?>">Request a Sample</a></li>
			</ul>
		</div>
	</section>
<?php else : ?>
	<!-- NOTHING -->
<?php endif; ?>

<!-- DEFAULT CONTENT -->
<div id="wooproducts">
	<div class="wrap">
		<section id="fullwidth">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article class="post" id="post-<?php the_ID(); ?>">
					<div class="entry">
						<?php the_content(); ?>
					</div>
				</article>
			<?php endwhile; endif; ?>
		</section>
	</div>
</div>

<?php get_footer(); ?>
