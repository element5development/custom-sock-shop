<?php
/**
 * Template Name: Front Page
 */
?>

<?php get_header(); ?>

<?php
	$bgimage = get_field('header_image');
?>
<?php if ( $bgimage ) : ?>
	<header class="front-title single-image<?php if ( get_field('notification_bar_text', 'options') ) : ?> notification-on<?php endif; ?>" style="background-image: url('<?php echo $bgimage['url']; ?>');">
		<div>
			<h1><?php the_field('tagline'); ?></h1>
			<p><?php the_field('description'); ?></p>
			<?php 
				$link = get_field('button'); 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="btn" href="<?php echo esc_url($link['url']); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		</div>
	</header>
<?php else : ?>
	<header class="front-title<?php if ( get_field('notification_bar_text', 'options') ) : ?> notification-on<?php endif; ?>">
		<?php
			$socks = get_field('header_images');
			$i = 1;
		?>
		<?php foreach( $socks as $sock ): ?>
		<?php if ( $i == 11 ) : ?>
		<div class="sock">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/css-logo-teal.svg" alt="Custom Sock Shop Logo" />
		</div>
		<?php elseif ( $i == 21 ) : ?>
		<div class="sock title">
			<h1><?php the_field('tagline'); ?></h1>
			<?php 
						$link = get_field('button'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
			<a class="btn" href="<?php echo esc_url($link['url']); ?>"
				target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
		</div>
		<?php else : ?>
		<div class="sock">
			<img src="<?php echo $sock['sizes']['medium']; ?>" alt="<?php echo $sock['alt']; ?>" />
		</div>
		<?php endif; ?>
		<?php $i++; endforeach; ?>
	</header>
<?php endif; ?>

<?php if( get_field('intro_video') & get_field('intro_text') ): ?>
	<section class="intro">
		<div class="video">
			<?php the_field('intro_video'); ?>
		</div>
		<div class="text-content">
			<?php the_field('intro_text'); ?>
		</div>
		<?php $clients = get_field('client_logos'); ?>
		<?php if( $clients ): ?>
		<div class="clients">
			<h3>Clients we've worked with</h3>
			<ul>
				<?php foreach( $clients as $client ): ?>
				<li>
					<img src="<?php echo $client['sizes']['medium']; ?>" alt="<?php echo $client['alt']; ?>" />
				</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php endif; ?>
	</section>
<?php endif; ?>
<div id="feefo-service-review-carousel-widgetId" class="feefo-review-carousel-widget-service"></div>
<?php if( have_rows('key_point_columns') ): ?>
	<section class="key-point-columns">
		<h2>Common Questions</h2>
		<div class="key-points">
			<?php while ( have_rows('key_point_columns') ) : the_row(); ?>
				<div class="key-point">
					<h3><?php the_sub_field('heading'); ?></h3>
					<p><?php the_sub_field('description'); ?></p>
				</div>
			<?php endwhile; ?>
		</div>
		<div class="actions">
			<a href="tel:+18885579185" class="btn purple">Talk to a human</a>
			<a href="<?php the_permalink(14); ?>" class="btn teal">View More Questions</a>
		</div>
	</section>
<?php endif; ?>
<?php if( have_rows('styles') ): ?>
	<section class="shop-style">
		<h2>Custom socks for any occasion. Shop by style:<span>What's the difference?</span></h2>
		<p>So you might be wondering, what’s the difference between athletic, marketing and dress/business use socks? They all
			appear to have the same length cuts but different prices. Without getting too technical, below will be a brief
			description to try and help guide you into the perfect pair for you desired use:
			<button class="comparison" data-featherlight="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/CompChart.png">Sock Comparison Chart</button>
		</p>

		<div class="styles">
			<?php while ( have_rows('styles') ) : the_row(); ?>
			<div class="style">
				<?php if ( get_sub_field('background_image') ) : ?>
				<?php $image = get_sub_field('background_image'); ?>
				<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>">
				<?php endif; ?>
				<h3><?php the_sub_field('heading'); ?></h3>
				<p><?php the_sub_field('description'); ?></p>
				<?php 
							$link = get_sub_field('button');
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
				<a class="btn" href="<?php echo esc_url($link_url); ?>"
					target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			</div>
			<?php endwhile; ?>
		</div>
		<div class="featured-style">
			<h3><?php the_field('featured_style_title'); ?></h3>
			<p><?php the_field('featured_style_description'); ?></p>
			<?php $link = get_field('featured_style_button'); ?>
			<?php if( $link ): ?>
			<a class="btn teal" href="<?php echo $link['url']; ?>"
				target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
			<?php endif; ?>
		</div>
	</section>
<?php endif; ?>
<!-- 
	removed to correct user flow
	<section class="start-blank">
		<div>
			<h2>Start your design from a blank sock</h2>
			<p>Don’t worry it’s all custom so design your own sock today and receive an art proof within 48 hours for free.</p>
			<div class="templates">
				<a href="/product/custom-footie-socks/" class="template">
					<svg data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 70">
						<path
							d="M37.37 33.06c-.58 3.62-5 4.19-7.59 4.32S23.94 39.6 22.3 41s-5.61 3.15-11.45 3.15-8.29-.94-8.29-2.69 3.5-2.33 5.26-2.79 5.49-2.69 8.64-5.85a57.38 57.38 0 0 0 4.19-4.57c8.82.13 13.92-1.85 15.19-2.4.96 2.46 1.85 5.21 1.53 7.21z"
							fill="#95D1D9" /></svg>
					Footie
				</a>
				<a href="/product/custom-anklet-socks/" class="template">
					<svg data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 70">
						<path
							d="M37.37 37.51c-.58 3.62-5 4.2-7.59 4.32s-5.84 2.22-7.48 3.62-5.61 3.16-11.45 3.16-8.29-.94-8.29-2.69 3.5-2.34 5.26-2.8 5.49-2.69 8.64-5.85 5.84-6.19 6.42-8.41a15.94 15.94 0 0 0 .45-5.72 72 72 0 0 0 10.19-1.3 24.84 24.84 0 0 0 .48 3.64c.57 2.33 4 8.41 3.37 12.03z"
							fill="#95D1D9" /></svg>
					Anklet
				</a>
				<a href="/product/custom-crew-socks/" class="template">
					<svg data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 70">
						<path
							d="M18.56 14.38a66.27 66.27 0 0 1 14.37-2.22c.12 12.27.47 20.21 1 22.55s4 8.41 3.39 12-5 4.21-7.59 4.32-5.84 2.22-7.48 3.62-5.6 3.16-11.45 3.16-8.29-.94-8.29-2.69 3.51-2.34 5.26-2.8 5.49-2.69 8.64-5.84 5.84-6.19 6.43-8.41 1.05-5.38-.82-13.2-3.46-10.49-3.46-10.49z"
							fill="#95D1D9" /></svg>
					Crew
				</a>
				<a href="/product/custom-elite-socks/" class="template">
					<svg data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 70">
						<path
							d="M18.56 14.38a66.27 66.27 0 0 1 14.37-2.22c.12 12.27.47 20.21 1 22.55s4 8.41 3.39 12-5 4.21-7.59 4.32-5.84 2.22-7.48 3.62-5.6 3.16-11.45 3.16-8.29-.94-8.29-2.69 3.51-2.34 5.26-2.8 5.49-2.69 8.64-5.84 5.84-6.19 6.43-8.41 1.05-5.38-.82-13.2-3.46-10.49-3.46-10.49z"
							fill="#6fc1cc" /></svg>
					Elite
				</a>
				<a href="/product/custom-knee-high-socks/" class="template">
					<svg data-name="Layer 2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 70">
						<path
							d="M12.44 4.24S18.76.52 30.11 1C33.46 14.38 33.4 43.56 34 45.89s4 8.41 3.39 12-5 4.2-7.59 4.32-5.84 2.22-7.48 3.62S16.7 69 10.85 69s-8.29-.93-8.29-2.68S6.07 64 7.82 63.53s5.49-2.68 8.64-5.84 5.84-6.19 6.43-8.41.48-8.26-2-15.92c-3.71-11.53-8.45-29.12-8.45-29.12z"
							fill="#95D1D9" /></svg>
					Knee High
				</a>
			</div>
		</div>
	</section> 
	removed to correct user flow
-->
<?php if( have_rows('client_examples') ): ?>
	<section class="clients">
		<div class="client intro">
			<h2>Everyone wants custom socks</h2>
			<a href="<?php the_permalink(23712); ?>" class="btn purple">Design Your Pair</a>
		</div>
		<?php while ( have_rows('client_examples') ) : the_row(); ?>
		<?php
					$logo = get_sub_field('logo');
					$sock = get_sub_field('sock');
				?>
		<div class="client">
			<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
			<?php if ( get_sub_field('sock') ) : ?>
			<img src="<?php echo $sock['url']; ?>" alt="<?php echo $sock['alt']; ?>" />
			<?php endif; ?>
		</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>
<?php if ( get_field('cta_banner') ) : ?>
	<section class="cta-banner">
		<div>
			<?php the_field('cta_banner'); ?>
		</div>
	</section>
<?php endif; ?>
<?php if ( have_rows('column_content') ) : ?>
	<section class="column-content">
		<?php while ( have_rows('column_content') ) : the_row(); ?>
		<div class="column">
			<h2><?php the_sub_field('heading'); ?></h2>
			<p><?php the_sub_field('description'); ?></p>
			<?php 
						$link = get_sub_field('button');
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self';
					?>
			<?php if ( get_sub_field('button') ) : ?>
			<a class="button" href="<?php echo esc_url($link_url); ?>"
				target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
			<?php endif; ?>
		</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>
<section class="sock-highlight">
	<?php 
		$args = array(
			'post_type'      => 'sock-of-the-month',
			'posts_per_page' => 1,
		);
		$socks = new WP_Query( $args );
	?>
	<?php if ( $socks->have_posts() ) : ?>
	<?php while ( $socks->have_posts() ) : $socks->the_post(); ?>
	<?php 
			$date = get_field('month');
			$date = new DateTime($date);
		?>
	<div class="info">
		<!-- <span class="month"><?php echo $date->format('M'); ?></span> -->
		<span class="label">Customer Case Study</span>
	</div>
	<div class="sock">
		<div class="text-content">
			<h2>Sock of the Month</h2>
			<?php the_field('summary'); ?>
			<a href="<?php the_permalink(); ?>" class="btn teal">Read The Rest</a>
		</div>
		<?php $image = get_field('image'); ?>
		<div class="image">
			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
	</div>
	<?php endwhile; ?>
	<?php endif; wp_reset_query(); ?>
</section>

<?php get_footer(); ?>