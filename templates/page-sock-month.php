<?php
/**
* Template Name: Sock of the Month
**/
?>

<?php get_header(); ?>

<!-- SUBHEADING -->
<?php if( get_field('page_subheading') ): ?>
	<?php get_template_part('template-parts/subheading'); ?>
<?php endif; ?>

	<section id="fullwidth">

		<!-- STEP ONE -->
		<div class="sock-month-step fullwidth">
			<div class="wrap flex-container">
				<div class="image one-half">
					<?php $image = get_field('step_one_image'); ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				<div class="contents one-half">
					<div class="step-number"><span>step</span>1</div>
					<?php the_field('step_one_content'); ?>
				</div>
			</div>
		</div>
		<!-- STEP TWO -->
		<div class="sock-month-step fullwidth">
			<div class="wrap flex-container">
				<div class="image one-half">
					<div class="fullwidth">
						<?php $image = get_field('step_two_image'); ?>
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>
				<div class="contents one-half">
					<div class="step-number"><span>step</span>2</div>
					<?php the_field('step_two_content'); ?>
				</div>
			</div>
		</div>
		<!-- STEP THREE -->
		<div class="sock-month-step fullwidth">
			<div class="wrap flex-container">
				<div class="image one-half">
					<?php $image = get_field('step_three_image'); ?>
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				<div class="contents one-half">
					<div class="step-number"><span>step</span>3</div>
					<?php the_field('step_three_content'); ?>
				</div>
			</div>
		</div>
		<!-- FULL WIDTH CTA -->
		<?php $image = get_field('final_product_image'); ?>
		<div class="sock-month-final fullwidth" style="background-image: url('<?php echo $image['url']; ?>');">
			<div class="wrap flex-container">
				<h2>The Finished Product</h2>
				<a href="<?php echo get_site_url(); ?>/custom-made-socks/" class="btn darkblue">Start your order now</a>
			</div>
		</div>

		<!-- QUERY OTHER SOCK OF MONTHS -->
		<?php get_template_part('template-parts/sock-of-month-preview'); ?>

	</section>

<?php get_footer(); ?>