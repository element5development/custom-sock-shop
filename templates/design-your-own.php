<?php 
/*----------------------------------------------------------------*\

	Template Name: Design Your Own

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php
	$style = $_GET["style"];
	$type = $_GET["type"];
?>

<header>
	<section class="step-numbers">
		<div class="step-one <?php if ( empty($style) && empty($type) ) : ?>is-active<?php else : ?>is-complete<?php endif; ?>">
			<a href="<?php the_permalink(23712); ?>" class="count">
				<span>1</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path fill="#fff" d="M18 40L4 26l6-6 8 8L38 8l6 6z"/></svg>
			</a>
			<div class="label">Usage</div>
		</div>
		<div class="step-two <?php if ( !empty($style) || !empty($type) ) : ?>is-active<?php endif; ?>">
			<div class="count">
				<span>2</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path fill="#fff" d="M18 40L4 26l6-6 8 8L38 8l6 6z"/></svg>
			</div>
			<div class="label">Style</div>
		</div>
		<div class="step-three">
			<div class="count">
				<span>3</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path fill="#fff" d="M18 40L4 26l6-6 8 8L38 8l6 6z"/></svg>
			</div>
			<div class="label">Design</div>
		</div>
	</section>
	<section class="headings useage-headings <?php if ( empty($style) && empty($type) ) : ?>is-active<?php endif; ?>">
		<?php if ( get_field('usage_title') ) : ?>
			<h1><?php the_field('usage_title'); ?></h1>
		<?php endif; ?>
		<?php if ( get_field('usage_summary') ) : ?>
			<p><?php the_field('usage_summary'); ?></p>
			<button class="comparison" data-featherlight="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/CompChart.png">Sock Comparison Chart</button>
		<?php endif; ?>
	</section>
	<section class="headings athletic-headings <?php if ( $style == 'athletic' || $type == 'athletic' ) : ?>is-active<?php endif; ?>">
		<?php if ( get_field('athletic_title') ) : ?>
			<h1><?php the_field('athletic_title'); ?></h1>
		<?php endif; ?>
		<?php if ( get_field('athletic_summary') ) : ?>
			<p><?php the_field('athletic_summary'); ?></p>
		<?php endif; ?>
	</section>
	<section class="headings standard-headings <?php if ( $style == 'standard' || $type == 'standard' || $type == 'marketing' ) : ?>is-active<?php endif; ?>">
		<?php if ( get_field('standard_title') ) : ?>
			<h1><?php the_field('standard_title'); ?></h1>
		<?php endif; ?>
		<?php if ( get_field('standard_summary') ) : ?>
			<p><?php the_field('standard_summary'); ?></p>
		<?php endif; ?>
	</section>
	<section class="headings dress-headings <?php if ( $style == 'dress' || $type == 'dress' ) : ?>is-active<?php endif; ?>">
		<?php if ( get_field('dress_title') ) : ?>
			<h1><?php the_field('dress_title'); ?></h1>
		<?php endif; ?>
		<?php if ( get_field('dress_description') ) : ?>
			<p><?php the_field('dress_description'); ?></p>
		<?php endif; ?>
	</section>
	<section class="headings print-headings <?php if ( $style == 'print' || $type == 'print' ) : ?>is-active<?php endif; ?>">
		<?php if ( get_field('print_title') ) : ?>
			<h1><?php the_field('print_title'); ?></h1>
		<?php endif; ?>
		<?php if ( get_field('print_description') ) : ?>
			<p><?php the_field('print_description'); ?></p>
		<?php endif; ?>
	</section>
</header>

<main>
	<article>
		<section class="usage options <?php if ( empty($style) && empty($type) ) : ?>is-active<?php endif; ?>">
			<?php $athleticBackground = get_field('athletic_background'); ?>
			<div class="option athletic">
				<div class="label" style="background-image: url(<?php echo $athleticBackground['url']; ?>);">
					<h2>Athletic</h2>
					<a href="<?php echo get_site_url(); ?>/custom-made-socks/?style=athletic" class="btn activate-athletic-style">I need these</a>
				</div>
				<?php if ( get_field('athletic_description') ) : ?>
					<div class="description">
						<p><?php the_field('athletic_description') ?></p>
					</div>
				<?php endif; ?>
			</div>
			<?php $standardBackground = get_field('standard_background'); ?>
			<div class="option standard">
				<div class="label" style="background-image: url(<?php echo $standardBackground['url']; ?>);">
					<h2>Standard</h2>
					<a href="<?php echo get_site_url(); ?>/custom-made-socks/?style=standard" class="btn activate-standard-style">I need these</a>
				</div>
				<?php if ( get_field('standard_description') ) : ?>
					<div class="description">
						<p><?php the_field('standard_description') ?></p>
					</div>
				<?php endif; ?>
			</div>
			<?php $dressBackground = get_field('dress_background'); ?>
			<div class="option dress">
				<div class="label" style="background-image: url(<?php echo $dressBackground['url']; ?>);">
					<h2>Dress</h2>
					<a href="<?php echo get_site_url(); ?>/custom-made-socks/?style=dress" class="btn activate-dress-style">I need these</a>
				</div>
				<?php if ( get_field('dress_description') ) : ?>
					<div class="description">
						<p><?php the_field('dress_description') ?></p>
					</div>
				<?php endif; ?>
			</div>
			<?php $printBackground = get_field('print_background'); ?>
			<!-- <div class="option print">
				<div class="label" style="background-image: url(<?php echo $printBackground['url']; ?>);">
					<h2>Print</h2>
					<a href="<?php echo get_site_url(); ?>/custom-made-socks/?style=print" class="btn activate-print-style">I need these</a>
				</div>
				<?php if ( get_field('print_description') ) : ?>
					<div class="description">
						<p><?php the_field('print_description') ?></p>
					</div>
				<?php endif; ?>
			</div> -->
		</section>
		<section class="style-athletic options <?php if ( $style == 'athletic' || $type == 'athletic' ) : ?>is-active<?php endif; ?>">
			<?php while ( have_rows('athletic_options') ) : the_row(); ?>
				<a href="<?php the_sub_field('url'); ?>?style=athletic" class="option">
					<div class="label">
						<?php $image = get_sub_field('image'); ?>
						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
						<h2><?php the_sub_field('label'); ?></h2>
					</div>
					<?php if ( get_sub_field('description') ) : ?>
						<div class="description">
							<p><?php the_sub_field('description'); ?></p>
						</div>
					<?php endif; ?>
				</a>
			<?php endwhile; ?>
		</section>
		<section class="style-standard options <?php if ( $style == 'standard' || $type == 'standard' || $type == 'marketing' ) : ?>is-active<?php endif; ?>">
			<?php while ( have_rows('standard_options') ) : the_row(); ?>
				<a href="<?php the_sub_field('url'); ?>?style=standard" class="option">
					<div class="label">
						<?php $image = get_sub_field('image'); ?>
						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
						<h2><?php the_sub_field('label'); ?></h2>
					</div>
					<?php if ( get_sub_field('description') ) : ?>
						<div class="description">
							<p><?php the_sub_field('description'); ?></p>
						</div>
					<?php endif; ?>
				</a>
			<?php endwhile; ?>
		</section>
		<section class="style-dress options <?php if ( $style == 'dress' || $type == 'dress' ) : ?>is-active<?php endif; ?>">
			<?php while ( have_rows('dress_options') ) : the_row(); ?>
				<a href="<?php the_sub_field('url'); ?>?style=dress" class="option">
					<div class="label">
						<?php $image = get_sub_field('image'); ?>
						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
						<h2><?php the_sub_field('label'); ?></h2>
					</div>
					<?php if ( get_sub_field('description') ) : ?>
						<div class="description">
							<p><?php the_sub_field('description'); ?></p>
						</div>
					<?php endif; ?>
				</a>
			<?php endwhile; ?>
		</section>
		<section class="style-print options <?php if ( $style == 'print' || $type == 'print' ) : ?>is-active<?php endif; ?>">
			<?php while ( have_rows('print_options') ) : the_row(); ?>
				<a href="<?php the_sub_field('url'); ?>?style=print" class="option">
					<div class="label">
						<?php $image = get_sub_field('image'); ?>
						<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
						<h2><?php the_sub_field('label'); ?></h2>
					</div>
					<?php if ( get_sub_field('description') ) : ?>
						<div class="description">
							<p><?php the_sub_field('description'); ?></p>
						</div>
					<?php endif; ?>
				</a>
			<?php endwhile; ?>
		</section>
	</article>
	<div id="feefo-service-review-carousel-widgetId" class="feefo-review-carousel-widget-service"></div>
</main>

<?php get_footer(); ?>