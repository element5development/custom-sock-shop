<?php
/**
 * Template Name: Fundraisers
 */
?>

<?php get_header(); ?>

<?php 
	if ( get_field('header_background_image') ) {
		$background = get_field('header_background_image');
		$backgroundURL = $background['url'];
	} else {
		$backgroundURL = get_stylesheet_directory_uri() . '/dist/images/header-default.jpg';
	}
?>
<section id="headerimg" class="<?php if ( get_field('notification_bar_text', 'options') ) : ?>notification-on<?php endif; ?>" data-speed="2" style="background-image: url(<?php echo $backgroundURL; ?>);">
	<div class="headingcontainer">
		<?php if ( get_field('header_title') ) : ?>
			<h1><?php the_field('header_title'); ?></h1>
		<?php else : ?>
			<h1><?php the_title(); ?></h1>
		<?php endif; ?>

		<?php if ( get_field('header_subtitle') ) : ?>
			<p><?php the_field('header_subtitle'); ?></p>
		<?php endif; ?>
	</div>
</section>

<div class="wrap">
	<section id="fullwidth">
	<?php
		$left = get_field('left_column');
	?>	
	<?php if( $left ): ?>
		<div class="product">
			<div class="product-gallery">
				<?php 
					$images = $left['gallery'];
				?>
				<?php if( $images ): ?>
					<ul>
						<?php $i=0; foreach( $images as $image ): $i++; ?>
							<?php if ( $i = 1 ) : ?>
								<li>
									<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
								</li>
							<?php else : ?>
								<li>
									<img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
					</ul>
				<?php endif; ?>
			</div>
			<hr>
			<h3>Order Pickup & Shipping</h3>
			<p><?php echo $left['order_pickup']; ?></p>
			<h3>Product Details</h3>
			<p><?php echo $left['product_details']; ?></p>
		</div>
	<?php endif; ?>
	<?php
		$right = get_field('right_column');
	?>	
	<?php if( $right ): ?>
		<div class="summary">
			<h2><?php echo $right['product_title']; ?></h2>
			<?php
				$date = $right['end_time'];
				$date2 = date("F j, Y g:i a", strtotime($date));
			?>
			<p>Available until <?php echo $date2; ?></p>
			<div class="counter">
				<div class="weeks">
					<p>
						<span>##</span>
						WEEKS
					</p>
				</div>
				<div class="days">
					<p>
						<span>##</span>
						DAYS
					</p>
				</div>
				<div class="hours">
					<p>
						<span>##</span>
						HOURS
					</p>
				</div>
				<div class="minutes">
					<p>
						<span>##</span>
						MINS
					</p>
				</div>
			</div>
			<h4>Support by Purchasing</h4>
			<h3><?php echo $right['price']; ?> per pair</h3>
			<hr>
			<?php $formid = $right['gravity_form_id']; ?>
			<?php echo do_shortcode( '[gravityform id="'. $formid .'" title="false" description="false"]' ); ?>
		</div>
	<?php endif; ?>
	</section>
</div>

<?php get_footer(); ?>

<?php
	$date = $right['end_time'];
?>
<script type="text/javascript"> // Get the Countdown Values
	jQuery('.counter').countdown('<?php echo $date; ?>', function(event) {
		jQuery('.weeks p span').html(event.strftime('%w'));
		jQuery('.days p span').html(event.strftime('%d'));
		jQuery('.hours p span').html(event.strftime('%H'));
		jQuery('.minutes p span').html(event.strftime('%M'));
	});

	jQuery('.minutes p span').each(function() {
		var el = $(this);
		if (el.text() === '00') {
			jQuery('body.page-template-page-fundraiser .gform_wrapper').addClass('disabled');
		}
	});
</script>