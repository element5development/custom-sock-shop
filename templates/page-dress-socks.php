<?php
/**
 * Template Name: Dress Socks
 */
?>

<?php get_header(); ?>

<header>
	<?php
		$background = get_field('header_image');
	?>
	<div style="background-image: url(<?php echo $background['url']; ?>);"></div>
	<div>
		<?php the_field('tagline'); ?>
	</div>
</header>

<?php if( have_rows('styles') ): ?>
	<section class="start-blank">
		<div>
			<h2>Select the style that best fits you</h2>
			<div class="templates">
				<?php while ( have_rows('styles') ) : the_row(); ?>
					<?php
						$link = get_sub_field('link');
						$image = get_sub_field('example_image');
					?>
					<a href="<?php echo $link['url']; ?> " class="template">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						<?php echo $link['title']; ?>
					</a>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<?php if( have_rows('bulk_pricing') ): ?>
<section class="bulk-pricing">
	<h2>Fully Custom Dress Sock Pricing</h2>
	<p>How many pairs are you looking for?</p>
	<?php if ( get_field('special_offer') ) : ?>
		<section class="special">
			<h3><?php the_field('special_offer'); ?></h3>
		</section>
	<?php endif; ?>
	<div>
		<ul>
			<?php while ( have_rows('bulk_pricing') ) : the_row(); ?>
				<li>
					<button>
						<?php the_sub_field('pair_count_range'); ?>
						<span> <?php the_sub_field('price'); ?></span>
					</button>
				</li>
			<?php endwhile; ?>
		</ul>
	</div>
	<div>
		<h3>How many pairs are you looking for?</h3>
		<div>
			<?php while ( have_rows('bulk_pricing') ) : the_row();; ?>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/bulk-socks.png" />
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>

<?php if ( get_field('cta_banner') ) : ?>
	<section class="cta-banner">
		<div>
			<?php the_field('cta_banner'); ?>
		</div>
	</section>
<?php endif; ?>

<?php if ( have_rows('column_content') ) : ?>
	<section class="column-content">
		<?php while ( have_rows('column_content') ) : the_row(); ?>
			<div class="column">
				<h2><?php the_sub_field('heading'); ?></h2>
				<p><?php the_sub_field('description'); ?></p>
				<?php 
					$link = get_sub_field('button');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<?php if ( get_sub_field('button') ) : ?>
					<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	</section>

	<?php if ( get_field('cta_banner') ) : ?>
		<section class="cta-banner yellow">
			<div>
				<?php the_field('cta_banner'); ?>
			</div>
		</section>
	<?php endif; ?>
<?php endif; ?>

<?php if( have_rows('client_examples') ): ?>
	<section class="clients">
		<div class="client intro">
			<h2>Everyone wants custom socks</h2>
			<a href="<?php the_permalink(7900); ?>" class="btn purple">Design Your Pair</a>
		</div>
		<?php while ( have_rows('client_examples') ) : the_row(); ?>
			<?php
				$logo = get_sub_field('logo');
				$sock = get_sub_field('sock');
			?>
			<div class="client">
				<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['alt']; ?>" />
				<?php if ( get_sub_field('sock') ) : ?>
					<img src="<?php echo $sock['url']; ?>" alt="<?php echo $sock['alt']; ?>" />
				<?php endif; ?>
			</div>
		<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php if ( get_field('proof_content') && get_field('proof_image') ) : ?>
	<section class="sock-proof">
		<?php
			$background = get_field('proof_image');
		?>
		<div>
			<?php the_field('proof_content'); ?>
		</div>
		<div style="background-image: url(<?php echo $background['url']; ?>);"></div>
	</section>
<?php endif; ?>

<?php if ( get_field('intro_video') && get_field('intro_text') ) : ?>
	<section class="intro">
		<div class="video">
			<?php the_field('intro_video'); ?>
		</div>
		<div class="text-content">
			<?php the_field('intro_text'); ?>
		</div>
		<?php $clients = get_field('client_logos'); ?>
		<?php if( $clients ): ?>
			<div class="clients">
				<h3>Clients we've worked with</h3>
				<ul>
					<?php foreach( $clients as $client ): ?>
						<li>
							<img src="<?php echo $client['sizes']['medium']; ?>" alt="<?php echo $client['alt']; ?>" />
						</li>
					<?php endforeach; ?>
				</ul>
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>

<?php get_footer(); ?>