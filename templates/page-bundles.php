<?php
/**
 * Template Name: Bundles
 */
 get_header(); ?>

<!-- SUBHEADING -->
<?php if( get_field('page_subheading') ): ?>
	<?php get_template_part('template-parts/subheading'); ?>
<?php endif; ?>

<div id="bundles-page">
 	<div class="wrap">
		 <!-- DEFAULT CONTENT -->
		<section id="left">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<article class="post" id="post-<?php the_ID(); ?>">
					<div class="entry">
						<?php the_content(); ?>
						<a href="<?php bloginfo('template_url'); ?>/dist/images/adult-bundle.pdf" target="_blank" class="btn darkblue fancybox">See the Gildan Adult Details</a>
						<a href="<?php bloginfo('template_url'); ?>/dist/images/youth-bundle.pdf" target="_blank" class="btn darkblue fancybox">See the Gildan Youth Details</a>
						<div id="testing-cta">
							<h2>Ready to learn more?<br/>Simply fill out this quick form.</h2>
						</div>
					</div>
				</article>
			<?php endwhile; endif; ?>
		</section>
		<!-- SIDEBAR -->
		<aside id="sidebar"> 
			<?php if ( is_page(991) ) { ?>
				<?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
			<?php } elseif ( is_page(7712) ) { ?>
				<?php echo do_shortcode('[gravityform id="4" title="false" description="false"]'); ?>
			<?php } elseif ( is_page(6461) ) { ?>
				<?php echo do_shortcode('[gravityform id="3" title="false" description="false"]'); ?>
			<?php } elseif ( is_page(9036) ) { ?>
				<?php echo do_shortcode('[gravityform id="5" title="false" description="false"]'); ?>
			<? } else { ?>
			<?php } ?>
		</aside>
	</div>
</div>

<?php get_footer(); ?>