<?php
/**
 * Template Name: Home
 */
?>
<?php get_header(); ?>

<?php if ( get_field('show_holiday_header') ) : ?>
<header class="holiday">
  <div class="hero-wrap">
    <div class="contents">
      <h1><?php the_field('holiday_title'); ?></h1>
			<?php if ( get_field('holiday_button') ) : ?>
				<?php
					$link = get_field('holiday_button');
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="btn purple" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
			<?php endif; ?>
      <p class="special"><?php the_field('holiday_sub_title'); ?></p>
      <p class="disclaimer"><?php the_field('holiday_disclaimer'); ?></p>
    </div>
    <div class="hero-image">
			<?php 
			$image = get_field('holiday_hero_image');
			if( !empty( $image ) ): ?>
					<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
			<?php endif; ?>
    </div>
  </div>
</header>
<?php else : ?>
<header>
	<?php if ( get_field('header_top_gallery') ) : ?>
		<?php $images = get_field('header_top_gallery'); ?>
		<ul class="top-gallery" dir="rtl">
			<?php foreach( $images as $image ): ?>
				<li>
					<img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
	<div class="contents">
		<h1><?php the_field('header_title'); ?></h1>
		<?php if( get_field('pricing') && get_field('shipping') ) : ?>
			<div class="stats">
				<div>
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/price-tag.svg" />
					<p><?php the_field('pricing'); ?></p>
				</div>
				<div>
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/shipping.svg" />
					<p><?php the_field('shipping'); ?></p>
				</div>
			</div>
		<?php endif; ?>
		<?php if ( get_field('header_button') ) : ?>
			<?php
				$link = get_field('header_button');
				$link_url = $link['url'];
    		$link_title = $link['title'];
    		$link_target = $link['target'] ? $link['target'] : '_self';
			?>
			<a class="btn purple" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>
	</div>
	<?php if ( get_field('header_bottom_gallery') ) : ?>
		<?php $images = get_field('header_bottom_gallery'); ?>
		<ul class="bottom-gallery">
			<?php foreach( $images as $image ): ?>
				<li>
					<img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
				</li>
			<?php endforeach; ?>
		</ul>
	<?php endif; ?>
</header>
<?php endif; ?>

<main>
	<article>
		<?php if ( get_field('intro_video') && get_field('intro_description') ) : ?>
			<section class="intro">
				<div>
					<?php the_field('intro_video'); ?>
				</div>
				<div>
					<?php the_field('intro_description'); ?>
				</div>
			</section>
		<?php endif; ?>
		<?php if ( get_field('intro_clients') || get_field('intro_reviews') ) : ?>
			<section class="social-proof">
				<?php if ( get_field('intro_clients') ) : ?>
					<div class="key-clients">
						<h3>Clients we have worked with</h3>
						<?php $images = get_field('intro_clients'); ?>
						<ul>
							<?php foreach( $images as $image ): ?>
								<li>
									<img src="<?php echo esc_url($image['sizes']['medium']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
				<?php endif; ?>
				<?php if ( get_field('intro_reviews') ) : ?>
					<div>
						<?php the_field('intro_reviews'); ?>
					</div>
				<?php endif; ?>
			</section>
		<?php endif; ?>
		<?php if ( have_rows('seasonal_hightlight') ) : ?>
			<section class="seasonal-cta">
				<h2>Custom Socks for Any Occasion</h2>
				<?php while( have_rows('seasonal_hightlight') ) : the_row(); ?>
					<div>
						<?php $image = get_sub_field('image'); ?>
						<img src="<?php echo esc_url($image['sizes']['large']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
						<div>
							<h3><?php the_sub_field('title'); ?></h3>
							<p><?php the_sub_field('description'); ?></p>
							<?php
								$link = get_sub_field('button');
								$link_url = $link['url'];
								$link_title = $link['title'];
								$link_target = $link['target'] ? $link['target'] : '_self';
							?>
							<a class="btn purple" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
						</div>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
		<?php if ( have_rows('card_grid') ) : ?>
			<section class="secondary-actions">
				<?php while( have_rows('card_grid') ) : the_row(); ?>
					<div>
						<h2><?php the_sub_field('title'); ?></h2>
						<p><?php the_sub_field('description'); ?></p>
						<?php
							$link = get_sub_field('button');
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
						<a class="btn teal" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
					</div>
				<?php endwhile; ?>
			</section>
		<?php endif; ?>
		<?php if ( have_rows('questions') ) : ?>
			<section class="common-questions">
				<div>
					<h2>Common Questions</h2>
					<?php while( have_rows('questions') ) : the_row(); ?>
						<div>
							<h3><?php the_sub_field('question'); ?></h3>
							<p><?php the_sub_field('answer'); ?></p>
						</div>
					<?php endwhile; ?>
				</div>
			</section>
		<?php endif; ?>
	</article>
</main>

<?php get_footer(); ?>