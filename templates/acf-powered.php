<?php 
/*----------------------------------------------------------------*\

	Template Name: ACF Builder

\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php $background = get_field('header_background'); ?>

<header class="page-header" style="background-image: url(<?php echo $background['url']; ?>);">
	<div>
		<h1><?php the_title(); ?></h1>
		<?php if ('subheading') : ?>
			<p><?php the_field('subheading'); ?></p>
		<?php endif; ?>
		<?php $link = get_field('header_button'); ?>
		<?php if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a class="btn teal" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		<?php endif; ?>
	</div>
</header>

<main class="main-content">
	<?php if( have_rows('article') ):  ?>
		<article>
			<?php 
			/*----------------------------------------------------------------*\
			|
			| Insert page content which is most often handled via ACF Pro
			| and highly recommend the use of the flexiable content so
			|	we already placed that code here.
			|
			| https://www.advancedcustomfields.com/resources/flexible-content/
			|
			\*----------------------------------------------------------------*/
			?>
			<?php
				while ( have_rows('article') ) : the_row();
					$id++;
					if( get_row_layout() == 'editor' ):
						hm_get_template_part('template-parts/acf-sections/editor', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == '2editor' ):
						hm_get_template_part('template-parts/acf-sections/editor-2-column', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == '3editor' ):
						hm_get_template_part('template-parts/acf-sections/editor-3-column', [ 'sectionId' => $id ] );
					elseif( get_row_layout() == 'media+text' ):
						hm_get_template_part('template-parts/acf-sections/media-text', [ 'sectionId' => $id ] );
					endif;
				endwhile;
			?>
		</article>
	<?php else : ?>
		<article>
			<section class="no-content">
				<h2>Uh Oh. Something is missing.</h2>
				<p>Looks like this page has no content.</p>
			</section>
		</article>
	<?php endif; ?>
</main>

<?php get_footer(); ?>