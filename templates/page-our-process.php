<?php
/**
 * Template Name: Our Process
 */
 get_header(); ?>

<!-- SUBHEADING -->
<?php if( get_field('page_subheading') ): ?>
	<?php get_template_part('template-parts/subheading'); ?>
<?php endif; ?>

<!-- STEPS / BASED ON CHILD PAGES -->
<?php if( have_rows('steps') ): $i = 0 ?>
	<section id="steps">
			<?php while ( have_rows('steps') ) : the_row(); $i++; ?>
				<article class="<?php echo $post->post_name;?>">
					<div class="wrap">
						<div class="stepcontainer">
							<div class="stepbox">
								<div class="step">
									<span>Step</span> <?php echo $i; ?>
								</div>
							</div>
							<p><?php the_sub_field('text'); ?></p>
						</div>
						<?php $image = get_sub_field('image'); ?>
						<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
					</div>
				</article>
			<?php endwhile; ?>
	</section>
<?php endif; ?>

<?php get_footer(); ?>
