<?php 
/* Template Name: OnBoarding*/
 ?>

<?php get_header(); ?>

<?php
	$step = $_GET["step"];
	/*USED IN STEP THREE*/
	$type = $_GET["type"];
	$style = $type.'-'.$_GET["style"];
	if ( $style ) :
		$args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => $type.'+'.$style);
	else :
		$args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => $type);
	endif;
?>

<!--CUSTOM SUBHEADING / BASED ON STEP-->
<section id="secondary" class="onboarding">
	<div class="pageicon">
		<?php if( get_field('page_icon') ): ?>
		<img src="<?php the_field('page_icon'); ?>" alt="<?php the_title(); ?>" />
		<?php else: ?>
		<img src="<?php bloginfo('template_url'); ?>/dist/images/icon-default.png" alt="<?php the_title(); ?>" />
		<?php endif; ?>
	</div>
	<div class="onboarding-step-identification">
		<div class="centerer">
			<?php if ( !isset($step) ) { ?>
			<p>STEP 1</p>
			<div class="dot-container">
				<div class="step-dot current-step"></div>
				<div class="step-dot"></div>
				<div class="step-dot"></div>
				<div style="clear: both"></div>
			</div>
			<?php } ?>
			<?php if ( $step == "2" )  { ?>
			<p>STEP 2</p>
			<div class="dot-container">
				<a href="/custom-made-socks/">
					<div class="step-dot"></div>
				</a>
				<div class="step-dot current-step"></div>
				<div class="step-dot"></div>
				<div style="clear: both"></div>
			</div>
			<?php } ?>
			<?php if ( $step == "3" ) { ?>
			<p>STEP 3</p>
			<div class="dot-container">
				<a href="/custom-made-socks/">
					<div class="step-dot"></div>
				</a>
				<a href="/custom-made-socks/?step=2&type=<?php echo $type; ?>">
					<div class="step-dot"></div>
				</a>
				<div class="step-dot current-step"></div>
				<div style="clear: both"></div>
			</div>
			<?php } ?>
		</div>
	</div>
	<div class="wrap onboarding">
		<?php if ( !isset($step) ) { ?>
		<h2 id="onboarding-one-subheading" class="onboarding-subheading current-step"><?php the_field('step_heading'); ?>
		</h2>
		<?php } ?>
		<?php if ( $step == "2" )  { ?>
		<h2 id="onboarding-two-subheading" class="onboarding-subheading"><?php the_field('step_2_heading'); ?></h2>
		<?php } ?>
		<?php if ( $step == "3" ) { ?>
		<h2 id="onboarding-three-subheading" class="onboarding-subheading"><?php the_field('step_3_heading'); ?></h2>
		<?php } ?>
	</div>
</section>

<!-- OPTIONS / BASED ON STEP -->
<div class="wrap onboarding-bg">
	<section id="fullwidth">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="post" id="post-<?php the_ID(); ?>">
			<div class="entry">
				<?php if ( !isset($step) ) { ?>
					<div id="onboarding-step-one" class="onboarding-step">
						<div class="one-third">
							<img src="<?php the_field('option_one_image'); ?>" />
							<h3><?php the_field('option_one_title'); ?></h3>
							<a href="/custom-made-socks/?step=2&type=athletic" class="btn darkblue">I need this one</a>
						</div>
						<div class="one-third">
							<img src="<?php the_field('option_two_image'); ?>" />
							<h3><?php the_field('option_two_title'); ?></h3>
							<a href="/custom-made-socks/?step=2&type=marketing" class="btn purple">I need this one</a>
						</div>
						<div class="one-third">
							<img src="<?php the_field('option_three_image'); ?>" />
							<h3><?php the_field('option_three_title'); ?></h3>
							<a href="/custom-made-socks/?step=3&type=dress" class="btn teal">I need this one</a>
						</div>
						<div style="clear: both"></div>
					</div>
				<?php } ?>
				<?php if ( $step == "2" )  { ?>
					<div id="onboarding-step-two" class="onboarding-step">
						<?php if ($type == 'athletic') { ?>
							<div class="one-sixth">
								<a href="/product/custom-athletic-footie-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_one_image'); ?>');"></div>
									<h3><?php the_field('2option_one_title'); ?></h3>
								</a>
							</div>
							<div class="one-sixth">
								<a href="/product/custom-athletic-anklet-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_two_image'); ?>');"></div>
									<h3><?php the_field('2option_two_title'); ?></h3>
								</a>
							</div>
							<div class="one-sixth">
								<a href="/product/custom-athletic-crew-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_three_image'); ?>');">
									</div>
									<h3><?php the_field('2option_three_title'); ?></h3>
								</a>
							</div>
							<div class="one-sixth">
								<a href="/product/custom-elite-crew-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_four_image'); ?>');"></div>
									<h3><?php the_field('2option_four_title'); ?></h3>
								</a>
							</div>
							<div class="one-sixth">
								<a href="/product/custom-athletic-knee-high-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_five_image'); ?>');"></div>
									<h3><?php the_field('2option_five_title'); ?></h3>
								</a>
							</div>
							<div class="one-sixth">
								<a href="/product/custom-elite-knee-high-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_six_image'); ?>');"></div>
									<h3><?php the_field('2option_six_title'); ?></h3>
								</a>
							</div>
						<?php } else { ?>
							<div class="one-sixth">
								<a href="/product/custom-marketing-footie-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_one_image'); ?>');"></div>
									<h3><?php the_field('2option_one_title'); ?></h3>
								</a>
							</div>
							<div class="one-sixth">
								<a href="/product/custom-marketing-anklet-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_two_image'); ?>');"></div>
									<h3><?php the_field('2option_two_title'); ?></h3>
								</a>
							</div>
							<div class="one-sixth">
								<a href="/product/custom-marketing-crew-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_three_image'); ?>');">
									</div>
									<h3><?php the_field('2option_three_title'); ?></h3>
								</a>
							</div>
							<div class="one-sixth">
								<a href="/product/custom-marketing-knee-high-socks/">
									<div class="option-img" style="background-image: url('<?php the_field('2option_five_image'); ?>');"></div>
									<h3><?php the_field('2option_five_title'); ?></h3>
								</a>
							</div>
						<?php } ?>
						<div style="clear: both"></div>
					</div>
				<?php } ?>
				<?php if ( $step == "3" ) { ?>
					<div id="onboarding-step-three" class="onboarding-step">
						<ul class="filter-products">
							<?php $loop = new WP_Query( $args ); ?>
							<?php while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
							<li class="one-fourth">
								<a href="<?php the_permalink(); ?>">
									<?php if ( has_post_thumbnail( $loop->post->ID ) ) : ?>
									<?php echo get_the_post_thumbnail( $loop->post->ID, 'shop_catalog' ); ?>
									<?php endif; ?>
									<h3><?php the_title(); ?></h3>
									<?php 
													global $post;	
													$terms = wp_get_post_terms( $post->ID, 'product_cat' );
													foreach ( $terms as $term ) $categories[] = $term->slug;
												?>
									<?php if ( in_array( 'dress', $categories ) ):  ?>
									<!-- DO NOT DISPLAY PRICE -->
									<?php else : ?>
									<?php echo $product->get_price_html(); ?>
									<?php endif; ?>
								</a>
							</li>
							<?php endwhile; ?>
							<?php wp_reset_query(); ?>
							<div style="clear: both"></div>
						</ul>
					</div>
				<?php } ?>
			</div>
		</article>
		<?php endwhile; endif; ?>
	</section>
</div>

<?php get_footer(); ?>