<?php
/**
 * Template Name: Sock Application
 */
?>

<?php get_header(); ?>
<?php $background = get_field('heading_background'); ?>

<header style="background-image: url(<?php echo $background['url']; ?>);">
	<div>
		<h1><?php the_title(); ?></h1>
		<?php if ('subheading') : ?>
			<p><?php the_field('subheading'); ?></p>
		<?php endif; ?>
	</div>
</header>
<main>
	<?php $posts = get_field('sock_options'); ?>
	<?php if( $posts ): ?>
		<section class="example-products">
			<h2>What type of sock do you want to customize?</h2>
			<ul class="options">
				<?php while ( have_rows('onboarding_options') ) : the_row(); ?>
					<?php $background = get_sub_field('background'); ?>
					<li class="option <?php the_sub_field('title'); ?>" style="background-image: url(<?php echo $background['sizes']['medium'] ?>);">
						<h2><?php the_sub_field('title'); ?></h2>
						<?php if ( get_sub_field('button') ) : 
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
							<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"></a>
							<div class="btn"><?php echo esc_html( $link_title ); ?></div>
						<?php endif; ?>
					</li>
				<?php endwhile; ?>
				<li class="option">
					<h2>Want help with your sock design?</h2>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/CSS_Cursor.svg" alt="designer help" />
					<a href="<?php the_permalink(13); ?>"></a>
					<div class="btn">Contact Us</div>
				</li>
			</ul>
		</section>
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif; ?>
	<?php if( have_rows('testimonies') ): ?>
		<section class="testimonies">
			<h2>Real Stories from real people</h2>
			<ul>
				<?php while ( have_rows('testimonies') ) : the_row(); ?>
					<li>
						<div class="icon"></div>
						<p><?php the_sub_field('quote'); ?><b><?php the_sub_field('name'); ?></b></p>
					</li>
				<?php endwhile; ?>
			</ul>
		</section>
	<?php endif; ?>
	<?php if( have_rows('content_repeater') ): ?>
		<?php while ( have_rows('content_repeater') ) : the_row(); ?>
			<section class="content">
				<div>
					<?php $image = get_sub_field('image'); ?>
					<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				<div>
					<?php the_sub_field('contents'); ?>
				</div>
			</section>
		<?php endwhile; ?>
	<?php endif; ?>
	<div class="key-point-wrapper">
		<section class="fifteen-minimum">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/MinimumOrder.svg" alt="15 pair minimum for every custom sock" />
			<h2>The lowest minimum order for teams of all sizes.</h2>
			<p>Minimum order is <b>15 pairs per size and design</b>. Orders cannot be broken up into multiple sizes with the 15 pair minimum.</p>
		</section>
		<section class="artproof-required">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/ProofApproval.svg" alt="art proof approval is required" />
			<h2>Production will not begin without your written approval.</h2>
			<p>Artwork proof will be provided within <b>48 business hours</b> of order being placed. Start designing using the sock builder to see an instant virtual mock up!</p>
		</section>
	</div>
	<section class="image-qualities">
		<h2>The perfect logo makes a perfect sock</h2>
		<div class="examples">
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Crisp-Logo.svg" alt="upload crisp high resolution images" />
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Blurry-Logo.svg" alt="avoid blurry or pixilated images" />
		</div>
		<ul>
			<li>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/NotBlur.svg" alt="crisp high resolution, avoid blurry or pixilated" />
				<p>Crisp image, not blurry</p>
			</li>
			<li>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/NoFilter.svg" alt="avoid images with filter or special effects" />
				<p>No filters or special effects</p>
			</li>
			<li>
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Crop.svg" alt="avoid images that have been cropped" />
				<p>Entire logo, not cropped</p>
			</li>
			<li>
			<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/Large.svg" alt="larger the file the better" />
				<p>Larger files work the best</p>
			</li>
		</ul>
	</section>
	<?php $posts = get_field('sock_options'); ?>
	<?php if( $posts ): ?>
		<section class="example-products">
			<h2>What type of sock do you want to customize?</h2>
			<ul class="options">
				<?php while ( have_rows('onboarding_options') ) : the_row(); ?>
					<?php $background = get_sub_field('background'); ?>
					<li class="option <?php the_sub_field('title'); ?>" style="background-image: url(<?php echo $background['sizes']['medium'] ?>);">
						<h2><?php the_sub_field('title'); ?></h2>
						<?php if ( get_sub_field('button') ) : 
							$link = get_sub_field('button'); 
							$link_url = $link['url'];
							$link_title = $link['title'];
							$link_target = $link['target'] ? $link['target'] : '_self';
						?>
							<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"></a>
							<div class="btn"><?php echo esc_html( $link_title ); ?></div>
						<?php endif; ?>
					</li>
				<?php endwhile; ?>
				<li class="option">
					<h2>Want help with your sock design?</h2>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/CSS_Cursor.svg" alt="designer help" />
					<a href="<?php the_permalink(13); ?>"></a>
					<div class="btn">Contact Us</div>
				</li>
			</ul>
		</section>
		<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
	<?php endif; ?>
	<?php $clients = get_field('client_logos', '976'); //home page ?>
	<?php if( $clients ): ?>
		<section class="clients">
			<h3>Clients we've worked with</h3>
			<ul>
				<?php foreach( $clients as $client ): ?>
					<li>
						<img src="<?php echo $client['sizes']['medium']; ?>" alt="<?php echo $client['alt']; ?>" />
					</li>
				<?php endforeach; ?>
			</ul>
		</section>
	<?php endif; ?>
</main>

<?php get_footer(); ?>