<?php
/**
 * Template Name: Landing 2019
 */
 get_header(); ?>

<!--URL for featured image-->
<?php 
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), array( 5600,1000 ), false, '' );
	$featuredimg = $src[0];
?>

<!-- PAGE TITLE -->
<div class="landing-intro" style="background-image: url(<?php echo $featuredimg; ?>);">
	<div class="intro-content-center">
		<div class="intro-content">
			<img src="<?php bloginfo('template_directory'); ?>/dist/images/css-logo-landing.png" />
			<div class="landing-icon" style="background-image: url(<?php the_field('landing_page_icon') ?>)"></div>
			<h1><?php the_title(); ?></h1>
			<a href="<?php the_field('landing_cta_url') ?>" class="btn purple"><?php the_field('landing_cta_text') ?></a>
		</div>
	</div>
</div>

<!-- THEME INTRO -->
<div class="landing-page-content-container" style="background-image: url(<?php the_field('landing_content_bg') ?>)">
	<div class="landing-page-center">
		<div class="landing-page-content">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; endif; ?>
			<a href="<?php the_field('landing_cta_url') ?>" class="btn yellow"><?php the_field('landing_cta_text') ?></a>
		</div>
	</div>
</div>

<!-- FAQ CTA -->
<div class="landing-questions" style="background-image: url(<?php the_field('Landing_questions_background') ?>)">
	<div class="wrap">
		<h2><?php the_field('landing_questions_title') ?></h2>
		<p><?php the_field('landing_questions_subtitle') ?></p>
		<?php $link = get_field('landing_button_1'); ?>
		<?php if( $link ): ?>
			<a class="btn purple" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
		<?php endif; ?>
		<?php $link2 = get_field('landing_button_2'); ?>
		<?php if( $link2 ): ?>
			<a class="btn yellow" href="<?php echo $link2['url']; ?>" target="<?php echo $link2['target']; ?>"><?php echo $link2['title']; ?></a>
		<?php endif; ?>
		<?php $link3 = get_field('landing_button_3'); ?>
		<?php if( $link3 ): ?>
			<a class="btn teal" href="<?php echo $link3['url']; ?>" target="<?php echo $link3['target']; ?>"><?php echo $link3['title']; ?></a>
		<?php endif; ?>
	</div>
</div>

<?php wp_reset_postdata(); ?>

<!-- TESTIMONY SLIDER / CHANGE CATEGORY AS NEEDED -->
<section id="slider" class="testimonals-container">
	<div class="quote"></div>
	<h3>WHAT OUR CUSTOMERS HAVE TO SAY </h3>
	<div class="flexslider">
		<ul class="slides">
			<?php // Slide Query
				$args = array('post_type' => 'testimonial', 'category_name'  => 'baseball', 'order'=> 'ASC', 'showposts' => 5);
				$testimony_query = new WP_Query($args);
				while($testimony_query->have_posts()) : $testimony_query->the_post(); 
			?>
			<li>
				<div class="wrap">
					<div class="slidecontent">
							<div class="testimony">
						  	<?php the_content(); ?><span class="author"><?php the_field('reviewer'); ?></span>
						  </div>
					</div>
				</div>
			</li>
		<?php endwhile; wp_reset_postdata(); ?>	
		</ul>
	</div>
</section>


<!-- SIMPLE FOOTER -->
<footer id="footer" class="source-org vcard copyright landing-footer" role="contentinfo">
	<div class="wrap">
		<div id="footertop">
			<img src="<?php bloginfo('template_directory'); ?>/dist/images/css-logo-landing.png" />
			<p>© Copyright <?php echo date('Y'); ?> Custom Sock Shop. All Rights Reserved<a href="">TERMS AND CONDITIONS</a><a href="">PRIVACY POLICY</a></p>
		</div>
	</div>
</footer>


<?php wp_footer(); ?>

</body>
</html>