<?php 
/*-------------------------------------------------------------------
Footer
------------------------------------------------------------------*/
?>

<!-- Related Products -->
<!-- <?php if (  is_product() ) : ?>
	<section id="related">
		<div class="wrap">
			<h2>Related Products</h2>
			<ul class="products">
				<?php //Related
				$args = array(
					'post_type' => 'product', 
					'posts_per_page' => 4, 
					'orderby' => 'rand',
					'category' =>	75 //change to 103 for 'Bundles'
					);
				$related_query = new WP_Query($args);
				$price = get_post_meta( get_the_ID(), '_regular_price', true);
				while($related_query->have_posts()) : $related_query->the_post(); ?>
				<li>
					<?php woocommerce_get_template_part( 'content', 'product' ); ?>
				</li>
				<?php endwhile; wp_reset_postdata(); ?>
			</ul>
		</div>
	</section>
<? endif; ?> -->

<?php if ( ( is_page(12) || is_page(13) || is_page(14) || is_page(542) ) ) : ?>
	<section id="journey">
		<div class="wrap">
			<h2>You look like you're ready.</h2>
			<p>{CLICK ONE OF THE LINKS BELOW TO START YOUR EPIC CUSTOM SOCK JOURNEY}</p>
			<a href="<?php echo get_site_url() ?>/custom-made-socks/?step=2&type=athletic" class="btn darkblue">
				Shop Custom Athletic Socks
			</a>
			<a href="<?php echo get_site_url() ?>/custom-made-socks/?step=3&type=dress" class="btn purple">
				Shop Custom Dress Socks
			</a>
			<a href="<?php echo get_site_url() ?>/custom-made-socks/?step=2&type=marketing" class="btn teal">
				Shop Custom Marketing Socks
			</a>
		</div>
	</section>
<?php endif; ?>

<?php if( !is_page(14) ) : ?>
	<section id="faq">
		<div class="wrap">
			<h2>Still have questions?</h2>
			<div>
				<a href="<?php echo get_permalink(14); ?>" class="btn purple">Read Our FAQ</a>
				<span class="or">or</span>
				<a href="tel:+18885579185" class="btn teal">Call (888) 557-9185</a>
			</div>
		</div>
	</section>
<?php endif; ?>

<footer>
	<div class="icons">
		<div>
			<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/css-logo-teal.svg" alt="Custom Sock Shop Logo" />
		</div>
		<div>
			<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/authorize.svg" alt="Custom Sock Shop approved by authroize.net" />
			<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/made-in-usa.svg" alt="Custom Sock Shop made in the USA" />
			<a href="https://www.facebook.com/CustomSockShop/" target="_blank">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/facebook.svg" alt="Custom Sock Shop Facebook profile" />
			</a>
			<a href="https://www.linkedin.com/company/customsockshop" target="_blank">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/linkedin.svg" alt="Custom Sock Shop Linkedin profile" />
			</a>
			<a href="https://www.instagram.com/customsockshop/" target="_blank">
				<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/ig.svg" alt="Custom Sock Shop Instagram profile" />
			</a>
		</div>
	</div>
	<div class="info">
		<div><?php dynamic_sidebar( 'footer-contact' ); ?></div>
		<div><?php dynamic_sidebar( 'footer-newsletter' ); ?></div>
		<div><?php dynamic_sidebar( 'footer-menu-one' ); ?></div>
		<div><?php dynamic_sidebar( 'footer-menu-two' ); ?></div>
		<!-- <div><?php dynamic_sidebar( 'footer-menu-three' ); ?></div> -->
	</div>
</footer>

<section class="copyright">
	<div class="center-content">
		<p>© <?php echo date('Y'); ?> Custom Sock Shop. All Rights Reserved.</p>
		<a target="_blank" href="https://element5digital.com">
			<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/e5-credit.svg" alt="Crafted by Element5 Digital" />
		</a>
	</div>
</section>



<?php wp_footer(); ?>

</body>

</html>