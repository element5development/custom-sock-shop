// requires: liquidpix-all-logos.js
var $ = jQuery;
(function () {
  // Add version on each major version release.
  // Must contain a list of acceptable versions to allow rendering using different version
  var versions = {
      '1': true,
      '2': true,
      '3': true,
      '4': true,
      '5': true,
    },
    UNDEFINED,
    NULL = null,
    isNil = function (v) {
      return v === NULL || v === UNDEFINED
    };

  var $ = jQuery,
    qsearch = (location.search || '').replace('?', '&'),
    _qs = (function (qs) {
      var q = {};
      if (qsearch) {
        qs = qs.replace(/&(.+?)=(.+?)/, function (fullmatch, part1, part2) {
          if (part1) {
            q[part1] = part2;
          }
        });
      }
      return q;
    }(qsearch)),
    liquidPixlesConfig = {},
    needsLiquidPixels = true;

    needsLiquidPixels = $('[name=enable_liquid_pixels]').val() == 1;

    if (!needsLiquidPixels) {
      window.Elm5LiquidPix = {};
    }
    else {

      window.initLiquidPixels = function initLiquidPixels(lp) {

        var specFormContainer = $('form.cart');
        liquidPixlesConfig.specFormContainer = specFormContainer

        // init version and options from query string (mostly for debugging)
        initOptionsFromQS()

        initCustomConfigs(specFormContainer, lp)
        initSpecialSocks(specFormContainer, lp)

        initNudges(specFormContainer);
        initScroller(specFormContainer);
        initSubmitButtonFix(specFormContainer);

        if (window.Elm5LiquidPix) {
          Elm5LiquidPix.init(liquidPixlesConfig);
        }
      };

      if (window.Elm5LiquidPix) {
        initLiquidPixels(Elm5LiquidPix);
      } else {
        window.Elm5LiquidPixReady = window.Elm5LiquidPixReady || [];
        window.Elm5LiquidPixReady.push(function (lp) {
          initLiquidPixels(lp);
        });

      }

    function initScroller(container, lp) {
      var scroller = $('.lpScrollToSock');
      if (!scroller.length) {
        scroller = $('<div class="lpScrollToSock">Check the sock</div>').appendTo($('body'));
        scroller.click(function () {
           var top = ($("#specPreviewContainer").offset().top) - ($("#header").height()) - 15;
           $('html, body').animate({
              scrollTop: top
            }, 1000);
        });

      }
    }

    function initSubmitButtonFix(container, lp) {
      var form = container;
      var size = container.find('select[name$="-sock-size"], input[type=checkbox][name$="-sock-size[]"]');
      var sizeIsCheckbox = size.attr('type') === 'checkbox';
      var sizeGroup = size.closest('.product-addon');
      var sizeRow = size.closest('.form-row');
      var sizeError = sizeRow.find('.size-error');

      form.submit(function (event) {
        var sizeVal = sizeIsCheckbox ? size.prop('checked') : size.val();
        if (!size.length || sizeVal) {
          return;
        }

         var top = (sizeGroup.offset().top) - ($("#header").height()) - 15;
         $('html, body').animate({
            scrollTop: top
          }, 1000);
         if (!sizeIsCheckbox) {
           size.css({
             'border-color': 'red',
             'background-color': "#ffeeee"
           });
         }
         if (!sizeError.length) {
           sizeError = $('<p class="size-error">Please select a size to continue</p>').appendTo(sizeRow)
         }
         sizeError.css({color: 'red'})
         event.preventDefault();
      })

    }

    function initOptionsFromQS(liquidPixlesConfig, lp) {
      if (_qs['v'] && versions[_qs['v']]) {
        liquidPixlesConfig['version'] = _qs['v'];
      }
      if (_qs['nudgeOffset']) {
        liquidPixlesConfig['nudgeOffset'] = _qs['nudgeOffset'];
      }
      if (_qs['lpdebug']) {
        liquidPixlesConfig['lpdebug'] = _qs['lpdebug'];
      }
      if (_qs['debug']) {
        liquidPixlesConfig['debug'] = _qs['debug'];
      }
    }

    function initNudges(specFormContainer, lp) {
      specFormContainer.find('.product-addon[class*="-nudge"] > p > input').each(function () {
        var elm = $(this);
        var value = elm.val();
        var parent = elm.closest('p[class*="-nudge-"]');
        var directionMap = {
          up: 'vertical',
          down: 'vertical',
          left: 'horizontal',
          right: 'horizontal',
        };
        var valueMap = {
          'up': -1,
          'down': 1,
          'left': -1,
          'right': 1,
        }
        var direction = directionMap[value] || '';
        var directionValue = (valueMap[value] || 0);

        $('<button class="nudger" ' +
          'value="' + directionValue + '" ' +
          'data-offset="' + directionValue + '" ' +
          'data-direction="' + direction + '" ' +
          'data-offset-name="' + value +
          '">' +
          '<span class="dashicons dashicons-arrow-' + value + '-alt2"></span>' +
          '</button>')
          .appendTo(parent);
        // .click(function(e) {
        //   e.preventDefault();
        // var oldVal = (1 * $(this).val());
        // var val = oldVal + directionValue;
        // console.log([oldVal, val]);
        // $(this).closest('.product-addon').find('button[data-' + direction + ']').val(val);
        // });
      });
    }

    function initCustomConfigs(specFormContainer, lp) {
      // special disabling of logo color by default - which manes no color as default color for logo
      var sockize = lp.get('sockize'),
        sock = sockize(),
        getOverrides = lp.get('inputsOverrides'),
        overrides = getOverrides(sock),
        inputs = $.extend(true, {}, lp.get('inputs'), overrides),
        catKey,
        cat,
        value,
        defkey,
        def;

      for (catKey in inputs) {
        cat = inputs[catKey]
        for (defkey in cat) {
          def = cat[defkey];
          value = def.defaultValue
          if(def.type ==='checkbox' || def.type === 'radio') {
            if (def.initVal === false) {
              specFormContainer.find(def.selector).filter(':checked').prop('checked', false);
            }
            if (def.initVal === true) {
              if (isNil(value)) {
                value = specFormContainer.find(def.selector).filter(':first').val()
              }
              if (isNil(value)) {
                specFormContainer.find(def.selector).filter(':first').prop('checked', true);
              } else {
                specFormContainer.find(def.selector).filter('[value="' + value + '"]').prop('checked', true);
              }
            }
          } else {

          }

        }
      }
    }

    function initSpecialSocks(specFormContainer, lp) {
      // special disabling of logo color by default - which manes no color as default color for logo
      var sockize = lp.get('sockize'), sock = sockize();

      //[hackfix] detect dress sock and disable stock logo button
      if (/dress/.test(sock))  {
        lp.set('noStockLogo', true);
         specFormContainer.find('[name$="-stock-logo"]')
           .data('noStockLogo', true);
         specFormContainer.find('.logoOptionsViewButton')
           .css('display', 'none');
      }

      // sock with only one size option - check/select only sock size option (checkbox) by default (if it's checkbox)
      var socksizes = specFormContainer.find('.product-addon-sock-size input[type=checkbox]');
      if (socksizes.length === 1) {
        socksizes.first().prop('checked', true);
      }

    }
  }

}());

