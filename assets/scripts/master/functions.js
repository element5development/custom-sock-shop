//Add Class based on Touch or NonTouch Device
var $ = jQuery;
if ("ontouchstart" in document.documentElement) {
	$('body').addClass('touch');
} else {
	$('body').addClass('no-touch');
}

//Responsive WP Images
$(document).ready(function ($) {
	$('.entry img').each(function () {
		$(this).removeAttr('width')
		$(this).removeAttr('height');
	});

	$('.wp-caption').each(function () {
		$(this).css("width", "100%");
	});

});

//Fading Quotes
(function () {

	var quotes = $(".quotes");
	var quoteIndex = -1;

	function showNextQuote() {
		++quoteIndex;
		quotes.eq(quoteIndex % quotes.length)
			.fadeIn(2000)
			.delay(5000)
			.fadeOut(2000, showNextQuote);
	}

	showNextQuote();

})();

//Remove Empty WP <p></p> tags
$('p').each(function () {
	var $this = $(this);
	if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
		$this.remove();
});

//Parallax (Data Speed)
$('section#headerimg').each(function () {
	var $obj = $(this);

	if ($(window).width() > 1200) {
		$(window).scroll(function () {
			var yPos = -($(window).scrollTop() / $obj.data('speed'));
			var bgpos = '50% ' + yPos + 'px';
			$obj.css('background-position', bgpos);
		});
	}
});

// $('a').each(function() {
//    var a = new RegExp('/' + window.location.host + '/');
//    if(!a.test(this.href)) {
//        $(this).click(function(event) {
//            event.preventDefault();
//            event.stopPropagation();
//            window.open(this.href, '_blank');
//        });
//    }
// });

//Main Nav Menu Reveal
$("header[role='banner'] nav ul li.shop a").click(function () {
	$("header[role='banner'] nav ul li.shop").toggleClass('current');

	$(".menureveal").slideToggle("fast", function () {
		// Animation complete.
	});
});

$("div.search-toggle").click(function () {
	$(this).toggleClass('clicked');
	$("#search-container").slideToggle("fast", function () {});
});

//Flexslider
$(window).load(function () {
	$('.flexslider').flexslider({
		animation: "slide",
		pauseOnAction: false,
		pauseOnHover: false
	});
});

//Mobile Menu
$("#cartmenu").click(function () {
	$(this).toggleClass('clicked');
	$("ul#cart-slidedown").slideToggle();
	$("ul#cart-slidedown").css("z-index", "1000");
});

//Add Span Class to "Shop" Nav Link
$('header[role="banner"] #headerbottom nav[role="navigation"] ul li.shop a').wrapInner('<span />');

//Add Active Class to Subcategories Menu
$(function () {
	$('ul#subcategories li a[href^="/' + location.pathname.split("/")[1] + '"]').addClass('active');
});

//Fading Headings in Header
jQuery(function ($) {

	function fade_home_top() {
		if ($(window).width() > 100) {
			window_scroll = $(this).scrollTop();
			$("#headerimg .headingcontainer h2").css({
				'opacity': 1 - (window_scroll / 550)
			});
		}
	}
	$(window).scroll(function () {
		fade_home_top();
	});

});

//Change Transparency of Header Menu on Scroll
$(window).scroll(function () {
	var scroll = $(window).scrollTop();
	if (scroll >= 100) {
		//clearHeader, not clearheader - caps H
		$("header[role='banner']").addClass("darkscroll");
		$("header[role='banner'] .border").hide();
	} else {
		$("header[role='banner']").removeClass("darkscroll");
		$("header[role='banner'] .border").show();
	}
});

//Initialize Mean Menu Mobile Menu
jQuery(document).ready(function () {

	//Remove Empty Li On Related Posts
	jQuery('#related li').each(function () {
		var $this = jQuery(this);
		if ($this.html().replace(/\s|&nbsp;/g, '').length == 0)
			$this.remove();
	});

	jQuery('.fancybox_video').fancybox({
		openEffect: 'none',
		closeEffect: 'none',
		helpers: {
			media: {}
		}
	});

	//Add the backack ground color
	jQuery('#parts-bg-color-selector').css("background-color", "#945f94");
	jQuery('#txt-color-selector').css("background-color", "#f00");

	jQuery(".variations .size_chart_button").bind("click", function () {
		setTimeout(function () {
			jQuery(".pp_woocommerce .pp_content_container::after").css("clear", "none");
			jQuery(".pp_woocommerce .pp_content_container::before").css("content", "none");
			jQuery(".pp_woocommerce .pp_content").css("height", "auto");
			jQuery(".pp_woocommerce .pp_details").css("padding-top", "0px");
		}, 1500);
	});

	jQuery('.shipping_address').hide();

	$(".menureveal li.custom-athletic-socks a").attr("href", "/custom-made-socks/?step=2&type=athletic");
	$(".menureveal li.custom-made-socks a").attr("href", "/custom-made-socks");

});

jQuery(window).load(function () {
	jQuery('#nav').meanmenu();
	jQuery('.shipping_address').hide();
});

var $container = $('.previous-months .flex-container').infiniteScroll({
	// options
	path: '.sotm-pagination a',
	append: '.past-sock',
	button: '.load-more',
	scrollThreshold: false,
	history: false,
	checkLastPage: true,
	status: '.page-load-status',
	// disable loading on scroll
	loadOnScroll: false,
});

var $viewMoreButton = $('.load-more');
$viewMoreButton.on('click', function () {
	// load next page
	$container.infiniteScroll('loadNextPage');
	// enable loading on scroll
	$container.infiniteScroll('option', {
		loadOnScroll: true,
	});
	// hide button
	$viewMoreButton.hide();
});

//ACCORDION
jQuery(".collapseomatic").click(function () {
	jQuery(this).next().toggleClass("is-active");
	jQuery(this).toggleClass('colomat-close');
});

//FORM UPLOAD
if ($('input[type="file"]').length > 0) {
	var fileInput = document.querySelector("input[type='file']");
	var button = document.querySelector("input[type='file']+small");
	fileInput.addEventListener("change", function (event) {
		if (!$(button).data('originalValue')) {
			$(button).data('originalValue', $(button).html());
		}
		button.innerHTML = this.value;
		fileInput.classList.add("file-uploaded");
	});
}

var $ = jQuery;
$("h3#ship-to-different-address").prepend("Is this item a Gift? Are you shipping to a different address?");

//SEARCH MODAL
$("button.search-toggle-open").click(function () {
	$(".search-modal").addClass("active");
});
$("button.search-toggle-close").click(function () {
	$(".search-modal").removeClass("active");
});

//MOBILE NAV
$("button.mobile-menu-toggle").click(function () {
	$("nav.primary .right").toggleClass("active");
});

//BULK PRICING
$("section.bulk-pricing ul li").click(function () {
	var button = $(this).children('button')
	var words = button.text().split(' ');
	words.splice(2, 0, 'for');
	var index = $(this).parent().children().index(this);
	var integer = parseInt(index, 10) + 1;
	$("section.bulk-pricing img:nth-child(n+1)").css('opacity', '0');
	$('section.bulk-pricing div h3').text(words.join(' '));
	do {
		$("section.bulk-pricing img:nth-child(" + integer + ")").css('opacity', '1');
		integer = integer - 1;
	} while (integer > 0);
});

//BULK PRICING
$('.gallery-icon a').featherlightGallery({
	galleryFadeIn: 100,
	/* fadeIn speed when slide is loaded */
	galleryFadeOut: 300 /* fadeOut speed before slide is loaded */
});

/*----------------------------------------------------------------*\
	DESIGN YOUR OWN TEMPLATE
\*----------------------------------------------------------------*/
$(document).ready(function ($) {
	$("button.activate-athletic-style").click(function () {
		$(".step-one").removeClass("is-active").addClass('is-complete');
		$(".step-two").addClass("is-active");
		$(".useage-headings").removeClass("is-active");
		$(".athletic-headings").addClass("is-active");
		$(".usage.options").removeClass("is-active");
		$(".style-athletic.options").addClass("is-active");
		$(window).scrollTop(0);
	});
	$("button.activate-standard-style").click(function () {
		$(".step-one").removeClass("is-active").addClass('is-complete');
		$(".step-two").addClass("is-active");
		$(".useage-headings").removeClass("is-active");
		$(".standard-headings").addClass("is-active");
		$(".usage.options").removeClass("is-active");
		$(".style-standard.options").addClass("is-active");
		$(window).scrollTop(0);
	});
	$("button.activate-dress-style").click(function () {
		$(".step-one").removeClass("is-active").addClass('is-complete');
		$(".step-two").addClass("is-active");
		$(".useage-headings").removeClass("is-active");
		$(".dress-headings").addClass("is-active");
		$(".usage.options").removeClass("is-active");
		$(".style-dress.options").addClass("is-active");
		$(window).scrollTop(0);
	});
});