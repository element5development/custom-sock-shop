// requires: liquidpix.js

/*******************  Custom Logo Upload code base **************************/

(function (){
	var fn = function () {


	var UNDEFINED,
		UNFN = function () {},
		defLogos,
    noStockLogo = false,
		ajaxUrl = window.ajax_url || (window.woocommerce_params && window.woocommerce_params.ajax_url) || '/wp-admin/admin-ajax.php',
		specFormContainer = $('form.cart'),
		logoContainer = injectStockLogoSelector(specFormContainer),
		logoOptionsHeader = logoContainer.find('.addon-description'),
		mainContanier = logoContainer, //.form-row

		logoIdInputbox = mainContanier.find('.logoIdInputbox'), //
		logoIdInputboxContainer = logoIdInputbox.closest('p.form-row'),
		stockLogosViewButton = mainContanier.find('.logoOptionsViewButton'),

		customLogoUploadContainer = mainContanier.find('.customLogoUploadContainer'),
		customLogoUploadContainerAvailable = customLogoUploadContainer.length > 0,

		curtain = customLogoUploadContainer.find('.curtain'),
		progress = customLogoUploadContainer.find('.progress'),
		deleteButton = customLogoUploadContainer.find('.deleteCustomLogoFile'),
		uploadButton = customLogoUploadContainer.find('.uploadCustomLogoFile'),
		viewerImage = customLogoUploadContainer.find('img.customLogoView'),
		fileInputElement = customLogoUploadContainer.find('input:file'),
		fileInputElementValueContainer = customLogoUploadContainer.find('.logoFileInput'),
		uploadButtonBackgroundColor = uploadButton.css('background-color'),
		uploadButtonColor = uploadButton.css('color'),

		showCurtain = function () {
			curtain.removeClass('noshow');
		},
		showProgress = function () {
			progress.removeClass('noshow');
		},
		hideCurtain = function () {
			curtain.addClass('noshow');
		},
		hideProgress = function () {
			progress.addClass('noshow');
		},
		showImage = window.showLogoImageInSpecForm = function (src) {
			// console.log(viewerImage, src);
			viewerImage.attr('src', src);
			viewerImage.parent().removeClass('browsenoshow').fadeIn();
			logoOptionsHeader.hide();
			showDeleteButton();
			hideUploadButton();
		},
		hideImage = function () {
			viewerImage.attr('src', null);
			viewerImage.parent().addClass('browsenoshow').hide();
		},
		showStockControls = function (src) {
	    noStockLogo = logoIdInputbox.data('noStockLogo');
	    if (!noStockLogo) {
        logoIdInputboxContainer.show();
        stockLogosViewButton.show();
      }
		},
		hideStockControls = function () {
			logoIdInputboxContainer.hide();
			stockLogosViewButton.hide();
		},
		showDeleteButton = function () {
			deleteButton.removeClass('browsenoshow').fadeIn();
		},
		hideDeleteButton = function () {
			deleteButton.addClass('browsenoshow').fadeOut();
		},
		showUploadButton = function () {
			uploadButton.parent().find("input[type='file']+small")
					.html(uploadButton.data('originalValue') || '(MAX FILE SIZE 200 MB)');
			uploadButton.removeClass("file-uploaded");
			uploadButton.parent().removeClass('browsenodisplay');
		},
		hideUploadButton = function () {
			uploadButton.parent().addClass('browsenodisplay'); //browsenoshow //browsenodisplay
		},
		displayCustomLogoOptions = function (imageUrl) {
			deleteButton.data('sourceUrl', true);
			fileInputElementValueContainer.val(imageUrl).trigger('change');
			logoIdInputbox.val('');
			hideStockControls();
			hideUploadButton();
			logoOptionsHeader.hide();
			showDeleteButton();
			if (imageUrl) {
				showImage(imageUrl);
			}
		},
		displayDefaultLogoOptions = function () {
			deleteButton.data('sourceUrl', false);

			hideImage();
			hideDeleteButton();
			showUploadButton();
			showStockControls();
			logoOptionsHeader.show();

			fileInputElementValueContainer.val('').trigger('change');
			logoIdInputbox.val('');

		},
		removeStockLogo = function () {
			logoIdInputbox.val('').trigger('change');
			hideImage();
			logoOptionsHeader.show();
			hideDeleteButton();
			showUploadButton();
		},
		beforeAjax = function () {
			showCurtain();
			showProgress();
		},
		afterAjax = function () {
			hideCurtain();
			hideProgress();
		},

		beforeAjaxUpload = function () {
			uploadButton.css('color', uploadButtonBackgroundColor);
			beforeAjax();
		},
		afterAjaxUpload = function () {
			afterAjax();
			uploadButton.css('color', uploadButtonColor);
		},

		startFileBrowser = function (e) {
			fileInputElement.change(function (evt) {
				logoFileSelectedForUpload(evt);
			});
			fileInputElement.trigger('click');
		};

	function initiateLogoFileDelete(evt) {
		var fileToDelete = fileInputElement.data('customLogoFile'),
			confirmation = fileToDelete && (confirm('Do you really want to permanently delete this image?'));
		if (!fileToDelete || confirmation) {
			deleteUploadedLogoFile(fileToDelete);
		}
	}

	function deleteUploadedLogoFile(fileToDelete) {
		var data = {
			'action': 'delete_custom_logo',
			'filename': fileToDelete
		};

		beforeAjax();
		$.ajax({
			url: ajaxUrl,
			data: data,
			type: "POST",
			dataType: "json",
			success: displayDefaultLogoOptions,
			error: UNFN,
			complete: afterAjax
		});
	}

	function afterCustomLogoUploaded(response) {
		response = response || {};
		var isSuccess = response.success,
			imageFile = response.file,
			imageUrl = response.url;

		fileInputElement.data('customLogoFile', imageFile);
		fileInputElement.data('imageurl', imageUrl);
		displayCustomLogoOptions(imageUrl);

	}

	function logoFileSelectedForUpload(evt) {
		var formdata = new FormData(),
			fileInput = fileInputElement[0],
			files = fileInput.files,
			file = files && files[0],
			fileType = file && file.type,
			isImage = /image.*/.test(fileType),
			//productId = $('input[name="product_id"]').val(),
			i;

		if (!formdata || !isImage) {
			return;
		}
		if (isImage) {
			showLocalImageFile(file);
		}

		formdata.append('action', 'upload_logo_image');
		//formdata.append('pid', productId);
		formdata.append("images", file);

		beforeAjaxUpload();

		$.ajax({
			url: ajaxUrl,
			data: formdata,
			type: "POST",
			dataType: "json",
			processData: false,
			contentType: false,
			success: afterCustomLogoUploaded,
			error: UNFN,
			complete: afterAjaxUpload
		});




	}

	function showLocalImageFile(file) {
		if (window.FileReader) {
			var reader = new FileReader();
			reader.onloadend = function (e) {
				//showImage(file);
			};
			reader.readAsDataURL(file);
		}
	}



	defLogos = function () {
			return {
				"ms2": {
					"path": "77",
					"name": "Wolf"
				},
				"ms1": {
					"path": "65",
					"name": "Bulldog"
				},
				"ms10": {
					"path": "70",
					"name": "Spartan"
				},
				"ms32": {
					"path": "63",
					"name": "Vollyboll"
				},
				"ms29": {
					"path": "64",
					"name": "Tennis Ball"
				},
				"ms33": {
					"path": "69",
					"name": "Vollyball"
				},
				"ms4": {
					"path": "79",
					"name": "Cougar"
				},
				"ms12": {
					"path": "71",
					"name": "Spartans"
				},
				"ms17": {
					"path": "142",
					"name": "Indian"
				},
				"ms36": {
					"path": "74",
					"name": "Football"
				},
				"ms39": {
					"path": "78",
					"name": "Paw"
				},
				"ms60": {
					"path": "75",
					"name": "Clover"
				},
				"ms62": {
					"path": "54",
					"name": "Lacrosse"
				},
				"ms162clr": {
					"path": "76",
					"name": "Lions"
				},
				"vikings": {
					"path": "56",
					"name": "Viking"
				},
				"ms51": {
					"path": "73",
					"name": "Golf"
				},
				"ms47": {
					"path": "58",
					"name": "Cheerleader"
				},
				"ms45": {
					"path": "59",
					"name": "Megaphone"
				},
				"ms31": {
					"path": "60",
					"name": "Soccerball"
				},
				"ms30": {
					"path": "61",
					"name": "Basketball"
				},
				"ms34": {
					"path": "72",
					"name": "Tennis Balls"
				},
				"ms6": {
					"path": "80",
					"name": "Eagle"
				},
				"ms8": {
					"path": "81",
					"name": "Cardinals"
				},
				"ms13": {
					"path": "82",
					"name": "Mustang"
				},
				"ms14": {
					"path": "83",
					"name": "Mustang"
				},
				"ms19": {
					"path": "84",
					"name": "Fighting Irish"
				},
				"ms22": {
					"path": "85",
					"name": "Eagle"
				},
				"ms23": {
					"path": "86",
					"name": "Pirate"
				},
				"ms26": {
					"path": "87",
					"name": "Bull"
				},
				"ms27": {
					"path": "88",
					"name": "Cowboy"
				},
				"ms28": {
					"path": "89",
					"name": "Rebel"
				},
				"ms37": {
					"path": "90",
					"name": "Basketball"
				},
				"ms38": {
					"path": "91",
					"name": "Basketball"
				},
				"ms40": {
					"path": "92",
					"name": "Bear Paw"
				},
				"ms41": {
					"path": "93",
					"name": "Paw"
				},
				"ms50": {
					"path": "94",
					"name": "Rackets &amp; Ball"
				},
				"ms59": {
					"path": "95",
					"name": "Dancer"
				},
				"ms61": {
					"path": "96",
					"name": "Track Shoe"
				},
				"ms64": {
					"path": "97",
					"name": "Wing Shoe"
				},
				"ms65": {
					"path": "98",
					"name": "Alien"
				},
				"ms66": {
					"path": "99",
					"name": "Apple"
				},
				"ms67": {
					"path": "100",
					"name": "Angel"
				},
				"ms68": {
					"path": "101",
					"name": "Volleyball"
				},
				"ms69": {
					"path": "102",
					"name": "Bear"
				},
				"ms70": {
					"path": "103",
					"name": "Paws"
				},
				"ms71": {
					"path": "104",
					"name": "Pillar"
				},
				"ms72": {
					"path": "105",
					"name": "Volleyball"
				},
				"ms73": {
					"path": "106",
					"name": "MC Volleyball"
				},
				"ms74": {
					"path": "107",
					"name": "Music Note"
				},
				"ms75": {
					"path": "108",
					"name": "Newly Weds"
				},
				"ms76": {
					"path": "109",
					"name": "Patriots"
				},
				"ms77": {
					"path": "110",
					"name": "Paw"
				},
				"ms78": {
					"path": "111",
					"name": "Peace"
				},
				"ms79": {
					"path": "112",
					"name": "Penguin"
				},
				"ms80": {
					"path": "113",
					"name": "Heart"
				},
				"ms81": {
					"path": "114",
					"name": "Heart"
				},
				"ms82": {
					"path": "115",
					"name": "Heart"
				},
				"ms83": {
					"path": "116",
					"name": "Butterfly"
				},
				"ms84": {
					"path": "117",
					"name": "Fleur De Lis"
				},
				"ms85": {
					"path": "118",
					"name": "Handicap"
				},
				"ms86": {
					"path": "119",
					"name": "Cancer Ribbo"
				},
				"ms87": {
					"path": "120",
					"name": "Maple Leaf"
				},
				"ms88": {
					"path": "121",
					"name": "Unicorn"
				},
				"ms89": {
					"path": "122",
					"name": "Torch"
				},
				"ms90": {
					"path": "123",
					"name": "US Flag"
				},
				"ms91": {
					"path": "124",
					"name": "Pink Ribbon"
				},
				"ms92": {
					"path": "125",
					"name": "Star"
				},
				"ms93": {
					"path": "126",
					"name": "Thunderbolt"
				},
				"ms94": {
					"path": "127",
					"name": "Pirate"
				},
				"ms95": {
					"path": "128",
					"name": "Rackets"
				},
				"ms96": {
					"path": "129",
					"name": "Rebel"
				},
				"ms97": {
					"path": "130",
					"name": "Runner"
				},
				"ms98": {
					"path": "131",
					"name": "Shell"
				},
				"ms99": {
					"path": "132",
					"name": "Women Ribbon"
				},
				"ms100": {
					"path": "133",
					"name": "Paws"
				},
				"ms101": {
					"path": "134",
					"name": "Star"
				},
				"ms102": {
					"path": "135",
					"name": "Bulldog"
				},
				"ms103": {
					"path": "136",
					"name": "Bulldog"
				},
				"ms104": {
					"path": "137",
					"name": "Cougar"
				},
				"ms105": {
					"path": "138",
					"name": "Bird"
				},
				"ms106": {
					"path": "139",
					"name": "Hawk"
				},
				"ms107": {
					"path": "140",
					"name": "Track"
				},
				"ms108": {
					"path": "141",
					"name": "Wing"
				},
				"ms110": {
					"path": "143",
					"name": "MC Bee"
				},
				"ms111": {
					"path": "144",
					"name": "Horse"
				},
				"ms112": {
					"path": "145",
					"name": "Wildcat"
				},
				"ms113": {
					"path": "146",
					"name": "Field Hockey"
				},
				"ms114": {
					"path": "147",
					"name": "Cross Country"
				},
				"ms115": {
					"path": "148",
					"name": "Bull"
				},
				"ms116": {
					"path": "149",
					"name": "Bus"
				},
				"ms117": {
					"path": "150",
					"name": "School Bus"
				},
				"ms118": {
					"path": "151",
					"name": "Butterfly"
				},
				"ms119": {
					"path": "152",
					"name": "Megaphone"
				},
				"ms120": {
					"path": "153",
					"name": "Star"
				},
				"ms122": {
					"path": "155",
					"name": "Bone"
				},
				"ms123": {
					"path": "156",
					"name": "Devil"
				},
				"ms124": {
					"path": "157",
					"name": "Caduceus"
				},
				"ms125": {
					"path": "158",
					"name": "Dragon"
				},
				"ms126": {
					"path": "159",
					"name": "Eagle"
				},
				"ms127": {
					"path": "160",
					"name": "Bird"
				},
				"ms128": {
					"path": "161",
					"name": "Elephant"
				},
				"ms129": {
					"path": "162",
					"name": "Thunderbolt"
				},
				"ms130": {
					"path": "163",
					"name": "Lightning"
				},
				"ms131": {
					"path": "164",
					"name": "Bird"
				},
				"ms132": {
					"path": "165",
					"name": "Bird"
				},
				"ms133": {
					"path": "166",
					"name": "Bird"
				},
				"ms137": {
					"path": "170",
					"name": "Weightlifting"
				},
				"ms138": {
					"path": "171",
					"name": "Crocodile"
				},
				"ms139": {
					"path": "172",
					"name": "Field Hockey"
				},
				"ms140": {
					"path": "173",
					"name": "Firepit"
				},
				"ms141": {
					"path": "174",
					"name": "Guitar"
				},
				"ms142": {
					"path": "175",
					"name": "Hair Comb"
				},
				"ms143": {
					"path": "176",
					"name": "Healthy Heart"
				},
				"ms144": {
					"path": "177",
					"name": "Bee"
				},
				"ms145": {
					"path": "178",
					"name": "Footprint"
				},
				"ms146": {
					"path": "179",
					"name": "Horseshoes"
				},
				"ms147": {
					"path": "180",
					"name": "Mustang"
				},
				"ms148": {
					"path": "181",
					"name": "Indian"
				},
				"ms149": {
					"path": "182",
					"name": "Children"
				},
				"ms150": {
					"path": "183",
					"name": "Red Robin"
				},
				"ms151": {
					"path": "184",
					"name": "Dragon"
				},
				"ms152": {
					"path": "185",
					"name": "Golf Putter"
				},
				"ms153": {
					"path": "186",
					"name": "Stethascope"
				},
				"ms154": {
					"path": "187",
					"name": "Knight"
				}
			};
		};


  window.initLogoBox = function (elm, specFormContainer, inputs) {

			var logos = defLogos(),
				logoId,
				logoItem,
				logo,
				logoName,
				logoPath,
				htmlItems,
				htmls = [],
				html = "",
				logoInputSelector = inputs.logo.id.selector,
				logoInput = specFormContainer.find(logoInputSelector),
				mainPath = '/wp-content/themes/customsockshop/dist/images/stock-logos/';

			// elm.removeClass('fancybox-iframe fancybox');

			for (logoId in logos) {
				logoItem = logos[logoId];
				logo = logoItem.path + '.png';
				logoName = logoItem.name;
				logoPath = mainPath + logo;
				htmlItems = [];
				htmlItems.push("<div class='builtLogo'>");

				htmlItems.push("<figure ");
				htmlItems.push(" data-id='");
				htmlItems.push(logoId);
				htmlItems.push("' data-url='");
				htmlItems.push(logoPath);
				htmlItems.push("' data-name='");
				htmlItems.push(logoName);
				htmlItems.push("' >");
				htmlItems.push("<img src='");
				htmlItems.push(logoPath);
				htmlItems.push("' alt='");
				htmlItems.push(logoId + ' ' + logoName);
				htmlItems.push("' ");
				htmlItems.push(" class=''/>");
				htmlItems.push("<figcaption>");
				htmlItems.push(logoId + ' ' + logoName);
				htmlItems.push("</figcaption>");
				htmlItems.push("</figure>");
				htmlItems.push("</div>");
				htmls.push(htmlItems.join("") + "");
			}

			html = "<div class='builtLogoContainer clearfix'>" + htmls.join("") + "</div>";

			$(document).on('click', elm, function () {

				elm.fancybox({
					'modal': false,
					'hideOnContentClick': false,
					'titleShow': false,
					'hideOnOverlayClick': true,
					title: "Select Custom Sock Shop Logo",
					content: html,
					onComplete: function () {

					}
				});
			});

			$(document).on('click', logoInputSelector, function () {
				if (this) {
					elm.click();
				}
			});

			$(document).on('click', '.builtLogo', function () {
				var element = $(this),
					pastVal = logoIdInputbox.val() || '',
					logoElement = element.find('figure'),
					id = "#" + logoElement.data('id'),
					img = logoElement.data('url'),
					name = logoElement.data('name');

				if (pastVal !== id) {
					logoIdInputbox.val(id).trigger('change');
					fileInputElementValueContainer.val(''); // dont add id here as url and id log entries are separated
				}

				showLogoImageInSpecForm(img);

				$.fancybox.close();
			});

			// logoIdInputbox.hide();

			return logos;
		};

  function injectStockLogoSelector(specFormContainer) {

    var uploadButton = specFormContainer.find('input[name$="-logo-on-sock"],input[name$="-add-logo"]'),
			grandParent  = uploadButton.closest('.product-addon'),
      parent = uploadButton.closest('.form-row'),
      hasUploadButton = !!uploadButton.length,
			logoTextInputAttr = uploadButton.attr('name'),

			logoTextInput,
			idInput = specFormContainer.find('[name$="-stock-logo"]'),
      idInputParent = idInput.closest('.form-row'),

      stockButton = parent.find('.viewbtn.logoBox'),
      alliedItems;

    noStockLogo = idInput.data('noStockLogo');

    if (hasUploadButton) {
			grandParent.find('.addon-description p').addClass('logoOptionsHeader');
			parent.addClass('customLogoUploadContainer');
    	uploadButton
				.attr({
						'id': 'logoFileInput',
						'accept': 'image/*',
						'data-multiple': 'false',
						'multiple': 'false',
					})
				.addClass('browsenodisplay');

			$('<input type="button" class="lpBtn viewbtn uploadCustomLogoFile" value="Upload File" />')
				.css({'display': 'block'})
				.insertBefore(uploadButton);

    	logoTextInput = $('<input type="hidden" class="logoFileInput" value="" >').insertBefore(uploadButton);
    	logoTextInput.attr('name', logoTextInputAttr);

			$('<input type="button" class="lpBtn viewbtn logoBox logoOptionsViewButton fancybox" ' +
          'value="Your Stock Logo Options" />').insertAfter(parent);

      $('<p class="browsenoshow" style="display: none;"><img src="" class="customLogoView"></p>' +
          '<input  type="button" style="display: none;" ' +
          'class="lpBtn noshow viewbtn browsenoshow deleteCustomLogoFile" value="Remove your logo" />' +
          '<div class="noshow fullfloaters curtain"></div>' +
          '<div class="noshow fullfloaters progress"></div>'
      ).appendTo(parent);

      idInput.addClass('logoIdInputbox browsenodisplay').prop('type', 'hidden');
      idInputParent.find('label').remove();
		  idInputParent.closest('.product-addon').remove();
      idInputParent.detach().insertAfter(parent);

      if (noStockLogo) {
        idInputParent.hide();
        idInputParent.find('+ .logoOptionsViewButton').hide();
      }
    	// $('<p class="form-row form-row-wide">' +
			// 	'<input type="hidden" name="logoStockId" class="logoIdInputbox" value="" >' +
			// 	'</p>'
			// ).insertAfter(parent);

    }

		return grandParent;

    /*

<div class="section item-8 specFormLogo">
    <div class="customLogoUploadContainer">
        <a href="JavaScript:void(0);" class="viewbtn uploadCustomLogoFile">Browse and Upload your logo</a>
        <br>
        <input name="image" id="logoFileInput" type="file" accept="image/*" class="browsenoshow" data-multiple="false"
               placeholder="Logo ID #">
        <br>
        <input name="logoFileInput" type="hidden" placeholder="Logo ID #">
        <p class="browsenoshow" style="display: none;">
            <img class="customLogoView">
        </p>
        <a href="JavaScript:void(0);" style="display: none;" class="noshow viewbtn deleteCustomLogoFile browsenoshow">Remove your logo</a>
        <div class="noshow fullfloaters curtain"></div>
        <div class="noshow fullfloaters progress"></div>
    </div>
    <a href="https://www.customsockshop.com/logos/" class="viewbtn logoBox logoOptionsViewButton">Select Your Stock Logo
        Options</a>
    <div class=" product-addon product-addon-logo-id">
        <h3 class="addon-name">Logo ID #</h3>
        <p class="form-row form-row-wide addon-wrap-6234-logo-id">
            <label>Enter logo ID # </label>
            <input type="text" class="logoIdInputbox"  placeholder="Logo ID #" style="display: none;">
        </p>
        <div class="clear"></div>
    </div>
</div>


*/


  }


	if (customLogoUploadContainerAvailable) {

    if (noStockLogo) {
	    logoIdInputboxContainer.hide();
      stockLogosViewButton.hide();
    }

		uploadButton.unbind('click').click(function (e) {
			e.preventDefault();
			fileInputElement.unbind('change');
			fileInputElement.data('customLogoFile', null);
			fileInputElement.val("");
			startFileBrowser(e);
			return false;
		});

		deleteButton.unbind('click').click(function (e) {
			// console.log('delete', e);
			e.preventDefault();

			var elm = $(this),
				sourceIsUrl = elm.data('sourceUrl');

			// console.log('delete', elm, sourceIsUrl, fileInputElement);

			if (!sourceIsUrl) {
				removeStockLogo();
				hideImage();
				return false;
			}

			fileInputElement.unbind('change');
			fileInputElement.val("");
			initiateLogoFileDelete(e);
			hideDeleteButton();
			return false;
		});
	}



}
	fn();
	// jQuery(document).ready(fn);
}());
/****************  Custom Logo Upload code base (end) ***********************/