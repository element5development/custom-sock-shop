(function ($) {
	'use strict';
	// CHANGE VERSION on each major version release.
	// VERSION is important and is used in various placed to generate the image
	var VERSION = 5;

	window.Elm5LiquidPix = window.Elm5LiquidPix || elm5LiquidPix();

	function elm5LiquidPix() {
		var UNDEFINED,
			ns = 'elm5liquidpix',
			ssl = location.protocol,
			lfOSUrl = ssl + '//custsock.liquifire.com/custsock?',
			that = {},
			config = {},
			allConfig = {},
			specFormContainer,
			attachListeners,
			detachListeners,
			renderOutput,
			updateData,
			updateDefaultData,
			renderData,
			imageLoaded,
			render,
			createUrl,
			skipUrlParts = null,
			//skipUrlParts = /stripes-.+?|text-.+?|logo-.+?|general-size|general-chain/,
			forceUrlParts = {
				'call': ['url[file:socks/gen.chain]']
			},
			//forceUrlParts = {},
			filteredData = {},
			data = {},
			rawData = {},
			urlParts = {},
			url = '',
			trim,
			trimDashEnd,
			deleteFromArray,
			textSizer,
			sanitizeText,
			alignText,
			colorize,
			multiColorize,

			defInputs,
			defInputsOverrides,
			defUrlMap,
			inputs,

			getPreviousValue,
			setPreviousValue,

			defSocks,
			socks,
			sock,
			sockize,
			face,
			defaultFace = 'left',
			faces,
			getFaces,
			sockFaceChainer,

			logos,
			logoize,

			initNudges,
			nudgeConfig = {
				offset: 10,
				resetOnLocationChange: true,
				resetOnAlignmentChange: false,
				logo: {
					vertical: 0,
					horizontal: 0,
				},
				text: {
					vertical: 0,
					horizontal: 0,
				},
			},
			nudgeItem,
			resetNudge,
			getNudgedValue,

			cleanLogoId,

			// validators
			isNil,
			isNotNil,
			ankletTextLogoCombi,
			isScarf,
			isBeanie,
			isScarfBeanie,
			isNotScarfBeanie,

			out,
			outField,
			outContainer,
			outMainImage,
			faceSelector,
			preloader,
			pinButton,
			shareContainer,
			shareButtonsContainer,
			shareButtons,
			shareExpandButton,
			pinBoxIt,
			positionPinnedBox,
			debug = false,
			lpdebug = false,
			version = VERSION,
			log,
			replaceAll = function (text, target, replacement) {
				return text.split(target).join(replacement);
			};

		that.init = function (options) {
			var ioverrides;
			options = options || [];
			specFormContainer = options.specFormContainer;
			version = options.version || version || '';
			debug = options.debug !== UNDEFINED ? options.debug : debug;
			lpdebug = options.lpdebug !== UNDEFINED ? options.lpdebug : lpdebug;
			ioverrides = defInputsOverrides(sockize());
			inputs = $.extend(true, {}, inputs, ioverrides);
			// console.log(options);
			// console.log(inputs);
			logos = initLogoBox(specFormContainer.find(".viewbtn.logoBox"), specFormContainer, inputs);
			initNudges(specFormContainer, options, inputs)
			attachListeners(options);
			that.refresh(options);
			config = options;
			allConfig = {options: options, inputs: inputs}

		};

		that.destroy = function (options) {
			faceSelector.remove();
			outMainImage.remove();
			out.remove();
			outContainer.remove();

			out = null;
			outContainer = null;
			outMainImage = null;
			faceSelector = null;

			detachListeners();
		};
		that.refresh = function (options) {
			renderData();
			render();
		};
		that.get = function (source, params) {
			var sourceMap = {
				'face': function () {
					return face;
				},
				'socks': function () {
					return socks;
				},
				'sock': function () {
					return sock;
				},
				'logos': function () {
					return logos;
				},
				'filteredData': function () {
					return filteredData;
				},
				'options': function () {
					return config;
				},
				'config': function () {
					return allConfig;
				},
				'data': function () {
					return data;
				},
				'rawData': function () {
					return rawData;
				},
				'url': function () {
					return url || (url = createUrl());
				},
				'urlParts': function () {
					return urlParts;
				},
				'log': function () {
					return log;
				},
				'inputs': function () {
					return inputs;
				},
				'inputsOverrides': function () {
					return defInputsOverrides;
				},

				'spec': function () {
					return specFormContainer;
				},
				'output': function () {
					return out;
				},
				'colorize': function () {
					return colorize;
				},
				'sockize': function () {
					return sockize;
				},
				'getFaces': function () {
					return getFaces;
				},
				'outputMainImage': function () {
					return outMainImage;
				}
			};
			return source && sourceMap[source] && sourceMap[source]();
		};

		that.set = function (property, value) {
			config[property] = value;
			if (property == 'debug') {
				debug = value;
			}
		};

		ankletTextLogoCombi = function (value, lpdata) {
			var _elm = this,
				elm = $(this),
				matchSock = 'anklet',
				matchStripe = '1',
				cat = lpdata.category,
				isAnklet = that.get('sock') == 'anklet',
				stripes = $('[name$="-number-of-stripes[]"]:checked').val() || 0,
				logoLocation = $('[name$="-logo-location"]').val() || 'logo',
				textLocation = $('[name$="-text-location"]').val() || 'text',
				messages = {
					'stripes': 'You can not add stripe with logo and text enabled on the same side. Please remove text or logo to set a stripe.',
					'text': 'You can not add a text with stripe and text enabled on the same side. Please remove stripe or logo to set a text.',
					'logo': 'You can not add logo with stripe and text enabled on the same side. Please remove stripe or text to set a logo.',
				},
				message = messages[cat];

			//console.log(isAnklet, logoLocation, textLocation, stripes);
			if (isAnklet && logoLocation == textLocation && stripes > 0) {
				alert(message);
				return true;
			}
			return false;
		};

		isScarf = function (value, lpdata) {
			var sock = sockize()
			return sock === 'scarf'
		};
		isBeanie = function (value, lpdata) {
			var sock = sockize()
			return sock === 'beanie'
		};
		isScarfBeanie = function (value, lpdata) {
			return isScarf(value, lpdata) || isBeanie(value, lpdata)
		};
		isNotScarfBeanie = function (value, lpdata) {
		return !isScarfBeanie(value, lpdata)
		};
		isNil = function (value, lpdata) {
			return value === '' || value === null || value === UNDEFINED
		};
		isNotNil = function (value, lpdata) {
			return !isNil(value, lpdata)
		};

		attachListeners = function (options) {
			var selector, selectorNot, selectorFilter, elements, event, isSpec, type, multi,
				skipActions, allowActions, allow, skip;
			$.each(inputs, function (category, defs) {

				$.each(defs, function (key, def) {
					selector = def.selector;
					selectorNot = def.selectorNot;
					selectorFilter = def.selectorFilter;
					// is part of Spec Form? indicating whether to find
					// elements using via jQuery specFormContainer.find + not and filter.
					isSpec = def.isSpec;
					multi = def.multi;
					event = def.event;
					skipActions = def.skipActions || [];
					allowActions = def.allowActions || [];

					allow = true;

					if (allowActions.length) {
						allow = false;
						$.each(allowActions, function (index, action) {
							if (action && typeof action === 'function') {
								allow = action()
								if (allow) {
									return false;
								}
							}
						});
					}
					// if (allow && skipActions.length) {
					// 	$.each(skipActions, function (index, action) {
					// 		if (action && typeof action === 'function') {
					// 			skip = action()
					// 			if (skip) {
					// 				allow = false;
					// 				return false;
					// 			}
					// 		}
					// 	});
					// }
					// console.log({attachListeners_allow: allow, def, key, skipActions, allowActions})
					if (allow) {
						type = def.type;
						elements = options[key]
						if (!elements) {
							elements = isSpec ? specFormContainer.find(selector) : $(selector)
							if (selectorNot) {
								elements = elements.not(selectorNot)
							}
							if (selectorFilter) {
								elements = elements.filter(selectorFilter)
							}
						}

						def.element = elements
						if (elements && elements.length) {
							var eventActions = def.eventActions || [];
							elements.each(function () {
								var elm = this,
								$elm = $(elm);
								var lpData = {
									'category': category,
									'key': key,
									'config': def
								};
								$elm.data('liquidpix', lpData);
								//setPreviousValue($elm, def.type);
								$elm.on(event + '.' + ns, function (e) {

									$(eventActions).each(function (index, item) {
										if (typeof item === 'function') {
											item.call(elm, e, lpData)
										}
									})

									var updated = updateData.call(this, e, true);
									if (updated) {
										render();
									}
								});

							});
						}
					}
				});
			});
		};
		detachListeners = function () {

			var elements, event, multi;

			$(window).off('resize' + '.' + ns);
			$(window).off('scroll' + '.' + ns);
			pinButton.off('click' + '.' + ns);

			$.each(inputs, function (key, defs) {
				$.each(defs, function (index, def) {
					multi = def.multi;
					elements = def.element;
					event = def.event;
					if (!elements || !elements.length) {
						return;
					}
					elements.each(function () {
						var elm = $(this);
						elm.off(event + '.' + ns);
					});
				});
			});
		};
		renderData = function () {
			// console.log('render data');
			$.each(inputs, function (category, defs) {
				$.each(defs, function (key, def) {
					var elements = def.element,
						element,
						initVal = def.initVal,
						isCheckbox = def.type === 'checkbox',
						isGroup = isCheckbox && !def.multi;

					if (!elements || !elements.length) {
						element = null;
						updateDefaultData(category, key, def, defs);
						return;
					}

					if (isGroup) {
						element = elements.filter(':checked');
						if (!element.length) {
							element = elements.filter(':first');
						}
						// console.log(['group checked', element, elements]);
						updateData.call(element);
					} else {
						elements.each(function () {
							var elm = this,
								$elm = $(elm);
							updateData.call(elm);
						});
					}

				});
			});
		};

		getPreviousValue = function (elm, type) {
			var isCheckbox = type === 'checkbox',
				parentContainer = elm.closest('.product-addon');
			if (isCheckbox) {
				return parentContainer.data('previousValue');
			}
			return elm.data('previousValue');
		};

		setPreviousValue = function (elm, type, value) {
			var isCheckbox = type === 'checkbox',
				parentContainer = elm.closest('.product-addon');

			if (isCheckbox) {
				parentContainer.data('previousValue', value);

			} else {
				elm.data('previousValue', value);
			}
			return value;
		};

		updateDefaultData = function (cat, key, def, defs) { // when input element is absent
			var rawDataCat = rawData[cat] || (rawData[cat] = {}),
				dataCat = data[cat] || (data[cat] = {}),
				val = def.defaultValue;

			if (val !== undefined) {
				dataCat[key] = val;
				rawDataCat[key] = val;
			}

		};

		updateData = function (e, isInteractive) {
			var _elm = this,
				elm = $(_elm),
				lpData = elm.data('liquidpix'),
				cat = lpData.category,
				key = lpData.key,
				config = lpData.config,
				elms = config.element,
				type = config.type,
				triggerOnUpdate = config.triggerOnUpdate || false,
				triggerItem,
				triggerElement,
				multi = config.multi,
				// [when multi=true] max - maximum allowed with LIFO
				max = config.max || null,
				min = config.min || null,
				// limit - does not allow above max - no LIFO
				limit = config.limit,
				deselect = config.deselect,
				actions = config.actions || [],
				defaultValue = config.defaultValue,
				postUpdateActions = config.postUpdateActions || [],
				skipActions = config.skipActions || [],
				allowActions = config.allowActions || [],
				keepIt = true,
				skipIt,
				index,
				rawDataCat = rawData[cat] || (rawData[cat] = {}),
				dataCat = data[cat] || (data[cat] = {}),
				isCheckbox = type === 'checkbox',
				isRadio = type === 'radio' || elm.attr('type') === 'radio',
				isRadioCheckbox = isCheckbox || isRadio,
				isChecked = isRadioCheckbox && elm.prop('checked'),
				checked = isCheckbox && elm.prop('checked'),
				prevVal = getPreviousValue(elm, type),
				val = isRadio ? elms.filter(':checked').val() : elm.val(),
				rawVal = val,
				log;
			// console.log({updateData: key, isInteractive, elm, elms, e, val, rawVal, isChecked})
			if (isNil(defaultValue)) {
				defaultValue = ''
			}

			if (allowActions.length) {
				keepIt = false;
				$.each(allowActions, function (index, action) {
					var validated;
					if (action && typeof action === 'function') {
						validated = action.call(_elm, val, lpData);
						if (validated) {
							keepIt = true;
							return false;
						}
					}
				});
				// console.log({updateData_allow: keepIt, key, allowActions})
				if (!keepIt) {
					elm.val(prevVal);
					return false;
				}
			}

			if (skipActions.length) {
				skipIt = false;
				$.each(skipActions, function (index, action) {
					var val;
					if (action && typeof action === 'function') {
						val = action.call(_elm, val, lpData);
						if (val) {
							skipIt = val;
						}
					}
				});
				// console.log({updateData_skip: skipIt, key, skipActions})
				if (skipIt) {
					elm.val(prevVal);
					return false;
				}
			}

			if (prevVal !== null && prevVal !== '') {
				if (!val && config.noUnset) {
					elm.val(prevVal);
					return false;
				}
			}

			if (isCheckbox && checked && limit > 0 && dataCat[key] && dataCat[key].length >= limit) {
				// console.log('limit reached' +  limit);
				if (isCheckbox && checked) {
					elm.prop('checked', false)
					// console.log(['unchecked', elm]);
				}
				return false;
			}

			// console.log({deselect, isRadio,prevVal,rawVal})
			if (deselect && isRadio && prevVal && rawVal && prevVal === rawVal) {
				elm.prop('checked', false)
				// console.log(['radio deselect', elm]);
				rawVal = defaultValue;
				val = defaultValue;
			}

			setPreviousValue(elm, type, rawVal);

			if (val === UNDEFINED) {
				val = defaultValue;
			}

			if (actions.length) {
				$.each(actions, function (index, action) {
					if (action && typeof action === 'function') {
						val = action.call(_elm, val, key, cat, lpData);
					}
				});
			}

			// console.log(['VALS', cat, key, config, elms]);
			// console.log(['set default value: ', val, defaultValue]);
			if (isNil(val) && !isNil(defaultValue)) {
				rawVal = defaultValue;
				val = defaultValue;

				if (debug) {
					console.log(['set default value: ', val]);
				}
			}

			if (debug) {
				log = {
					'type': 'INPUT UPDATED',
					rawVal: rawVal,
					val: val,
					elm: elm,
					data: lpData
				};
				console.log(log);
			}

			if (!multi || isRadio) {
				if (isCheckbox && !isRadio) {
					elms.not(elm).prop('checked', false);
				}
				// console.log({isCheckbox, checked, val, rawVal});
				if (isCheckbox && !checked) {
					dataCat[key] = defaultValue;
					rawDataCat[key] = defaultValue;
				} else {
					dataCat[key] = val;
					rawDataCat[key] = rawVal;
				}
			}
			else {
				if (dataCat[key] === UNDEFINED) {
					dataCat[key] = [];
					rawDataCat[key] = [];
				}
				if (isCheckbox && !checked) {
					if (isInteractive && min && dataCat[key].length <= min) {
						// console.log('min reached ', val, rawVal, min, dataCat[key], key)
						elm.prop('checked', true);
					} else {
						dataCat[key] = deleteFromArray(dataCat[key], val)
						rawDataCat[key] = deleteFromArray(rawDataCat[key], rawVal)
						// console.log('delete from array ', val, rawVal, min, dataCat[key], key)
					}

				} else {
					if (max && dataCat[key].length >= max) {
						var toRemove = (dataCat[key].length - max) + 1
						if (toRemove > 0) {
							dataCat[key].splice(0, toRemove)
							var removed = rawDataCat[key].splice(0, toRemove)
							for(var ri =0; ri < removed.length; ri++) {
								elms.filter('[value="' + removed[ri] + '"]').prop('checked', false);
							}
						}
					}
					dataCat[key].push(val);
					rawDataCat[key].push(rawVal);
				}
			}

			if (isInteractive && triggerOnUpdate) {
				for (index in triggerOnUpdate) {
					triggerItem = inputs[cat][triggerOnUpdate[index]];
					triggerElement = triggerItem.element;
					if (triggerElement) {
						triggerElement.trigger(triggerItem.event);
					}
				}
			}

			return true;
		};

		render = function () {
			url = createUrl();
			renderOutput();
		};
		createUrl = function () {
			var partsMap = defUrlMap(),
				partsOrder = ['set', 'call'],
				partVars = {},
				finalPartVars = {},
				urlVars = [],
				joined,
				actions,
				final,
				log;

			filteredData = {};
			urlParts = {};
			// console.log(data);
			$.each(data, function (category, defs) {
				var allSetInCat = true,
					skipSet = {
						'general': true
					},
					key,
					value,
					cat = $.extend({}, defs),
					inputCategory = inputs[category],
					lastAlternateMaster;

				// find alternate master having a set value or the first one as default. for example, logo-id and logo-url
				for (key in defs) {
					value = defs[key];

					if (inputCategory[key] && inputCategory[key].master) {
						if (!lastAlternateMaster) {
							lastAlternateMaster = key;
						}
						if (value) {
							lastAlternateMaster = key;
						}
					}
				}


				// keep only one alternate master
				if (lastAlternateMaster) {
					for (key in defs) {
						if (inputCategory[key] && inputCategory[key].master && lastAlternateMaster != key) {
							delete cat[key];
						}
					}
				}


				for (key in cat) {
					value = cat[key];
					if (inputCategory[key]) {
						actions = inputCategory[key]['urlActions'] || [];
						if (actions.length) {
							$.each(actions, function (index, action) {
								if (action && typeof action === 'function') {
									value = action(value, key, category, cat, inputCategory[key], inputCategory, defs);
								}
							});
							cat[key] = value;
						}
					}
				}


				// check if all values are set in a category, skipping skipSet categories
				if (!skipSet[category]) {
					for (key in cat) {
						if (inputCategory[key] && !inputCategory[key].optional) {
							value = cat[key];

							if (value === null || value === undefined || value === '') {
								allSetInCat = false;
								break;
							}
						}
					}
				}

				// filter categories having all inputs set with values
				if (allSetInCat) {
					filteredData[category] = cat;
				}

			});

			$.each(filteredData, function (category, defs) {
				var inputCategory = inputs[category];

				$.each(defs, function (key, values) {
					if (values === UNDEFINED) {
						return;
					}

					var partVar,
						varComposeString,
						varComposed,
						partKey = category + '-' + key,
						partValue = typeof values === 'object' ? values.join(',') : values;
					urlParts[partKey] = partValue;
					if (!skipUrlParts || !skipUrlParts.test(partKey)) {
						if (partsMap[partKey]) {
							partVar = partsMap[partKey]['var'];
							varComposeString = partsMap[partKey]['compose'];

							if (typeof varComposeString === 'function') {
								varComposeString(values, partVars, partVar)
							} else {
								if (!partVars[partVar]) {
									partVars[partVar] = [];
								}
								varComposed = varComposeString.replace('{value}', partValue);
								partVars[partVar].push(varComposed);
							}

						}
					}

				});
			});

			if (lpdebug) {
				partVars['set'].push('debug[1]');
			}

			$.each(forceUrlParts, function (qvar, value) {
				partVars[qvar] = value;
			});

			$.each(partsOrder, function (index, qvar) {
				if (partVars[qvar]) {
					finalPartVars[qvar] = partVars[qvar];
					delete partVars[qvar];
				}
			});
			$.each(partVars, function (qvar, values) {
				finalPartVars[qvar] = values;
			});

			$.each(finalPartVars, function (qvar, values) {
				var index;

				if (qvar == 'call' && version) {
					for (index in values) {
						if (values[index]) {
							if (!/\/v[\d\.]+?\//.test(values[index])) {
								values[index] = values[index].replace('file:socks/', 'file:socks/v' + version + '/');
							}

							if (/dress_/.test(sock)) {
								values[index] = values[index].replace('/gen.chain', '/dress-gen.chain');
							}
							if (/beanie|scarf/.test(sock)) {
								values[index] = values[index].replace('/gen.chain', '/others-gen.chain');
							}
						}
					}
					finalPartVars[qvar] = values;
				}
			});

			$.each(finalPartVars, function (qvar, values) {
				var qs = qvar + '=' + encodeURIComponent((values.join(',')));
				urlVars.push(qs);
			});

			urlVars.push('sink');
			if (debug) {
				urlVars.push('tstamp=' + (new Date().valueOf()));
			}

			joined = urlVars.join('&');
			final = lfOSUrl + joined;

			if (debug) {
				log = {
					'filtered': filteredData,
					'data': data,
					'type': 'URL',
					'vars': urlVars,
					urlParts: urlParts,
					parts: partVars,
					src: final
				};
				console.log(log);
			}

			return final;
		};

		renderOutput = function () {

			var image,
				src,
				newUrl = url || (url = createUrl()),
				faceType,
				faceSelected,
				faceElement,
				specHeading = specFormContainer.find('*:first:visible'),
				boxFixedTop,
				sockType = sockize(),
				faceTypes = getFaces(sockType),
				facesCount = Object.keys(faceTypes).length,
				hasSingleFace = facesCount <= 1,
				log;

			if (!outContainer || !outContainer.length) {
				outContainer = specFormContainer.find('#specPreviewContainer');
				if (!outContainer.length) {
					outContainer = $('<div id="specPreviewContainer" class=""></div>').insertBefore(specHeading);
					pinButton = $('<span class="dashicons dashicons-admin-post"></span>').appendTo(outContainer)
						.addClass('lqpix-pintop')
						.on('click' + '.' + ns, function () {
							outContainer.data('forcePinned', true);
							pinBoxIt(pinButton, outContainer);
						});

					// shareButtonsContainer,
					// shareButtons,

					shareContainer = $('<div class="socialShareContainer"></div>').appendTo(outContainer)
						.on('click' + '.' + ns, function () {
							$(this).find('.shareExpandButton').hide();
							$(this).find('.a2a_kit').addClass('shareIcons visible');
						});

					shareExpandButton = $('<span class="shareExpandButton dashicons dashicons-share"></span>').appendTo(shareContainer);

					$('.socialShareContainer').append('<div style="display: none;" class="a2a_kit a2a_kit_size_20 a2a_floating_style a2a_vertical_style shareIcons visible" style="margin-left: 0px; top: 0px; background-color: transparent; line-height: 20px;"><a id="facebookLink" class="a2a_button_facebook" target="_blank" href="https://www.facebook.com/sharer/sharer.php?" rel="nofollow noopener noreferrer"><span class="a2a_svg a2a_s__default a2a_s_facebook" style="background-color: rgb(59, 89, 152); width: 20px; line-height: 20px; height: 20px; background-size: 20px; border-radius: 3px;"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M17.78 27.5V17.008h3.522l.527-4.09h-4.05v-2.61c0-1.182.33-1.99 2.023-1.99h2.166V4.66c-.375-.05-1.66-.16-3.155-.16-3.123 0-5.26 1.905-5.26 5.405v3.016h-3.53v4.09h3.53V27.5h4.223z"></path></svg></span><span class="a2a_label"></span></a><!-- <a id="twitterLink" class="a2a_button_twitter" target="_blank" href="https://twitter.com/home?status=" rel="nofollow noopener noreferrer"><span class="a2a_svg a2a_s__default a2a_s_twitter" style="background-color: rgb(85, 172, 238); width: 20px; line-height: 20px; height: 20px; background-size: 20px; border-radius: 3px;"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M28 8.557a9.913 9.913 0 0 1-2.828.775 4.93 4.93 0 0 0 2.166-2.725 9.738 9.738 0 0 1-3.13 1.194 4.92 4.92 0 0 0-3.593-1.55 4.924 4.924 0 0 0-4.794 6.049c-4.09-.21-7.72-2.17-10.15-5.15a4.942 4.942 0 0 0-.665 2.477c0 1.71.87 3.214 2.19 4.1a4.968 4.968 0 0 1-2.23-.616v.06c0 2.39 1.7 4.38 3.952 4.83-.414.115-.85.174-1.297.174-.318 0-.626-.03-.928-.086a4.935 4.935 0 0 0 4.6 3.42 9.893 9.893 0 0 1-6.114 2.107c-.398 0-.79-.023-1.175-.068a13.953 13.953 0 0 0 7.55 2.213c9.056 0 14.01-7.507 14.01-14.013 0-.213-.005-.426-.015-.637.96-.695 1.795-1.56 2.455-2.55z"></path></svg></span><span class="a2a_label"></span></a> --><a id="pinterestLink" class="a2a_button_pinterest" target="_blank" href="https://pinterest.com/pin/create/button/?url=&media=&description=" rel="nofollow noopener noreferrer"><span class="a2a_svg a2a_s__default a2a_s_pinterest" style="background-color: rgb(189, 8, 28); width: 20px; line-height: 20px; height: 20px; background-size: 20px; border-radius: 3px;"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M16.539 4.5c-6.277 0-9.442 4.5-9.442 8.253 0 2.272.86 4.293 2.705 5.046.303.125.574.005.662-.33.061-.231.205-.816.27-1.06.088-.331.053-.447-.191-.736-.532-.627-.873-1.439-.873-2.591 0-3.338 2.498-6.327 6.505-6.327 3.548 0 5.497 2.168 5.497 5.062 0 3.81-1.686 7.025-4.188 7.025-1.382 0-2.416-1.142-2.085-2.545.397-1.674 1.166-3.48 1.166-4.689 0-1.081-.581-1.983-1.782-1.983-1.413 0-2.548 1.462-2.548 3.419 0 1.247.421 2.091.421 2.091l-1.699 7.199c-.505 2.137-.076 4.755-.039 5.019.021.158.223.196.314.077.13-.17 1.813-2.247 2.384-4.324.162-.587.929-3.631.929-3.631.46.876 1.801 1.646 3.227 1.646 4.247 0 7.128-3.871 7.128-9.053.003-3.918-3.317-7.568-8.361-7.568z"></path></svg></span><span class="a2a_label"></span></a><a id="emailLink" class="a2a_button_email" href="mailto:?&body=" rel="nofollow noopener noreferrer"><span class="a2a_svg a2a_s__default a2a_s_email" style="background-color: rgb(1, 102, 255); width: 20px; line-height: 20px; height: 20px; background-size: 20px; border-radius: 3px;"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M26 21.25v-9s-9.1 6.35-9.984 6.68C15.144 18.616 6 12.25 6 12.25v9c0 1.25.266 1.5 1.5 1.5h17c1.266 0 1.5-.22 1.5-1.5zm-.015-10.765c0-.91-.265-1.235-1.485-1.235h-17c-1.255 0-1.5.39-1.5 1.3l.015.14s9.035 6.22 10 6.56c1.02-.395 9.985-6.7 9.985-6.7l-.015-.065z"></path></svg></span><span class="a2a_label"></span></a></div>');

					$('.shareExpandButton').click(function(){
							$(this).hide();
							$('.socialShareContainer .a2a_kit').show();
							$('.socialShareContainer .a2a_kit span.a2a_svg').css('display', 'block');
					});

					$('#facebookLink').click(function(){
							var link = $('#specPreview img').attr('src');
							window.open('https://www.facebook.com/sharer/sharer.php?u='+link.replace('&sink',"&blank=height[base1.height],name[aNewLayer],width[510],height[350]&composite=gravity[center],image[base1]&sink").replace(/%/g,"%25").replace(/&/g,"%26"), 'facebookwindow','left=20,top=20,width=500,height=700,toolbar=0,resizable=1');
							return false;
					});

					$('#pinterestLink').click(function(){
							var link = $('#specPreview img').attr('src');
							window.open('https://pinterest.com/pin/create/button/?url='+link.replace(/%/g,"%25").replace(/&/g,"%26")+'&media='+link.replace(/%/g,"%25").replace(/&/g,"%26")+'&description=', 'pinterestwindow','left=20,top=20,width=500,height=700,toolbar=0,resizable=1');
							return false;
					});

					$('#emailLink').click(function(){
							var link = $('#specPreview img').attr('src');
							$(this).attr('href', 'mailto:?&body='+link.replace(/%/g,"%25").replace(/&/g,"%26"));
					});

					boxFixedTop = outContainer.offset().top;
					$(window).on('resize' + '.' + ns, function () {
						boxFixedTop = outContainer.offset().top;
						if (!outContainer.hasClass('pinned')) {
							return;
						}
						positionPinnedBox(outContainer);
					});

					$(window).on('scroll' + '.' + ns, function () {
						if (!outContainer || !outContainer.length) {
							return;
						}
						var scrollTop = $(window).scrollTop(),
							header = $("#header"),
							headerOffset = header.offset(),
							headerHeight = header.outerHeight(),
							generalOffset = 30,
							genericTop = headerHeight + headerOffset.top + generalOffset,
							autoPinEnd = specFormContainer.find('*:last:visible').offset().top,
							forcedPinned = outContainer.data('forcePinned'),
							formElement = $("form.cart"),
							cartHeight = formElement.height(),
							cartOffset = formElement.offset(),
							endPoint = cartOffset && ((cartOffset.top || 0) + cartHeight),
							topPoint = cartOffset.top,
							pinnedOffset = outContainer.offset(),
							pinnedHeight = outContainer.height(),
							pinnedTop = pinnedOffset.top,
							pinnedEnd = pinnedTop + pinnedHeight;

						if (!forcedPinned) {

							if (genericTop > boxFixedTop && genericTop < autoPinEnd) {
								if (!outContainer.is(":visible")) {
									outContainer.show();
								}
								if (!outContainer.hasClass('pinned')) {
									pinBoxIt(pinButton, outContainer, true);
								}
							} else {
								if (genericTop >= autoPinEnd) {
									outContainer.hide();
								} else {
									if (outContainer.hasClass('pinned')) {
										pinBoxIt(pinButton, outContainer, false);
									}
								}
							}
							return;
						}

						if (!outContainer.hasClass('pinned')) {
							return;
						}
						if (pinnedTop >= topPoint && pinnedEnd <= endPoint) {
							outContainer.css({
								'opacity': 1
							});
							positionPinnedBox(outContainer);
						} else {
							if (pinnedTop < topPoint || pinnedEnd > endPoint) {
								outContainer.css({
									'opacity': 0
								});
							}
						}

					});
				}
			}

			if (!out || !out.length) {
				out = outContainer.find('#specPreview');
				if (!out.length) {
					out = $('<div id="specPreview" class=""></div>').appendTo(outContainer);
				}
			}

			if (!outMainImage || !outMainImage.length) {
				outMainImage = out.find('img.main');
				if (!outMainImage.length) {
					outMainImage = $('<img class="main"/>').appendTo(out);
				}
			}

			if (!faceSelector || !faceSelector.length) {
				faceSelector = outContainer.find('.faceSelectContainer');
				if (!faceSelector.length) {

					faceSelector = $('<div class="faceSelectContainer clearfix"></div>')
						.appendTo(outContainer);

					if (!hasSingleFace) {
						for (faceType in faceTypes) {
							faceElement = $('<div class="faceSelectorFace">' + faceType + '</div>')
								.data('face', faceType)
								.appendTo(faceSelector);
							faceElement.on('click', function () {
								var inputElement = $(inputs.general.face.selector),
									elm = $(this),
									currentValue = inputElement.val(),
									update = elm.data('face');

								if (update !== currentValue) {
									faceSelector.find('.faceSelectorFace').removeClass('active');
									inputElement.val(update).trigger('change');
									elm.addClass('active');
								}
							});

							faceSelected = face || defaultFace;
							if (faceSelected === faceType) {
								faceElement.addClass('active');
							}
						}
					}
				}
			}

			src = outMainImage.data('src') || '';
			if (src === newUrl) {
				return false;
			}

			if (!outField || !outField.length) {
				outField = specFormContainer.find('.product-addon-preview-details input');
				outField.closest('.product-addon').hide();
				outField.attr('type', 'hidden');
			}

			preloader(out);
			image = new Image();
			image.onload = imageLoaded;
			image.src = newUrl;
			if (outField && outField.length) {
				outField.val(encodeURIComponent(newUrl));
			}
			outMainImage.data('src', newUrl);

			if (debug) {
				log = {
					'type': 'RENDER',
					src: url,
					out: out,
					outMain: outMainImage
				};
				// console.log(url);console.log(viewerImage, src);
				console.log(decodeURIComponent(url));
			}

		};

		imageLoaded = function () {
			var elm = this,
				src = elm.src,
				latestSrc = outMainImage.data('src');

			if (src == latestSrc) {
				outMainImage.attr('src', src);
				preloader(out, true);
			}

		};

		pinBoxIt = function (pin, container, pinIt) {

			if (pinIt === UNDEFINED) {
				pinIt = !container.hasClass('pinned');
			}

			if (pinIt === true) {
				pin.addClass('pinned');
				container.addClass('pinned');
			} else {
				pin.removeClass('pinned');
				container.removeClass('pinned');
			}

			positionPinnedBox(container);

		};

		positionPinnedBox = function (box) {
			var generalOffset = 30,
				boxOffset = box.position(),
				boxTop = boxOffset.top,
				header = $("#header"),
				headerOffset = header.position(),
				headerHeight = header.outerHeight(),
				genericTop = headerHeight + headerOffset.top + generalOffset;

			//console.log([boxOffset, headerOffset, boxTop, genericTop]);
			if (box.hasClass('pinned')) {
				if (boxTop < genericTop) {
					box.css({
						'top': genericTop + 'px'
					});
				}
			} else {
				box.css({
					'top': 0
				});
			}
		};

		preloader = function (elm, hide) {
			if (hide) {
				elm.removeClass('preload');
			} else {
				elm.addClass('preload');
			}
		};

		trim = function (val) {
			if (val) {
				val = ('' + val).replace(/^\s+?|\s+?$/g, '');
			}
			return val;
		};

		trimDashEnd = function (val) {
			if (val) {
				val = ('' + val).replace(/\-.+?$/g, '');
			}
			return val;
		};

		deleteFromArray = function (arr, value) {
			var i = 0, l = arr.length, newArray = [];
			for(i = 0; i < l; i++) {
				if (arr[i] !== value) {
					newArray.push(arr[i])
				}
			}
			return newArray;
		};

		textSizer = function (val) {
			var map = {
				'small': 'a',
				'medium': 'b',
				'large': 'c',
				'extra-large': 'd'
			};
			return map[val] || 'a';
		};

		defSocks = function () {
			return {
				'kneehigh': 'kneehigh',
				'knee-high': 'kneehigh',
				'anklet': 'anklet',
				'footie': 'footie',
				'crew': 'crew',
				'leg-warmer': 'legwarmers',
				'legwarmer': 'legwarmers',
				'legwarmers': 'legwarmers',
				'dress': {
					//'polka': 'dress_polka',
					//'polka': 'dress_polka_v2',
					'polka': 'dress_polka',
					'repeated': 'dress_repeated_logo',
					'argyle': 'dress_argyle',
					'stripes': 'dress_stripes',
					'custom': 'crew',
				},
				scarf: 'scarf',
				beanie: 'beanie',
				// 'elite': 'elite',
				// 'elite-series': 'elite',

			};
		};

		that.getSockCategory = sockize = function (socksToProbe) {
			sock = socks['knee-high'];
			var socksArray = (socksToProbe || $('[name=liquidpixels_product_categories]').val() || '').split(','),
				sockKey, sockValue, found, i, j, sockInnerKey, sockInnerValue;
			for (i in socksArray) {
				sockKey = socksArray[i];
				sockValue = socks[socksArray[i]] || null;
				if (sockValue) {
					if (typeof sockValue === 'string') {
						sock = sockValue;
					} else {
						for(j in socksArray) {
							sockInnerKey = socksArray[j];
							sockInnerValue = sockValue[sockInnerKey] || null;
							if (sockInnerKey !== sockKey && sockInnerValue) {
								sock = sockInnerValue;
								break;
							}
						}
					}
					break;
				}
			}
			return sock;
		};

		initNudges = function (container, options, inputs) {
			nudgeConfig.offset = options.nudgeOffset || nudgeConfig.offset || 10;
		};

		resetNudge = function (event, data) {
			var category = data.category
			var key = data.key
			if (key === 'location' && nudgeConfig.resetOnLocationChange ||
					key === 'alignment' && nudgeConfig.resetOnAlignmentChange) {
				// console.log(['reset ' + category + ' nudge', event, data])
				nudgeConfig[category].vertical = 0
				nudgeConfig[category].horizontal = 0
			}
		};

		nudgeItem = function(e, data) {
			// console.log(e, data)
			e.preventDefault();
			var val = (nudgeConfig.offset) * (1 * $(this).val());
			var category = data.category
			var directions = {'nudgeH': 'horizontal', 'nudgeV': 'vertical'};
			var direction = directions[data.key] || ''
			var prevVal = nudgeConfig[category][direction] || 0;
			val = prevVal + val;
			nudgeConfig[category][direction] = val;
			// console.log(['general nudge for ', category, direction, val, this])

		};

		getNudgedValue = function (value, key, category) {
			var directions = {'nudgeH': 'horizontal', 'nudgeV': 'vertical'};
			var direction = directions[key] || ''
			return nudgeConfig[category][direction] || 0
		};

		logoize = function (logoId) {
			logoId = (logoId || '').replace('#', '');
			var logoPath = '',
				logo = logos[logoId];
			if (logo) {
				logoPath = 'logos/' + logo.path + '.png';
			}
			return logoPath;
		};

		cleanLogoId = function (val) {
			if (/^#/.test(val)) {
				return null;
			}
			return val;
		};

		sanitizeText = function (value) {

			var needle, replaceWith, replaceList = {
				'U': '\U',
				',': 'U002C',
				'.': 'U002E',
				'"': 'U0022',
				"'": 'U0027',
				'“': 'U201C',
				'”': 'U201D',
				'‘': 'U2018',
				'’': 'U2019',
				'*': 'U002A',
				'+': 'U002B',
				'÷': 'U00F7',

				'(': 'U0026lp;',
				')': 'U0026rp;',
				'&': 'U0026amp;',
				'<': 'U0026lt;',
				'>': 'U0026gt;',

				'#': 'U0023',
				'[': 'U005B',
				']': 'U005D',
				'©': 'U00A9',
				'®': 'U00AE',

			};

			for (needle in replaceList) {
				replaceWith = replaceList[needle];
				value = replaceAll(value, needle, replaceWith);
			}

			return value;
		};

		alignText = function (value) {

			var elm = $(this),
				lpData = elm.data('liquidpix'),
				cat = lpData.category,
				rawDataCat = rawData[cat] || (rawData[cat] = {}),
				dataCat = data[cat] || (data[cat] = {}),
				textItem = inputs[cat].master,
				textElement = textItem.element,
				text = dataCat.master = rawDataCat.master || textElement.val(),
				newText;

			if (/vertical/.test(value)) {
				newText = text.replace(/([\s\S])/g, '$1\\n');
				dataCat.master = newText;
			}

			return value;
		};

		sockFaceChainer = function (value) {
			return 'socks/' + value + '/' + face + '.chain';
		};

		colorize = function (val) {
			var colors = {
				"black": "#000000",
				"brown": "#563702",
				"bubble-gum": "#feccff",
				"dark-green": "#2a6503",
				"cardinal": "#A0312E",
				"forest": "#2a6503",
				"gold": "#f3d71a",
				"grey": "#949494",
				"hot-pink": "#fd57c4",
				"kelly-green": "#24c60e",
				"maroon": "#6d060f",
				"navy-blue": "#072369",
				"neon-green": "#7eff00",
				"neon-orange": "#F47920",
				"neon-pink": "#ff3cea",
				"neon-yellow": "#D7DF23",
				"orange": "#ff5918",
				"pink": "#feccff",
				"purple": "#660b90",
				"red": "#ff0000",
				"royal-blue": "#105fe1",
				"scarlet": "#ff0000",
				"silver": "#949494",
				"sky-blue": "#58b9f1",
				"tan": "#D0BB94",
				"teal": "#008080",
				"texas-orange": "#9C4529",
				"vegas-gold": "#c2b757",
				"white": "#FFFFFF",
				"yellow": "#f6ff00"
			};
			if (val && colors[val]) {
				val = colors[val];
			}
			// else {
			//     val = colors['black'];
			// }
			return val;
		};

		multiColorize = function (val) {

			var colors = {
				"black": "#000000",
				"brown": "#592B13",
				"dark-green": "#005C48",
				"gold": "#FFBB00",
				"grey": "#A3ACB0",
				"hot-pink": "#FA88C1",
				"kelly-green": "#008355",
				"maroon": "#812B4A",
				"navy-blue": "#002458",
				"neon-green": "#67E24E",
				"neon-pink": "#F149B8",
				"neon-yellow": "#FCEA00",
				"orange": "#FF7E00",
				"pink": "#F6A5C5",
				"purple": "#542488",
				"red": "#C8053F",
				"royal-blue": "#023DA2",
				"sky-blue": "#4B94E1",
				"tan": "#D0BB94",
				"teal": "#018C9E",
				"vegas-gold": "#EFDCB7",
				"white": "#FFFFFF",
				"yellow": "#FED400"
			};
			if (val && colors[val]) {
				val = colors[val];
			}
			// else {
			//     val = colors['black'];
			// }
			return val;
		};


		//
		defUrlMap = function () {
			return {
				//'general-sock': {'var': 'call', 'compose': 'url[file:{value}]'},
				'general-revision': {
					'var': 'revision',
					'compose': '{value}'
				},
				'general-sock': {
					'var': 'set',
					'compose': 'sock[{value}]'
				},
				'general-color': {
					'var': 'set',
					'compose': 'sockcolor[{value}]'
				},
				'general-face': {
					'var': 'set',
					'compose': 'face[{value}]'
				},

				'toeheel-colors': {
					'var': 'set',
					'compose': 'toeheelcolor[{value}]'
				},

				'stripes-master': {
					'var': 'set',
					'compose': 'stripes[{value}]'
				},
				'stripes-colors': {
					'var': 'set',
					'compose': 'stripecolor[{value}]'
				},

				'text-master': {
					'var': 'set',
					'compose': function (value, partVars, partVar) {
						if (!partVars[partVar]) {
							partVars[partVar] = [];
						}
						partVars[partVar].push('text[' + value + ']')
						if (isScarfBeanie()) {
							var sizesDefs = {
								20: 11, //16-20 11 (21-x=9)
								15: 15,//11-15  15
								10: 20,//6-10 20
								5: 25, //1-5 25
								0: 30,
							}
							var lengths = [20, 15, 10,  5,  0]
							var sizes = 	[11, 15, 20, 25, 30]

							var size = 0
							var length = (value || '').length
							if (length) {
								var maxLength = 999999999
								for(var i=0; i < lengths.length; i++) {
									var l = 1 * lengths[i]
									// console.log({l, length, maxLength, exp: length > l && length <= maxLength, lengths})
									if (length > l && length <= maxLength) {
										size = sizes[i];
										break;
									}
									maxLength = l;
								}
							}
							// console.log({TEXT_FONT_SIZE: size, length})
							partVars[partVar].push('fontsize[' + size + ']')
						}
					}
					//'compose': 'text[{value}]'
				},
				'text-location': {
					'var': 'set',
					'compose': 'textpos[{value}]'
				},
				'text-alignment': {
					'var': 'set',
					'compose': 'textalign[{value}]'
				},
				'text-size': {
					'var': 'set',
					'compose': 'textsize[{value}]'
				},
				'text-colors': {
					'var': 'set',
					'compose': 'textcolor[{value}]'
				},
				'text-nudgeV': {
					'var': 'set',
					'compose': 'textvnudge[{value}]'
				},
				'text-nudgeH': {
					'var': 'set',
					'compose': 'texthnudge[{value}]'
				},

				'logo-image': {
					'var': 'set',
					'compose': 'logourl[{value}]'
				},
				'logo-id': {
					'var': 'set',
					'compose': 'logofile[{value}]'
				},
				'logo-color': {
					'var': 'set',
					'compose': 'logocolor[{value}]'
				},
				'logo-location': {
					'var': 'set',
					'compose': 'logopos[{value}]'
				},
				'logo-nudgeV': {
					'var': 'set',
					'compose': 'logovnudge[{value}]'
				},
				'logo-nudgeH': {
					'var': 'set',
					'compose': 'logohnudge[{value}]'
				},

				'cuff-color': {
					'var': 'set',
					'compose': 'cuffcolor[{value}]'
				},

				'polka-colors': {
					'var': 'set',
					'compose': function (values, partVars, partVar) {
						// console.log(values);
						var sanitizedValues = [], value, newValues = [], i;
						for (i = 0; i < values.length; i++) {
								value = values[i];
								if (value) {
									sanitizedValues.push(value)
								}
						}
						if (sanitizedValues.length) {
							value = sanitizedValues[0];
							newValues.push(value);
							newValues.push(sanitizedValues[1] ||value);
							newValues.push(sanitizedValues[2] || value);

							if (!partVars[partVar]) {
								partVars[partVar] = [];
							}
							for (i = 0; i < 3; i++) {
								partVars[partVar].push('polka' + (i + 1) + '[' + newValues[i] + ']');
							}
						}
					}
				},
				'polka-color-one': {
					'var': 'set',
					'compose': 'polka1[{value}]'
				},
				'polka-color-two': {
					'var': 'set',
					'compose': 'polka2[{value}]'
				},
				'polka-color-three': {
					'var': 'set',
					'compose': 'polka3[{value}]'
				},

				'dress-stripes-color-one': {
					'var': 'set',
					'compose': 'dressstripe1[{value}]'
				},
				'dress-stripes-color-two': {
					'var': 'set',
					'compose': 'dressstripe2[{value}]'
				},
				'dress-stripes-color-three': {
					'var': 'set',
					'compose': 'dressstripe3[{value}]'
				},

				'argyle-diamond-one-color': {
					'var': 'set',
					'compose': 'argylediamond1[{value}]'
				},
				'argyle-diamond-two-color': {
					'var': 'set',
					'compose': 'argylediamond2[{value}]'
				},
				'argyle-line-color': {
					'var': 'set',
					'compose': 'argyleline[{value}]'
				},

				'scarf-stripe-style': {
					'var': 'set',
					'compose': 'scarfstripestyle[{value}]'
				},
				'scarf-tassel-colors': {
					'var': 'set',
					'compose': function (values, partVars, partVar) {
						var sanitizedValues = [], value, newValues = [], i, j;
						for (i = 0; i < values.length; i++) {
								value = values[i];
								if (value) {
									sanitizedValues.push(value)
								}
						}
						var length = sanitizedValues.length
						if (length) {
							if (!partVars[partVar]) {
								partVars[partVar] = [];
							}
							var repeats =  6 / length
							var index = 0
							for(i = 0; i < repeats; i++) {
								for(j = 0; j < length; j++) {
									newValues[index] = sanitizedValues[j]
									index++
								}
							}

							if (newValues.length) {
								for (i = 0; i < newValues.length; i++) {
									partVars[partVar].push('tasselcolor' + (i + 1) + '[' + newValues[i] + ']');
								}
							}
						}
					}
				},

				'beanie-pom-pom-colors': {
					'var': 'set',
					'compose': function (values, partVars, partVar) {
						var sanitizedValues = [], value, newValues = [], i;
						for (i = 0; i < values.length; i++) {
								value = values[i];
								if (value) {
									sanitizedValues.push(value)
								}
						}
						if (!partVars[partVar]) {
								partVars[partVar] = [];
						}
						if (sanitizedValues.length) {
							for (i = 0; i < sanitizedValues.length; i++) {
								partVars[partVar].push('pompomcolor' + (i + 1) + '[' + sanitizedValues[i] + ']');
							}
						}
					}
				},
				'beanie-brim-base-color': {
					'var': 'set',
					'compose': 'brimbasecolor[{value}]'
				},
				'beanie-brim-stripe-color': {
					'var': 'set',
					'compose': 'brimstripecolor[{value}]'
				},
				'beanie-primary-stripe-color': {
					'var': 'set',
					'compose': 'logostripecolor[{value}]'
				},
				'beanie-secondary-stripe-color': {
					'var': 'set',
					'compose': 'abovebelowstripecolor[{value}]'
				},


			};
		};

		defInputs = function () {
			// 'initVal': true, => if set to true it sets the first value of the default value of the group
			return {
				'general': {
					'revision': {
						'selector': '[name$="liquid_pixels_revision"]'
					},
					'face': {
						'selector': '[name=liquidpixels_product_face]',
						'element': null,
						'isSpec': false,
						'event': 'change',
						'actions': [function (value) {
							face = value || face || defaultFace;
							return face;
						}],
					},
					'sock': {
						'selector': '[name=liquidpixels_product_categories]',
						'element': null,
						'isSpec': false,
						'actions': [sockize],
						'urlActions': [],
					},
					'size': {
						'selector': '[name$="-sock-size"]',
						'element': null,
						'event': 'change',
						'isSpec': false,
						'noUnset': true
					},
					'color': {
						'selector': '[name$="sock-base-color[]"], [name$="-beanie-base-color[]"], [name$="-scarf-base-color[]"]',
						'initVal': true,
						'element': null,
						'event': 'change',
						'isSpec': false,
						'defaultValue': 'black',
						'type': 'checkbox',
						'deselect': false,
						'multi': false,
						'actions': [colorize],
						'noUnset': true
					}
				},
				'toeheel': {
					'colors': {
						'selector': '[name$="-toe-heel-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'actions': [colorize]
					}
				},
				'stripes': {
					'master': {
						'selector': '[name$="-number-of-stripes[]"]',
						'element': null,
						'event': 'change',
						'isSpec': true,
						'defaultValue': 0,
						'type': 'checkbox',
						'multi': false,
						'actions': [function (value) {
							return parseInt(value);
						}],
						'skipActions': [ankletTextLogoCombi]
					},
					'colors': {
						'selector': '[name$="-stripe-color[]"],[name$="-strip-color[]"]',
						'initVal': true,
						'element': null,
						'event': 'change',
						'isSpec': true,
						'optional': true,
						'defaultValue': 'black',
						'type': 'checkbox',
						'deselect': false,
						'multi': false,
						'allowActions': [isNotScarfBeanie, isScarf],
						'actions': [colorize],
						urlActions: [
							function (value) {
								return value || colorize('black');
							}
						]
					}
				},
				'text': {
					'master': {
						'selector': '[name$="-text-on-sock"], [name$="-add-text"], [name$="-add-text-to-brim"]',
						'element': null,
						'event': 'change',
						'isSpec': true,
						'actions': [],
						'optional': false,
						'urlActions': [sanitizeText],
						'triggerOnUpdate': ['alignment']
					},
					'location': {
						'selector': '[name$="-text-location"]',
						// ,urlActions: [function (value, key, category, cat, inputCategoryItem, inputCategory, defs) {
						//     return $(inputCategoryItem.selector).val() || face;
						// }],
						'element': null,
						'event': 'change',
						'eventActions': [resetNudge],
						'isSpec': true,
						'actions': [trimDashEnd],
						'skipActions': [ankletTextLogoCombi]
					},
					'alignment': {
						'selector': '[name$="-text-alignment"]',
						'element': null,
						'event': 'change',
						'isSpec': true,
						'eventActions': [resetNudge],
						'actions': [trimDashEnd, alignText]
					},
					'size': {
						'selector': '[name$="-text-size[]"]',
						'initVal': true,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'defaultValue': 'a',
						'actions': [textSizer]
					},
					'colors': {
						'selector': '[name$="-text-color[]"]',
						'initVal': true,
						'element': null,
						'event': 'change',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': false,
						'defaultValue': 'black',
						'actions': [colorize],
						'urlActions': [
							function (value) {
								return value || 'black';
							}
						]
					},
					'nudgeV': {
						'selector': 'p[class*="-text-nudge-"] button[data-direction=vertical]',
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'button',
						'isSelfDestruct': true,
						'optional': true,
						'eventActions': [nudgeItem],
						'urlActions': [getNudgedValue],
						'actions': [getNudgedValue]
					},
					'nudgeH': {
						'selector': 'p[class*="-text-nudge-"] button[data-direction=horizontal]',
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'button',
						'isSelfDestruct': true,
						'optional': true,
						'eventActions': [nudgeItem],
						'urlActions': [getNudgedValue],
						'actions': [getNudgedValue]
					}
				},
				'logo': {
					'image': {
						'selector': '.logoFileInput',
						'element': null,
						'event': 'change',
						'isSpec': true,
						'type': 'hidden',
						'actions': [cleanLogoId],
						'master': true
					},
					'id': {
						'selector': '[name$="-stock-logo"]',
						'element': null,
						'event': 'change',
						'isSpec': true,
						'actions': [trim, logoize],
						'master': true
					},
					'location': {
						'selector': '[name$="-logo-location"]',
						'element': null,
						'event': 'change',
						'isSpec': true,
						'eventActions': [resetNudge],
						'actions': [trimDashEnd],
						'skipActions': [ankletTextLogoCombi],
						// ,urlActions: [function (value, key, category, cat, inputCategoryItem, inputCategory, defs) {
						//     return $(inputCategoryItem.selector).val() || face;
						// }]
					},
					'color': {
						'selector': '[name$="-logo-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'deselect': true,
						'isSelfDestruct': true,
						'optional': true,
						'actions': [colorize]
					},
					'nudgeV': {
						'selector': 'p[class*="-logo-nudge-"] button[data-direction=vertical]:visible',
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'button',
						'isSelfDestruct': true,
						'optional': true,
						'eventActions': [nudgeItem],
						'urlActions': [getNudgedValue],
						'actions': [getNudgedValue]
					},
					'nudgeH': {
						'selector': 'p[class*="-logo-nudge-"] button[data-direction=horizontal]:visible',
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'button',
						'isSelfDestruct': true,
						'optional': true,
						'eventActions': [nudgeItem],
						'urlActions': [getNudgedValue],
						'actions': [getNudgedValue]
					}
				},

				'cuff': {
					'color': {
						'selector': '[name$="-cuff-color[]"]',
						'initVal': false,
						'defaultValue': '',
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'actions': [colorize]
					}
				},
				'polka': {
					'color-one': {
						'selector': '[name$="-polka-one-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					},
					'color-two': {
						'selector': '[name$="-polka-two-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					},
					'color-three': {
						'selector': '[name$="-polka-three-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					},
				},
				'dress-stripes': {
					'color-one': {
						'selector': '[name$="-stripe-one-color[]"]',
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					},
					'color-two': {
						'selector': '[name$="-stripe-two-color[]"]',
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					},
					'color-three': {
						'selector': '[name$="-stripe-three-color[]"]',
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					},
				},
				'argyle': {
					'diamond-one-color': {
						'selector': '[name$="-diamond-one-color[]"]',
						'initVal': true,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					},
					'diamond-two-color': {
						'selector': '[name$="-diamond-two-color[]"]',
						'initVal': true,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					},
					'line-color': {
						'selector': '[name$="-line-color[]"]',
						'initVal': true,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize]
					}

				},

				scarf: {
					'stripe-style': {
						'selector': '[name$="-stripe-style[]"]',
						'element': null,
						'defaultValue': 'none',
						'event': 'click',
						'isSpec': true,
						'type': 'radio',
						'allowActions': [isScarf],
					},
					'tassel-colors': {
						'selector': '[name$="-tassel-colors[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': true,
						'max': 3,
						'deselect': true,
						'optional': true,
						'actions': [colorize],
						'allowActions': [isScarf],
					},
				},

				beanie: {
					'brim-base-color': {
						'selector': '[name$="-brim-base-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize],
						'allowActions': [isBeanie],
					},
					'pom-pom-colors': {
						'selector': '[name$="-pom-pom-colors[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': true,
						'max': 3,
						'deselect': true,
						'optional': true,
						'actions': [colorize],
						'allowActions': [isBeanie]
					},
					'brim-stripe-color': {
						'selector': '[name$="-brim-stripe-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize],
						'allowActions': [isBeanie],
					},
					'primary-stripe-color': {
						'selector': '[name$="-primary-stripe-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize],
						'allowActions': [isBeanie],
					},
					'secondary-stripe-color': {
						'selector': '[name$="-secondary-stripe-color[]"]',
						'initVal': false,
						'element': null,
						'event': 'click',
						'isSpec': true,
						'type': 'checkbox',
						'multi': false,
						'deselect': true,
						'optional': true,
						'actions': [colorize],
						'allowActions': [isBeanie],
					},
				}

			};
		};
		defInputsOverrides = function (type) {
			var overrides = {
				'anklet': {
					text: {
						alignment: {
							"defaultValue": 'horizontal'
						}
					}
				},
				'footie': {
					logo: {
						location: {
							"defaultValue": 'front'
						}
					},
					text: {
						alignment: {
							"defaultValue": 'horizontal'
						}
					}
				},
				'beanie': {
					logo: {
						location: {
							"defaultValue": 'front'
						}
					},
					text: {
						alignment: {
							"defaultValue": 'horizontal'
						}
					}
				},
				'scarf': {
					stripes: {
						master: {
							"defaultValue": 1
						},
						colors: {
							"defaultValue": 'grey'
						}
					},
					logo: {
						location: {
							"defaultValue": 'front'
						}
					},
					text: {
						alignment: {
							"defaultValue": 'horizontal'
						}
					}
				},
			};
			if (!type) {
				return overrides
			}
			return overrides[type] || {};
		};

		inputs = defInputs();
		socks = defSocks();

		faces = {
			'left': {},
			'front': {},
			'right': {},
			'back': {},
			'bottom': {}
		};
		getFaces = function(type) {
			if (/beanie|scarf/.test(type)) {
				return {
					front: {}
				}
			}
			return faces
		}

		return that;
	}

	function initiator() {
		var index, fn;
		if (window.Elm5LiquidPixReady && Elm5LiquidPixReady.length) {
			for (index in Elm5LiquidPixReady) {
				fn = Elm5LiquidPixReady[index];
				if (typeof (fn) === 'function') {
					fn(Elm5LiquidPix);
				}
			}
		}
	}
	initiator()
	// $(document).ready(initiator)

})(jQuery);