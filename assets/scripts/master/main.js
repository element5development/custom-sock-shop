var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
		TOP GALLERY
	\*----------------------------------------------------------------*/
	$("body.page-template-page-home header ul.top-gallery").slick({
		infinite: !0,
		slidesToShow: 8,
		slidesToScroll: 1,
		centerMode: !0,
		centerPadding: "60px",
		arrows: !1,
		dots: !1,
		autoplay: !0,
		autoplaySpeed: 0,
		speed: 3e3,
		cssEase: "linear",
		rtl: !0,
		pauseOnFocus: !1,
		pauseOnHover: !1,
		responsive: [
			{ breakpoint: 1024, settings: { slidesToShow: 5 } },
			{ breakpoint: 600, settings: { slidesToShow: 3 } },
		],
	});
	/*----------------------------------------------------------------*\
		BOTTOM GALLERY
	\*----------------------------------------------------------------*/
	$("body.page-template-page-home header ul.bottom-gallery").slick({
		infinite: !0,
		slidesToShow: 8,
		slidesToScroll: 1,
		centerMode: !0,
		centerPadding: "60px",
		arrows: !1,
		dots: !1,
		autoplay: !0,
		autoplaySpeed: 0,
		speed: 3e3,
		cssEase: "linear",
		pauseOnFocus: !1,
		pauseOnHover: !1,
		responsive: [
			{ breakpoint: 1024, settings: { slidesToShow: 5 } },
			{ breakpoint: 600, settings: { slidesToShow: 3 } },
		],
	});

});