<?php
/**
 * The Template for displaying radio button field.
 *
 * @version 3.0.0
 */
global $post;
$post_id = $post->ID;

$loop          = 0;
$field_name    = ! empty( $addon['field_name'] ) ? $addon['field_name'] : '';
$sanitized_field_name = sanitize_title ( $field_name );
$name = ! empty( $addon['name'] ) ? $addon['name'] : '';
$sanitized_name = sanitize_title ( $name );
$field_key = $post_id . '-' . $sanitized_name;

$addon_key     = 'addon-' . $field_key;
$required      = ! empty( $addon['required'] ) ? $addon['required'] : '';
$current_value = isset( $_POST[ $addon_key ] ) && isset( $_POST[ $addon_key ][0] ) ? wc_clean( $_POST[ $addon_key ][0] ) : '';
?>

<?php if ( false && empty( $required ) ) { ?>
	<p class="form-row form-row-wide addon-wrap addon-wrap-<?php echo $field_key; ?>">
		<label>
			<input type="radio" class="wc-pao-addon-field wc-pao-addon-radio"
                   value=""
                   name="<?php echo $addon_key; ?>[]"
            />&nbsp;&nbsp;<?php esc_html_e( 'None', 'woocommerce-product-addons' ); ?>
		</label>
	</p>
<?php } ?>

<?php
$first = true;
foreach ( $addon['options'] as $i => $option ) :

    $loop++;
    $price        = ! empty( $option['price'] ) ? $option['price'] : '';
    $price_prefix = 0 < $price ? '+' : '';
    $price_type   = ! empty( $option['price_type'] ) ? $option['price_type'] : '';
    $price_raw    = apply_filters( 'woocommerce_product_addons_option_price_raw', $price, $option );
    $label        = ( '0' === $option['label'] ) || ! empty( $option['label'] ) ? $option['label'] : '';
    $sanitized_label = sanitize_title( $label );

    if ( 'percentage_based' === $price_type ) {
        $price_for_display = apply_filters( 'woocommerce_product_addons_option_price',
            $price_raw ? '(' . $price_prefix . $price_raw . '%)' : '',
            $option,
            $i,
            'radiobutton'
        );
    } else {
        $price_for_display = apply_filters( 'woocommerce_product_addons_option_price',
            $price_raw ? '(' . $price_prefix . wc_price( WC_Product_Addons_Helper::get_product_addon_price_for_display( $price_raw ) ) . ')' : '',
            $option,
            $i,
            'radiobutton'
        );
    }

    $price_display = WC_Product_Addons_Helper::get_product_addon_price_for_display( $price_raw );

    if ( 'percentage_based' === $price_type ) {
        $price_display = $price_raw;
    }

	if ( isset( $_POST[ $addon_key ] ) ) {
		$current_value = (
				isset( $_POST[ $addon_key ] ) &&
				in_array( $sanitized_label, $_POST[ $addon_key ] )
				) ? 1 : 0;
	} else {
		$current_value = $first ? 1 : 0;
		$first         = false;
	}

    $addon_wrap_key = 'addon-wrap-' . $field_key. '-' . $i;
	$field_id = $field_key . '-' . $sanitized_label;
	?>

	<p class="form-row form-row-wide addon-wrap <?php echo $addon_wrap_key; ?> ">
		<input id="<?php echo $field_id; ?>"
                type="radio"
                class="addon addon-radio"
                name="<?php echo $addon_key; ?>[]"
                data-raw-price="<?php echo esc_attr( $price_raw ); ?>"
                data-price="<?php echo esc_attr( $price_display ); ?>"
                data-price-type="<?php echo esc_attr( $price_type ); ?>"
                value="<?php echo $sanitized_label; ?>"
                <?php checked( $current_value, 1 ); ?>
                <?php if ( WC_Product_Addons_Helper::is_addon_required( $addon ) ) { echo 'required'; } ?>
                data-label="<?php echo esc_attr( wptexturize( $label ) ); ?>"
        />
		<label for="<?php echo $field_id; ?>"
               class="<?php echo $sanitized_label; ?>">
			<?php echo wptexturize( $label . ' ' . $price_for_display ); ?>
		</label>
	</p>

<?php endforeach; ?>