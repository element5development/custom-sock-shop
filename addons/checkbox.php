<?php
/**
 * The Template for displaying checkbox field.
 *
 * @version 3.0.0
 */
global $post;
$post_id = $post->ID;


$field_name  = ! empty( $addon['field_name'] ) ? $addon['field_name'] : '';
$sanitized_field_name = sanitize_title ( $field_name );
$name = ! empty( $addon['name'] ) ? $addon['name'] : '';
$sanitized_name = sanitize_title ( $name );
$field_key = $post_id . '-' . $sanitized_name;
$addon_key     = 'addon-' . sanitize_title( $field_key );

$addon_required = WC_Product_Addons_Helper::is_addon_required( $addon );
$addon_required = false; // force not required until final due-diligence

if ( $addon_required ) {
	?>
	<div class="wc-pao-addon-checkbox-group-required">
	<?php
}
?>

<?php
foreach ( $addon['options'] as $i => $option ) :

	$option_price      = ! empty( $option['price'] ) ? $option['price'] : '';
	$option_price_type = ! empty( $option['price_type'] ) ? $option['price_type'] : '';
	$price_prefix       = 0 < $option_price ? '+' : '';
	$price_type        = $option_price_type;
	$price_raw         = apply_filters( 'woocommerce_product_addons_option_price_raw', $option_price, $option );
	$option_label      = ( '0' === $option['label'] ) || ! empty( $option['label'] ) ? $option['label'] : '';
	$sanitized_label = sanitize_title( $option_label );
	$price_display     = WC_Product_Addons_Helper::get_product_addon_price_for_display( $price_raw );
	$required_html     = $addon_required ? 'required' : '';

	if ( 'percentage_based' === $price_type ) {
		$price_display = $price_raw;
		$price_for_display = apply_filters( 'woocommerce_product_addons_option_price',
			$price_raw ? '(' . $price_prefix . $price_raw . '%)' : '',
			$option,
			$i,
			'checkbox'
		);
	} else {
		$price_for_display = apply_filters( 'woocommerce_product_addons_option_price',
			$price_raw ? '(' . $price_prefix . wc_price( WC_Product_Addons_Helper::get_product_addon_price_for_display( $price_raw ) ) . ')' : '',
			$option,
			$i,
			'checkbox'
		);
	}

	$selected = isset( $_POST[ $addon_key ] ) ? $_POST[$addon_key ] : array();
	if ( ! is_array( $selected ) ) {
		$selected = array( $selected );
	}

	$current_value     = ( in_array( $sanitized_label, $selected ) ) ? 1 : 0;
    $addon_wrap_key = 'addon-wrap-' . $field_key. '-' . $i;
    $field_id = $field_key . '-' . $sanitized_label;
	?>

	<p class="form-row form-row-wide addon-wrap <?php echo $addon_wrap_key; ?>">
        <input
            id="<?php echo $field_id; ?>"
            type="checkbox"
            class="addon addon-checkbox"
            name="<?php echo $addon_key; ?>[]"
            data-raw-price="<?php echo esc_attr( $price_raw ); ?>"
            data-price="<?php echo esc_attr( $price_display ); ?>"
            data-price-type="<?php echo esc_attr( $price_type ); ?>"
            value="<?php echo $sanitized_label; ?>"
            data-label="<?php echo esc_attr( wptexturize( $option_label ) ); ?>"
            <?php checked( $current_value, 1 ); ?>
            <?php echo $required_html; ?>
		/>
		<label for="<?php echo $field_id; ?>">
			<?php echo wptexturize( $option_label . ' ' . $price_for_display ); ?>
		</label>
	</p>

<?php endforeach; ?>
<?php
if ( $addon_required ) {
	?>
	</div>
	<?php
}

?>
