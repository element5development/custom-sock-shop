<?php 
	if ( get_field('background_image') ) {
		$background = get_field('background_image');
		$backgroundURL = $background['url'];
	} else {
		$backgroundURL = get_stylesheet_directory_uri() . '/dist/images/header-default.jpg';
	}
?>

<?php $style = $_GET["style"]; ?>
<?php if ( is_product() && !empty($style) ) : ?>
	<section class="step-numbers">
		<div class="step-one is-complete">
			<a href="<?php the_permalink(23712); ?>" class="count">
				<span>1</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path fill="#fff" d="M18 40L4 26l6-6 8 8L38 8l6 6z"/></svg>
			</a>
			<div class="label">Usage</div>
		</div>
		<div class="step-two is-complete">
			<a href="<?php the_permalink(23712); ?>/?style=<?php echo $style; ?>" class="count">
				<span>2</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path fill="#fff" d="M18 40L4 26l6-6 8 8L38 8l6 6z"/></svg>
			</a>
			<div class="label">Style</div>
		</div>
		<div class="step-three is-active">
			<div class="count">
				<span>3</span>
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48"><path fill="#fff" d="M18 40L4 26l6-6 8 8L38 8l6 6z"/></svg>
			</div>
			<div class="label">Design</div>
		</div>
	</section>
<?php elseif ( !is_product() ) : ?>
	<section id="headerimg" data-speed="2" style="background-image: url(<?php echo $backgroundURL; ?>);">
		<div class="headingcontainer">
			<?php if ( is_404() ) : ?>
				<h1>404 Error - Page Not Found</h1>
			<?php elseif ( is_shop() || is_product_category() || is_product_tag() ) : ?>
				<h1>
					<?php woocommerce_page_title(); ?>
				</h1>
			<?php elseif ( is_product() || is_cart() || is_checkout() || is_account_page() ) : ?>
				<h1>
					<?php the_title(); ?>
				</h1>
			<?php elseif ( is_search() ) : ?>
				<h1>Search</h1>
			<?php elseif ( is_home() ) : ?>
				<h1>The Sock Life</h1>
			<?php elseif ( is_page_template('templates/page-onboarding.php') && $_GET["type"] ) : ?>
				<?php 
					$type = strtok(ucwords(str_replace('-', ' ', $_GET["type"])), ' ');
				?>
				<h1>
					Custom <?php echo $type; ?> Socks
				</h1>
			<?php else : ?>
				<h1>
					<?php the_title(); ?>
				</h1>
			<?php endif; ?>

			<?php if ( function_exists('yoast_breadcrumb') ) {
			yoast_breadcrumb('<p id="breadcrumbs">','</p>');
			} ?>
		</div>
	</section>
<?php else: ?>
	<!-- empty -->
<?php endif; ?>

<!-- dress sock special -->
<?php if ( is_cart() && get_field('special_offer', 10776) ) : //dress socks landing  ?>

	<?php 
		// set our flag to be false until we find a product in that category
		$cat_check = false;			
		// check each cart item for our category
		foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {		
				$product = $cart_item['data'];
				// replace 'membership' with your category's slug
				if ( has_term( 'dress', 'product_cat', $product->id ) ) {
						$cat_check = true;
						// break because we only need one "true" to matter here
						break;
				}
		}				
	?>

	<?php if ( $cat_check ) { ?>
		<section class="special">
			<h3><?php the_field('special_offer', 10776); ?></h3>
		</section>
	<?php } ?>

<?php endif; ?>
<!-- dress sock special -->