<?php 
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
	$args = array(
		'post_type'      => 'sock-of-the-month',
		'posts_per_page' => 6,
		'order'          => 'DESC',
		'orderby'        => 'date',
		'paged' 				 => $paged
	);
	$parent = new WP_Query( $args );
?>
<?php if ( $parent->have_posts() ) : ?>
<div class="previous-months wrap">
	<h2>Previous Socks</h2>
	<div class="flex-container full-width">
		<?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
				<?php 
					$image = get_field('final_product_image');
					$size = 'large';
					$large = $image['sizes'][ $size ];
				?>
				<a href="<?php the_permalink(); ?>" class="past-sock">
					<div class="sock" style="background-image: url('<?php echo $large; ?>');"></div>
				</a>
		<?php endwhile; ?>
	</div>
	<div class="load-more-btn">
		<div class="page-load-status">
			<p class="infinite-scroll-request">
				<svg class="loading" x="0px" y="0px" width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;">
					<path d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
						<animateTransform attributeType="xml"
							attributeName="transform"
							type="rotate"
							from="0 25 25"
							to="360 25 25"
							dur="0.6s"
							repeatCount="indefinite"/>
					</path>
				</svg>
			</p>
			<p class="infinite-scroll-last"></p>
			<p class="infinite-scroll-error"></p>
		</div>
		<div class="sotm-pagination">
			<?php next_posts_link('&laquo; next', $parent->max_num_pages) ?>
			<?php previous_posts_link('previous &raquo;') ?>
		</div>
		<button class="btn load-more">Load more</button>
	</div>
</div>
<?php endif; wp_reset_query(); ?>