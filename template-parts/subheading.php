<section id="secondary">
	<div class="pageicon">
		<?php if( get_field('page_icon') ): ?>
			<img src="<?php the_field('page_icon'); ?>" alt="<?php the_title(); ?>" />
		<?php else: ?>
			<img src="<?php bloginfo('template_url'); ?>/dist/images/icon-default.png" alt="<?php the_title(); ?>" />			
		<?php endif; ?>
	</div>
	<div class="wrap">	
		<?php the_field('page_subheading'); ?>
	</div>
</section>