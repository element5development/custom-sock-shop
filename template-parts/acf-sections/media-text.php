<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying 2 columns one with a image the other with an editor

\*----------------------------------------------------------------*/
?>

<section id="section-<?php echo $template_args['sectionId']; ?>" class="media-text <?php the_sub_field('image_alignment'); ?> <?php the_sub_field('width'); ?>">
	<div>
		<?php $image = get_sub_field('image'); ?>
		<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
	<div>
		<div>
			<?php the_sub_field('content'); ?>
		</div>
	</div>
</section>