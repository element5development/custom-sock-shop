<?php 
/*-------------------------------------------------------------------
Header
------------------------------------------------------------------*/
?>

<!doctype html>
<!--[if lt IE 7 ]> <html class="ie ie6 ie-lt10 ie-lt9 ie-lt8 ie-lt7 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7 ]>    <html class="ie ie7 ie-lt10 ie-lt9 ie-lt8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8 ]>    <html class="ie ie8 ie-lt10 ie-lt9 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 9 ]>    <html class="ie ie9 ie-lt10 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<!--<![endif]-->
<!-- the "no-js" class is for Modernizr. -->

<?php //wp_set_auth_cookie(1);?>

<head data-template-set="html5-reset-wordpress-theme">

	<!-- Facebook Pixel Code -->
	<script>
		! function (f, b, e, v, n, t, s) {
			if (f.fbq) return;
			n = f.fbq = function () {
				n.callMethod ?
					n.callMethod.apply(n, arguments) : n.queue.push(arguments)
			};
			if (!f._fbq) f._fbq = n;
			n.push = n;
			n.loaded = !0;
			n.version = '2.0';
			n.queue = [];
			t = b.createElement(e);
			t.async = !0;
			t.src = v;
			s = b.getElementsByTagName(e)[0];
			s.parentNode.insertBefore(t, s)
		}(window,
			document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '605075289673697');
		fbq('track', "PageView");
	</script>
	<noscript>
		<img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=605075289673697&ev=PageView&noscript=1"
		/>
	</noscript>
	<!-- End Facebook Pixel Code -->

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<!--[if IE ]>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<![endif]-->

	<?php
		if (is_search())
			echo '<meta name="robots" content="noindex, nofollow" />';
	?>

		<title>
			<?php wp_title( '|', true, 'right' ); ?>
		</title>

		<meta name="title" content="<?php wp_title( '|', true, 'right' ); ?>">

			<meta name="Copyright" content="Copyright &copy; <?php bloginfo('name'); ?> <?php echo date('Y'); ?>. All Rights Reserved.">
			<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/dist/images/favicon.png">
			<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="msvalidate.01" content="D7FF62B50B5AC4465FB383958CD4A132" />

			<link rel="profile" href="http://gmpg.org/xfn/11" />
			<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
			<?php wp_head(); ?>
			<?php global $wp;
		//echo '<pre>'; print_r($wp->query_vars['pagename']); exit();	
		if(!empty($wp->query_vars['pagename']) && $wp->query_vars['pagename'] == 'sock-customizer/'){
	?>
			<script>
				var click_target = '';
				var click_text = '';
				var counter = 0;
				document.addEventListener('click', function (e) {
					e = e || window.event;
					click_target = e.target || e.srcElement;
					click_text = click_target.textContent.trim() == '' ? click_text : click_target.textContent;
				}, false);
				window.onbeforeunload = function (example) {
					//alert();
					if (click_text != 'ADD TO CART' && click_text != 'UPDATE CART ITEM') {
						example = example || window.event;
						// For IE and Firefox prior to version 4
						if (example) {
							example.returnValue =
								'If you leave this page without saving your design or adding it to your cart your design will be lost.  ';
						}
						// For Safari
						return 'If you leave this page without saving your design or adding it to your cart your design will be lost.  ';
					}
				};
			</script>
			<?php }?>

			<!-- Hotjar -->
			<script>
				(function (h, o, t, j, a, r) {
					h.hj = h.hj || function () {
						(h.hj.q = h.hj.q || []).push(arguments)
					};
					h._hjSettings = {
						hjid: 178850,
						hjsv: 5
					};
					a = o.getElementsByTagName('head')[0];
					r = o.createElement('script');
					r.async = 1;
					r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
					a.appendChild(r);
				})(window, document, '//static.hotjar.com/c/hotjar-', '.js?sv=');
			</script>
			<!-- HOTJAR -->

			<!-- Google Tag Manager -->
			<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-TRK7ZSR');</script>
			<!-- End Google Tag Manager -->

			<!--GOOGLE CALL TRACKING-->
			<script type="text/javascript">
				(function (a, e, c, f, g, b, d) {
					var h = {
						ak: "1006169617",
						cl: "OlxkCLvaymcQkdzj3wM"
					};
					a[c] = a[c] || function () {
						(a[c].q = a[c].q || []).push(arguments)
					};
					a[f] || (a[f] = h.ak);
					b = e.createElement(g);
					b.async = 1;
					b.src = "//www.gstatic.com/wcm/loader.js";
					d = e.getElementsByTagName(g)[0];
					d.parentNode.insertBefore(b, d);
					a._googWcmGet = function (b, d, e) {
						a[c](2, b, h, d, null, new Date, e)
					}
				})(window, document, "_googWcmImpl", "_googWcmAk", "script");
			</script>

			<meta name="p:domain_verify" content="b7b8ff6a6a853859243dff3a8124af71" />
</head>


<body <?php body_class(); ?> onload="_googWcmGet('number', '888-557-9185')">
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TRK7ZSR"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	
	<?php	$style = $_GET["style"]; ?>
	<?php if ( is_page_template('templates/design-your-own.php') || !empty($style) ) : ?>
		<nav id="header" class="primary simplified">
			<div class="left">
				<a class="logo" href="<?php echo get_site_url(); ?>">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/css-logo-teal.svg" alt="Custom Sock Shop Logo" />
				</a>
				<a class="phone" href="tel:+18885579185">Call Us: <span>888-557-9185</span></a>
			</div>
			<div class="cart">
				<div class="icon-menu">
					<?php 
						global $woocommerce; 
						$count = sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);
					?>
					<a class="cart" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
						<?php if ( $count != '0' && $count != 0  ) : ?>
							<span class="count">
								<?php echo $count; ?>
							</span>
						<?php endif; ?>
						<svg width="23" height="21" xmlns="http://www.w3.org/2000/svg"><g fill="#8949A2" fill-rule="nonzero"><path d="M22.696 2.673l-.094-.093c-.118-.118-.352-.305-.68-.305H5.928l-.023-.211A2.45 2.45 0 0 0 3.466 0H1.825A.901.901 0 0 0 .91.915c0 .515.399.914.915.914H3.49c.304 0 .562.211.61.516l1.406 8.442c0 .07.024.164.047.235l.235 1.43c.234 1.384 1.477 2.439 2.884 2.439h10.881a.901.901 0 0 0 .915-.915.901.901 0 0 0-.915-.914H8.65c-.54 0-.985-.375-1.056-.891l-.093-.516H19.88a.99.99 0 0 0 .516-.07.97.97 0 0 0 .47-.587l1.97-7.527a.988.988 0 0 0-.141-.798zM8.86 15.079a2.537 2.537 0 0 0-2.533 2.532c0 1.384 1.126 2.556 2.533 2.556a2.537 2.537 0 0 0 2.532-2.532c0-1.384-1.125-2.556-2.532-2.556zm0 1.852c.375 0 .656.305.656.68a.68.68 0 0 1-.68.68c-.375 0-.656-.305-.656-.68a.68.68 0 0 1 .68-.68zM16.458 15.079a2.537 2.537 0 0 0-2.533 2.532c0 1.384 1.126 2.556 2.533 2.556a2.537 2.537 0 0 0 2.532-2.532c0-1.384-1.149-2.556-2.532-2.556zm-.024 1.852c.399 0 .68.305.68.68a.68.68 0 0 1-1.36 0c-.023-.375.282-.68.68-.68z"/></g></svg>
					</a>
				</div>
			</div>
		</nav>
	<?php else : ?> 
		<?php get_template_part('template-parts/notification-bar'); ?>
		<nav id="header" class="primary<?php if ( get_field('notification_bar_text', 'options') ) : ?> notification-on<?php endif; ?>">
			<div class="left">
				<a class="logo" href="<?php echo get_site_url(); ?>">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/dist/images/css-logo-teal.svg" alt="Custom Sock Shop Logo" />
				</a>
				<a class="phone" href="tel:+18885579185">Call Us: <span>888-557-9185</span></a>
			</div>
			<button class="mobile-menu-toggle">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 12"><g class="nc-icon-wrapper" fill="#8949a2"><path d="M11 9H1a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2zM11 1H1a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2z"/><path d="M11 5H1a1 1 0 0 0 0 2h10a1 1 0 0 0 0-2z" data-color="color-2"/></g></svg>
			</button>
			<div class="right">
				<button class="mobile-menu-toggle">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M9.3 22.7c.2.2.4.3.7.3s.5-.1.7-.3l5.3-5.3 5.3 5.3c.2.2.5.3.7.3s.5-.1.7-.3c.4-.4.4-1 0-1.4L17.4 16l5.3-5.3c.4-.4.4-1 0-1.4s-1-.4-1.4 0L16 14.6l-5.3-5.3c-.4-.4-1-.4-1.4 0s-.4 1 0 1.4l5.3 5.3-5.3 5.3c-.4.4-.4 1 0 1.4z" fill="#8949a2"/></svg>
				</button>
				<?php wp_nav_menu(array( 'theme_location' => 'primary' )); ?>
				<div class="icon-menu">
					<a href="<?php the_permalink(7); ?>">
						<svg width="19" height="18" xmlns="http://www.w3.org/2000/svg"><path d="M18.865 17.07v.001c0 .47-.381.852-.852.852H1.539a.852.852 0 0 1-.853-.852v-.002s0-3.105 2.269-4.234c1.436-.714.883-.134 2.646-.865 1.762-.73 2.18-.985 2.18-.985l.016-1.683s-.66-.505-.865-2.088c-.413.12-.55-.485-.574-.87-.022-.371-.239-1.532.265-1.427-.103-.775-.177-1.474-.14-1.845C6.608 1.772 7.86.414 9.788.315c2.269.099 3.167 1.456 3.294 2.756.036.37-.044 1.07-.148 1.844.504-.104.285 1.056.26 1.427-.022.385-.161.987-.573.868-.206 1.583-.866 2.084-.866 2.084l.015 1.674s.418.239 2.18.969c1.763.73 1.21.184 2.646.898 2.269 1.13 2.269 4.234 2.269 4.234z" fill="#8949A2" fill-rule="evenodd"/></svg>
					</a>
					<?php 
						global $woocommerce; 
						$count = sprintf(_n('%d', '%d', $woocommerce->cart->cart_contents_count, 'woothemes'), $woocommerce->cart->cart_contents_count);
					?>
					<a class="cart" href="<?php echo wc_get_cart_url() ?>" title="<?php _e('View your shopping cart', 'woothemes'); ?>">
						<?php if ( $count != '0' && $count != 0  ) : ?>
							<span class="count">
								<?php echo $count; ?>
							</span>
						<?php endif; ?>
						<svg width="23" height="21" xmlns="http://www.w3.org/2000/svg"><g fill="#8949A2" fill-rule="nonzero"><path d="M22.696 2.673l-.094-.093c-.118-.118-.352-.305-.68-.305H5.928l-.023-.211A2.45 2.45 0 0 0 3.466 0H1.825A.901.901 0 0 0 .91.915c0 .515.399.914.915.914H3.49c.304 0 .562.211.61.516l1.406 8.442c0 .07.024.164.047.235l.235 1.43c.234 1.384 1.477 2.439 2.884 2.439h10.881a.901.901 0 0 0 .915-.915.901.901 0 0 0-.915-.914H8.65c-.54 0-.985-.375-1.056-.891l-.093-.516H19.88a.99.99 0 0 0 .516-.07.97.97 0 0 0 .47-.587l1.97-7.527a.988.988 0 0 0-.141-.798zM8.86 15.079a2.537 2.537 0 0 0-2.533 2.532c0 1.384 1.126 2.556 2.533 2.556a2.537 2.537 0 0 0 2.532-2.532c0-1.384-1.125-2.556-2.532-2.556zm0 1.852c.375 0 .656.305.656.68a.68.68 0 0 1-.68.68c-.375 0-.656-.305-.656-.68a.68.68 0 0 1 .68-.68zM16.458 15.079a2.537 2.537 0 0 0-2.533 2.532c0 1.384 1.126 2.556 2.533 2.556a2.537 2.537 0 0 0 2.532-2.532c0-1.384-1.149-2.556-2.532-2.556zm-.024 1.852c.399 0 .68.305.68.68a.68.68 0 0 1-1.36 0c-.023-.375.282-.68.68-.68z"/></g></svg>
					</a>
					<!-- <button class="search-toggle-open">
						<svg width="19" height="19" xmlns="http://www.w3.org/2000/svg"><path d="M18.69 17.1l-3.363-3.34c2.625-3.362 2.349-8.152-.668-11.17A8.378 8.378 0 0 0 8.694.127a8.378 8.378 0 0 0-5.965 2.465A8.378 8.378 0 0 0 .264 8.556c0 2.257.876 4.376 2.465 5.965a8.378 8.378 0 0 0 5.965 2.464 8.33 8.33 0 0 0 5.182-1.796l3.34 3.385c.206.207.46.3.736.3.276 0 .53-.116.737-.3.415-.391.415-1.06 0-1.474zm-3.64-8.544a6.306 6.306 0 0 1-1.865 4.49 6.346 6.346 0 0 1-4.491 1.866 6.346 6.346 0 0 1-4.491-1.865 6.346 6.346 0 0 1-1.866-4.491c0-1.705.668-3.294 1.866-4.491a6.346 6.346 0 0 1 4.49-1.866c1.705 0 3.294.668 4.492 1.866a6.306 6.306 0 0 1 1.865 4.49z" fill="#8949A2" fill-rule="nonzero"/></svg>
					</button> -->
				</div>
			</div>
		</nav>
		<div class="search-modal">
			<button class="search-toggle-close">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M9.3 22.7c.2.2.4.3.7.3s.5-.1.7-.3l5.3-5.3 5.3 5.3c.2.2.5.3.7.3s.5-.1.7-.3c.4-.4.4-1 0-1.4L17.4 16l5.3-5.3c.4-.4.4-1 0-1.4s-1-.4-1.4 0L16 14.6l-5.3-5.3c-.4-.4-1-.4-1.4 0s-.4 1 0 1.4l5.3 5.3-5.3 5.3c-.4.4-.4 1 0 1.4z" fill="#fff"/></svg>
			</button>
			<?php echo get_search_form(); ?>
		</div>
	<?php endif; ?>

	<?php if( !is_front_page() && !is_page_template('templates/page-home.php') && !is_page_template('templates/page-front.php') && !is_page_template('templates/page-dress-socks.php') && !is_page_template('templates/page-landing-2017.php') && !is_page_template('templates/page-landing-2019.php') && !is_page_template('templates/page-fundraiser.php') && !is_page_template('templates/application.php') && !is_page_template('templates/acf-powered.php') && !is_page_template('templates/design-your-own.php') ) : ?>
		<?php get_template_part('template-parts/header-interior'); ?>
	<?php endif; ?>