Instruction to v5 [Adding Scarf and Beanie]

1. WPADMIN: Add category "scarf" (Scarf)
2. WPADMIN: Add category "beanie" (Beanie)

3. WPADMIN: Add Scarf product page - set category to "Scarf"
4. WPADMIN: Add Beanie product page - set category to "Beanie"

5. WPADMIN: Custom Fields - change default value of Liquid Pixels > Liquid Pixels Revision to 5.0.0

6. WPADMIN: *Edit Product Addon  - "Base Sock" ->  Replace "All products" and select each supported product except scarf and beanie
*** THis is important or else "Sock Base Color" field will be visible also in Scarf and Beanie pages.

7. WPADMIN: Add new Addon for Beanie - import (wp-product-addon\beanie.txt) - select "Beanie" as product category

8. WPADMIN: Add new Addon for Scarf - import (wp-product-addon\scarf.txt) - select "Scarf" as product category

=================== SERVER FILE UPDATES ==================================
[in /wp-content/themes/customsockshop]
1. lib/liquid-pixels-setup.php

2. To allow existing customers load latest js and css change the version of the following js/css loaded
  in lib/theme_support.php
  I have set the version to 'lp-5.0.0.0'. You can use any other unique value which has never used earlier.

  wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css', array(), 'lp-5.0.0.0', 'all');
  ...
  wp_enqueue_script('main', get_template_directory_uri() . '/dist/scripts/master/main.js', array( 'jquery' ), 'lp-5.0.0.1', true);

3. Compile with Gulp and update or use the already present in the branch -
	a) dist/scripts/master/main.js
	a) dist/scripts/vendors/vendors.js
	b) dist/styles/main.css


===================== LIQUID PIXELS SERVER ===================================

1. Create directory Art > socks\v5
	- Upload SVGs for scarf and Beanie - there are 3 files for each
		a) front.svg
		b) border\front.svg
		c) full\front.svg

2. Create directory Data > socks\v5
	- Upload table files for Scarf and Beanie
		a) scarf.table
		b) scarf.textpos.table
		c) beanie.table
		d) beanie.textpos.table

3. Create directory Chains > socks\v5
	- Upload 3 Chain files
		a) gen.chain (directly copied from socks\v4)
		a) dress-gen.chain (directly copied from socks\v4)
		a) others-gen.chain (new one for Scarf and Beanie)

NOTE: LiquidPixel Assets (Art - sock svg images and Data - tables) for all existing v4 socks (all socks including dress socks)
are not copied to respective v5 directories in LiquidPixels. Those are still in v4 directories
and will be accessed by Chains\socks\v5\gen.chain and Chains\socks\v5\dress-gen.chain.
Hence, make sure not to delete any files from Art\socks\v4 and Data\socks\v4.


