#set=face[right],sock[kneehigh],sockcolor[#008355],stripecolor[red],stripes[4]

#load data
lookup=key[global.face],table[file:socks/v5/('global.sock').table],name[d]
lookup=key[global.face],table[file:socks/v5/('global.sock').textpos.table],name[dtext]

## General vars
set=isScarf[0],isBeanie[0]
set=isScarf[1],if[('global.sock' eq 'scarf')]
set=isBeanie[1],if[('global.sock' eq 'beanie')]
set=normalStrokeWidth[.75px],colorWhite[#FFFFFF]
set=thinStroke[#DDDDDD],thinStrokeWidth[1px]

set=hasWhiteFill[0],hasWhiteText[0]
set=hasWhiteFill[1],if[('global.sockcolor' eq 'global.colorWhite')]
set=hasWhiteText[1],if[('global.textcolor' eq 'global.colorWhite')]

set=sockBorderColor[global.sockcolor]
set=sockBorderColor[global.thinStroke],if[('global.sockcolor' eq 'global.colorWhite')]
set=borderStrokeWidth[.75px]
set=borderStrokeWidth[1px], if[('global.sockBorderColor' eq 'global.thinStroke')]


#text rendering alignment (set to alignto porperty)
#topcenter center bottomcenter
set=textalignto[center]
set=textfontsize[global.fontsize]

#define mask
source=url[file:socks/v5/global.sock/('global.face').svg],name[mask]
svg=type[style],id[base],value[fill: black; stroke:black; stroke-width: 2px;]
svg=type[style],id[tassels],value[fill: black; stroke:black; stroke-width: 2px;]
svg=type[style],id[brim],value[fill: black; stroke:black; stroke-width: 2px;]
svg=type[style],id[pompom-full],value[fill: black; stroke:black; stroke-width: 2px;]

# load sock image
source=url[file:socks/v5/global.sock/full/('global.face').svg],name[base1]
svg=type[style],id[base],value[fill: global.sockcolor; stroke-width: 0;]


################ SCARF
## stripecolor
## First disable all stripes
svg=type[style],id[stripe-behind-logo],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isScarf' eq '1')]
svg=type[style],id[stripe-above-and-below-text],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isScarf' eq '1')]
svg=type[style],id[stripe-above-and-below-text-and-behind-logo],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isScarf' eq '1')]
svg=type[style],id[stripe-across-whole-scarf],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isScarf' eq '1')]

set=scarfStripeStyleId[('')],hasScarfStyle[0]
set=hasScarfStyle[1],if[('global.scarfstripestyle' ne '' and 'global.scarfstripestyle' ne 'none')]
set=scarfStripeStyleId[('stripe-behind-logo')],if[('global.hasScarfStyle' eq '1' and 'global.scarfstripestyle' eq 'primary')]
set=scarfStripeStyleId[('stripe-above-and-below-text')],if[('global.hasScarfStyle' eq '1' and 'global.scarfstripestyle' eq 'secondary')]
set=scarfStripeStyleId[('stripe-above-and-below-text-and-behind-logo')],if[('global.hasScarfStyle' eq '1' and 'global.scarfstripestyle' eq 'both')]
set=scarfStripeStyleId[('stripe-across-whole-scarf')],if[('global.hasScarfStyle' eq '1' and 'global.scarfstripestyle' eq 'across-the-scarf-length')]

svg=type[style],id[global.scarfStripeStyleId],value[fill: global.stripecolor; fill-opacity: 1; opacity: 1; display: inline; ],if[('global.hasScarfStyle' eq '1')]
svg=type[style],id[global.scarfStripeStyleId],value[stroke: global.thinStroke; stroke-opacity: 1; stroke-width: global.thinStrokeWidth;],if[('global.stripecolor' eq 'global.colorWhite')]
set=hasWhiteStripes[1],if[('global.stripecolor' eq 'global.colorWhite')]

## tassels
set=tasselcolor1[global.sockcolor],if[('global.tasselcolor1' eq '' and 'global.isScarf' eq '1')]
set=tasselcolor2[global.tasselcolor1],if[('global.tasselcolor2' eq '' and 'global.isScarf' eq '1')]
set=tasselcolor3[global.tasselcolor1],if[('global.tasselcolor3' eq '' and 'global.isScarf' eq '1')]
set=tasselcolor4[global.tasselcolor1],if[('global.tasselcolor4' eq '' and 'global.isScarf' eq '1')]
set=tasselcolor5[global.tasselcolor1],if[('global.tasselcolor5' eq '' and 'global.isScarf' eq '1')]
set=tasselcolor6[global.tasselcolor1],if[('global.tasselcolor6' eq '' and 'global.isScarf' eq '1')]

set=tslc1s[global.tasselcolor1]
set=tslc2s[global.tasselcolor2]
set=tslc3s[global.tasselcolor3]
set=tslc4s[global.tasselcolor4]
set=tslc5s[global.tasselcolor5]
set=tslc6s[global.tasselcolor6]

set=tslc1s[global.thinStroke],if[('global.tslc1s' eq 'global.colorWhite')]
set=tslc2s[global.thinStroke],if[('global.tslc2s' eq 'global.colorWhite')]
set=tslc3s[global.thinStroke],if[('global.tslc3s' eq 'global.colorWhite')]
set=tslc4s[global.thinStroke],if[('global.tslc4s' eq 'global.colorWhite')]
set=tslc5s[global.thinStroke],if[('global.tslc5s' eq 'global.colorWhite')]
set=tslc6s[global.thinStroke],if[('global.tslc6s' eq 'global.colorWhite')]

svg=type[style],id[tassel-1],value[fill: global.tasselcolor1; stroke-width: 0; stroke: global.tslc1s; stroke-opacity: 0; fill-opacity: 1; opacity: 1;],if[('global.isScarf' eq '1')]
svg=type[style],id[tassel-2],value[fill: global.tasselcolor2; stroke-width: 0; stroke: global.tslc2s; stroke-opacity: 0; fill-opacity: 1; opacity: 1;],if[('global.isScarf' eq '1')]
svg=type[style],id[tassel-3],value[fill: global.tasselcolor3; stroke-width: 0; stroke: global.tslc3s; stroke-opacity: 0; fill-opacity: 1; opacity: 1;],if[('global.isScarf' eq '1')]
svg=type[style],id[tassel-4],value[fill: global.tasselcolor4; stroke-width: 0; stroke: global.tslc4s; stroke-opacity: 0; fill-opacity: 1; opacity: 1;],if[('global.isScarf' eq '1')]
svg=type[style],id[tassel-5],value[fill: global.tasselcolor5; stroke-width: 0; stroke: global.tslc5s; stroke-opacity: 0; fill-opacity: 1; opacity: 1;],if[('global.isScarf' eq '1')]
svg=type[style],id[tassel-6],value[fill: global.tasselcolor6; stroke-width: 0; stroke: global.tslc6s; stroke-opacity: 0; fill-opacity: 1; opacity: 1;],if[('global.isScarf' eq '1')]

svg=type[style],id[tassel-1],value[stroke-width: global.thinStrokeWidth; stroke-opacity: 1; ],if[('global.tasselcolor1' eq 'global.colorWhite')]
svg=type[style],id[tassel-2],value[stroke-width: global.thinStrokeWidth; stroke-opacity: 1; ],if[('global.tasselcolor2' eq 'global.colorWhite')]
svg=type[style],id[tassel-3],value[stroke-width: global.thinStrokeWidth; stroke-opacity: 1; ],if[('global.tasselcolor3' eq 'global.colorWhite')]
svg=type[style],id[tassel-4],value[stroke-width: global.thinStrokeWidth; stroke-opacity: 1; ],if[('global.tasselcolor4' eq 'global.colorWhite')]
svg=type[style],id[tassel-5],value[stroke-width: global.thinStrokeWidth; stroke-opacity: 1; ],if[('global.tasselcolor5' eq 'global.colorWhite')]
svg=type[style],id[tassel-6],value[stroke-width: global.thinStrokeWidth; stroke-opacity: 1; ],if[('global.tasselcolor6' eq 'global.colorWhite')]


############## BEANIE


#brim
set=brimcolor[global.brimbasecolor]
set=brimcolor[global.sockcolor],if[('global.brimbasecolor' eq '' and 'global.isBeanie' eq '1')]
svg=type[style],id[brim],value[fill: global.brimcolor; stroke-width: 0;]
set=hasWhiteBrim[0]
set=hasWhiteBrim[1],if[('global.brimcolor' eq 'global.colorWhite')]
svg=type[style],id[brim],value[stroke: global.thinStroke; stroke-width: global.thinStrokeWidth; stroke-opacity: 1;],if[('global.hasWhiteBrim' eq '1')]


## First disable all stripes and pompoms
svg=type[style],id[stripe-brim],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]
svg=type[style],id[stripe-top-bottom],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]
svg=type[style],id[stripe-logo],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]

svg=type[style],id[pompom-full],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]
svg=type[style],id[pompom-2-left],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]
svg=type[style],id[pompom-2-right],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]
svg=type[style],id[pompom-3-left],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]
svg=type[style],id[pompom-3-middle],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]
svg=type[style],id[pompom-3-right],value[opacity: 0; stroke-opacity: 0; fill-opacity: 0; display: none; stroke-width: 0;],if[('global.isBeanie' eq '1')]

#pompom
set=pompomcolor1[global.sockcolor],if[('global.pompomcolor1' eq '' and 'global.isBeanie' eq '1')]
set=pCount[0]
set=pCount[1],if[('global.pompomcolor1' ne '' and 'global.pompomcolor2' eq '' and 'global.pompomcolor3' eq '')]
set=pCount[2],if[('global.pompomcolor1' ne '' and 'global.pompomcolor2' ne '' and 'global.pompomcolor3' eq '')]
set=pCount[3],if[('global.pompomcolor1' ne '' and 'global.pompomcolor2' ne '' and 'global.pompomcolor3' ne '')]

svg=type[style],id[pompom-full],value[fill: global.pompomcolor1; stroke-width: 0; stroke: global.pompomcolor1; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.pCount' eq '1')]
svg=type[style],id[pompom-2-left],value[fill: global.pompomcolor1; stroke-width: 0; stroke: global.pompomcolor1; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.pCount' eq '2')]
svg=type[style],id[pompom-2-right],value[fill: global.pompomcolor2; stroke-width: 0; stroke: global.pompomcolor2; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.pCount' eq '2')]
svg=type[style],id[pompom-3-left],value[fill: global.pompomcolor1; stroke-width: 0; stroke: global.pompomcolor1; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.pCount' eq '3')]
svg=type[style],id[pompom-3-middle],value[fill: global.pompomcolor3; stroke-width: 0; stroke: global.pompomcolor3; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.pCount' eq '3')]
svg=type[style],id[pompom-3-right],value[fill: global.pompomcolor2; stroke-width: 0; stroke: global.pompomcolor2; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.pCount' eq '3')]

svg=type[style],id[pompom-full],value[stroke: global.thinStroke; stroke-width: global.thinStrokeWidth; stroke-opacity: 1;],if[('global.pompomcolor1' eq 'global.colorWhite' and 'global.pCount' eq '1')]
svg=type[style],id[pompom-2-left],value[stroke: global.thinStroke; stroke-width: global.thinStrokeWidth; stroke-opacity: 1;],if[('global.pompomcolor1' eq 'global.colorWhite' and 'global.pCount' eq '2')]
svg=type[style],id[pompom-2-right],value[stroke: global.thinStroke; stroke-width: global.thinStrokeWidth; stroke-opacity: 1;],if[('global.pompomcolor2' eq 'global.colorWhite' and 'global.pCount' eq '2')]
svg=type[style],id[pompom-3-left],value[stroke: global.thinStroke; stroke-width: global.thinStrokeWidth; stroke-opacity: 1;],if[('global.pompomcolor1' eq 'global.colorWhite' and 'global.pCount' eq '3')]
svg=type[style],id[pompom-3-middle],value[stroke: global.thinStroke; stroke-width: global.thinStrokeWidth; stroke-opacity: 1;],if[('global.pompomcolor3' eq 'global.colorWhite' and 'global.pCount' eq '3')]
svg=type[style],id[pompom-3-right],value[stroke: global.thinStroke; stroke-width: global.thinStrokeWidth; stroke-opacity: 1;],if[('global.pompomcolor2' eq 'global.colorWhite' and 'global.pCount' eq '3')]

#stripes
svg=type[style],id[stripe-brim],value[fill: global.brimstripecolor; stroke-width: 0; stroke: global.brimstripecolor; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.brimstripecolor' ne '')]
svg=type[style],id[stripe-logo],value[fill: global.logostripecolor; stroke-width: 0; stroke: global.logostripecolor; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.logostripecolor' ne '')]
svg=type[style],id[stripe-top-bottom],value[fill: global.abovebelowstripecolor; stroke-width: 0; stroke: global.abovebelowstripecolor; stroke-opacity: 0; fill-opacity: 1; opacity: 1; display: inline;],if[('global.abovebelowstripecolor' ne '')]

svg=type[style],id[stripe-brim],value[stroke-width: global.thinStrokeWidth; stroke: global.thinStroke; stroke-opacity: 1;],if[('global.brimstripecolor' eq 'global.colorWhite')]
svg=type[style],id[stripe-logo],value[stroke-width: global.thinStrokeWidth; stroke: global.thinStroke; stroke-opacity: 1;],if[('global.logostripecolor' eq 'global.colorWhite')]
svg=type[style],id[stripe-top-bottom],value[stroke-width: global.thinStrokeWidth; stroke: global.thinStroke; stroke-opacity: 1;],if[('global.abovebelowstripecolor' eq 'global.colorWhite')]

set=hasWhiteStripes[1],if[('global.brimstripecolor' eq 'global.colorWhite' or 'global.logostripecolor' eq 'global.colorWhite' or 'global.abovebelowstripecolor' eq 'global.colorWhite')]

################################### LOGO #################################
# load logo file (important to render logo here to set layer position)

# logo
set=haslogoUrl[0],haslogo[0],haslogo2[0]
set=haslogo[1],if[('global.logofile' =~ /[a-zA-z]/ or 'global.logourl' =~ /[a-zA-z]/)]
set=haslogo2[1],if[('global.haslogo' and 'global.isScarf')]
set=haslogoUrl[1],if[('global.logourl' =~ /[a-zA-z]/)]

# set variables from lookup
set=logosize[global.d.1],logocx[global.d.2],logocy[global.d.3]
set=logo2size[global.d.1],logo2cx[global.d.4],logo2cy[global.d.5],if[('global.isScarf')]

source=url[file:global.logofile],name[logo],if[('global.haslogo' and 'global.logofile')]
source=url[global.logourl],name[logo],if[('global.haslogoUrl')]
scale=size[global.logosize],if[('global.haslogo')]
colorize=fill[global.logocolor],opacity[100],if[('global.logocolor' and 'global.haslogo')]
rotate=degrees[90],if[('global.haslogo' eq '1' and 'global.isScarf')]


#mirror logo for scarf
source=url[file:global.logofile],name[logo2],if[('global.haslogo2' eq '1' and 'global.logofile')]
source=url[global.logourl],name[logo2],if[('global.haslogo2' eq '1' and 'global.haslogoUrl')]
scale=size[global.logo2size],if[('global.haslogo2' eq '1')]
colorize=fill[global.logocolor],opacity[100],if[('global.logocolor' and 'global.haslogo2' eq '1')]
rotate=degrees[-90],if[('global.haslogo2' eq '1')]
#flipy=,if[('global.haslogo2' eq '1')]

# action on sock - rasterize
select=image[base1]
_rasterize

# compose logo
composite=compose[over],image[logo],x[(round('global.logocx' - 'logo.width'/2))],y[(round('global.logocy' - 'logo.height'/2))],if[('global.logofile')]
composite=compose[over],image[logo],x[(round('global.logocx' - 'logo.width'/2))],y[(round('global.logocy' - 'logo.height'/2))],if[('global.logourl')]

composite=compose[over],image[logo2],x[(round('global.logo2cx' - 'logo.width'/2))],y[(round('global.logo2cy' - 'logo2.height'/2))],if[('global.logofile' and 'global.haslogo2' eq '1')]
composite=compose[over],image[logo2],x[(round('global.logo2cx' - 'logo.width'/2))],y[(round('global.logo2cy' - 'logo2.height'/2))],if[('global.logourl' and 'global.haslogo2' eq '1')]


############################### TEXT ##########################################
# set variables from lookup
set=textposx[global.dtext.1],textposy[global.dtext.2],textposw[global.dtext.3],textposh[global.dtext.4],textwrap[global.dtext.5]
set=global.textposx[('global.textposx' * 1)]
set=global.textposy[('global.textposy' * 1)]
annotate=text[global.text],x[global.textposx],y[global.textposy],alignto[global.textalignto],pointsize[global.textfontsize],font[VeraSans-Bold],fill[global.textcolor],stroke[global.sockBorderColor],swidth[0.5],metricres[fine],if[('global.text' and 'global.hasWhiteText' eq '1')]
annotate=text[global.text],x[global.textposx],y[global.textposy],alignto[global.textalignto],pointsize[global.textfontsize],font[VeraSans-Bold],fill[global.textcolor],metricres[fine],if[('global.text' and 'global.hasWhiteText' ne '1')]


######################## BORDER ##########################################
set=needsBorder[0]
set=needsBorder[1],if[('global.hasWhiteFill' eq '1' or 'global.hasWhiteStripes' eq '1')]
source=url[file:socks/v5/global.sock/border/('global.face').svg],name[border],if[('global.needsBorder' eq '1')]
svg=type[style],id[base],value[fill: white; stroke-linecap:round; stroke: global.sockBorderColor; stroke-width: global.borderStrokeWidth; fill-opacity: 1; stroke-opacity: 1;],if[('global.needsBorder' eq '1')]
svg=type[style],id[brim],value[fill: white; stroke-linecap:round; stroke: global.sockBorderColor; stroke-width: global.borderStrokeWidth; fill-opacity: 1; stroke-opacity: 1;],if[('global.needsBorder' eq '1')]
svg=type[style],id[tassels],value[fill: white; stroke-linecap:round; stroke: global.sockBorderColor; stroke-width: global.borderStrokeWidth; fill-opacity: 1; stroke-opacity: 1;],if[('global.needsBorder' eq '1')]
svg=type[style],id[pompom-full],value[fill: white; stroke-linecap:round; stroke: global.sockBorderColor; stroke-width: global.borderStrokeWidth; fill-opacity: 1; stroke-opacity: 1;],if[('global.needsBorder' eq '1')]
_rasterize,if[('global.needsBorder' eq '1')]

###################### apply mask ##########################
select=image[base1]
composite=compose[multiply],image[border],if[('global.needsBorder' eq '1')]
select=image[base1]
composite=compose[blend],image[mask]

set=lpdebug[0]
set=lpdebug[1],if[('global.debug' eq '1')]
#annotate=text[haslogo2: global.haslogo2, isScarf: global.isScarf, isBeanie: global.isBeanie],alignto[topleft],pointsize[10],font[VeraSans-Bold],fill[red],x[1],y[1],metricres[fine],if[('global.lpdebug' eq '1')]
#annotate=text[p1: global.pompomcolor1, p2: global.pompomcolor2, p3: global.pompomcolor3],alignto[topleft],pointsize[10],font[VeraSans-Bold],fill[blue],x[1],y[13],metricres[fine],if[('global.lpdebug' eq '1')]

#draw=primitive[line],points[0,global.textposy,600,global.textposy],stroke[green],swidth[1],if[('global.lpdebug' eq '1')]
#draw=primitive[line],points[0,62,600,62],stroke[red],swidth[1],if[('global.lpdebug' eq '1')]
sink
